pipeline {
  agent {
    node {
        label 'windows'
      }
    }

    environment {
        MSBuildToolsPath = 'C:\\Program Files (x86)\\Microsoft Visual Studio\\2019\\Community\\MSBuild\\Current\\Bin\\MSBuild.exe'
    }

    options {
      skipDefaultCheckout(true)
      gitLabConnection('GitLab Public Server')
    }

  stages {
/******************************************************************************************************************************/
/*                                             Start of checkout stage                                                        */
/******************************************************************************************************************************/
    stage('Checkout') {
      steps {
        echo 'Notify GitLab'
        updateGitlabCommitStatus name: 'Checkout', state: 'pending'

        echo 'Checkout git branch'
        cleanWs()
        checkout scm
      }
      post {
        success{
          updateGitlabCommitStatus name: 'Checkout', state: 'success'
        }
        failure {
          updateGitlabCommitStatus name: 'Checkout', state: 'failed'
        }
      }
    }

/******************************************************************************************************************************/
/*                                          Start of build module generator stage                                             */
/******************************************************************************************************************************/
    stage('Build module generator') {
      steps {
        dir('03_ModuleGenerator') {
          echo 'echo Building Visual Studio Solution File'
          updateGitlabCommitStatus name: 'Build module generator', state: 'pending'

          bat '''
            "%MSBuildToolsPath%" Nvm.sln

            copy ModuleLibrary\\bin\\Debug\\NvMModuleLibrary.dll .\\..\\02_Software\\01_Generator\\NvmModuleLibrary.dll
            copy ModuleLibrary\\bin\\Debug\\OsarResources.dll .\\..\\02_Software\\01_Generator\\OsarResources.dll
            copy ModuleLibrary\\bin\\Debug\\OsarSourceFileLib.dll .\\..\\02_Software\\01_Generator\\OsarSourceFileLib.dll
            copy ModuleLibrary\\bin\\Debug\\RteLib.dll .\\..\\02_Software\\01_Generator\\RteLib.dll
          '''
        }
      }
      post {
        success{
          updateGitlabCommitStatus name: 'Build module generator', state: 'success'
        }
        failure {
          updateGitlabCommitStatus name: 'Build module generator', state: 'failed'
        }
      }
    }

/******************************************************************************************************************************/
/*                                                  Start of test stage                                                       */
/******************************************************************************************************************************/
    // stage('Test') {
    //   steps {
    //     echo 'Testing not configured / available'
    //     updateGitlabCommitStatus name: 'Test', state: 'pending'
    //     //TODO: Add Test State
    //   }
    //   post {
    //     success{
    //       updateGitlabCommitStatus name: 'Test', state: 'success'
    //     }
    //     failure {
    //       updateGitlabCommitStatus name: 'Test', state: 'failed'
    //     }
    //   }
    // }

/******************************************************************************************************************************/
/*                                                  Collect Delivery                                                          */
/******************************************************************************************************************************/
    stage('Collect delivery data') {
      steps {
        echo 'Collect delivery data'
        updateGitlabCommitStatus name: 'Collect delivery data', state: 'pending'

        //>>>>> Download word to pdf builder <<<<<
        script
        {
          withCredentials([usernamePassword(
            credentialsId: 'bca9b7d5-e21b-4205-8471-bf7dce5a432e',
            usernameVariable: 'USERNAME',
            passwordVariable: 'PASSWORD')])
          {
            def server = Artifactory.server 'Riddiks Artifactory Server'
            server.username = "${USERNAME}"
            server.password = "${PASSWORD}"

            def downloadSpec = """{
              "files": [
                {
                  "pattern": "tools/docto/latest/docto.exe",
                  "target": "01_Documentation/",
                  "flat": "true"
                }
              ]}"""
            server.download(downloadSpec)
          }

        }

                //>>>>> Create delivery folder <<<<<
        bat '''
          mkdir 99_Delivery
        '''

        //>>>>> Collect build artefacts <<<<<
        bat '''
          Xcopy /E /I 02_Software 99_Delivery
        '''

        //>>>>> Build PDF documentation <<<<<
        bat '''
          cd 01_Documentation
          docto.exe -WD -f TechnicalReference_Nvm.docx -o TechnicalReference_Nvm.pdf -t wdFormatPDF
          copy TechnicalReference_Nvm.pdf .\\..\\99_Delivery\\05_Doc\\TechnicalReference_Nvm.pdf
        '''
      }
      post {
        success{
          updateGitlabCommitStatus name: 'Collect delivery data', state: 'success'
        }
        failure {
          updateGitlabCommitStatus name: 'Collect delivery data', state: 'failed'
        }
      }
    }

/******************************************************************************************************************************/
/*                                                Start of deploy stage                                                       */
/******************************************************************************************************************************/
    stage('Deploy') {
      steps {
        dir('99_Delivery') {
          echo 'Start Deploying'
          updateGitlabCommitStatus name: 'Deploy', state: 'pending'

          script{
            String simpleBranchName = BRANCH_NAME.replace("%","").replace(" ","")
            println "simpleBranchName ${simpleBranchName}"

            withCredentials([usernamePassword(
              credentialsId: 'bca9b7d5-e21b-4205-8471-bf7dce5a432e',
              usernameVariable: 'USERNAME',
              passwordVariable: 'PASSWORD')])
            {
              def uploadSpec
              def buildInfo1
              def server = Artifactory.server 'Riddiks Artifactory Server'
              server.username = "${USERNAME}"
              server.password = "${PASSWORD}"

              if("master" == BRANCH_NAME)
              {
                uploadSpec = """{
                "files": [
                  {
                    "pattern": "./",
                    "recursive": "true",
                    "flat": false,
                    "target": "prj-open-system-architecture/Osar_EmbeddedSW/BSW/OSAR_Nvm/latest/"
                  }
                ]}"""
                buildInfo1 = server.upload uploadSpec
                server.publishBuildInfo(buildInfo1)
              }
              else if(true == BRANCH_NAME.contains("release/"))
              {
                uploadSpec = """{
                "files": [
                  {
                    "pattern": "./",
                    "recursive": "true",
                    "flat": false,
                    "target": "prj-open-system-architecture/Osar_EmbeddedSW/BSW/OSAR_Nvm/${simpleBranchName}/"
                  }
                ]}"""

                buildInfo1 = server.upload uploadSpec
                server.publishBuildInfo(buildInfo1)
              }
              else
              {
                println "Do not deploy for non master/release branch"
              }
            }
          }
        }
      }
      post {
        success{
          updateGitlabCommitStatus name: 'Deploy', state: 'success'
        }
        failure {
          updateGitlabCommitStatus name: 'Deploy', state: 'failed'
        }
      }
    }
  }
}