
set "basePath=%~dp0.\..\02_Software\"
set "cfgFilePath=.\01_Generator\NvmModuleCfg.xml"
set "libFilePath=.\01_Generator\NvmModuleLibrary.dll"

set cfgFilePathStr=%basePath%%cfgFilePath%
set libFilePathStr=%basePath%%libFilePath%

OsarStandaloneModuleGenerator.exe -m %basePath% -c %cfgFilePathStr% -l %libFilePathStr% -g