# OSAR_BSW_Nvm

## General
The Nvm Modul abstracts the different eeprom driver modules.

## Abbreviations:
OSAR == Open System ARchitecture
Crc == Cyclic Redundancy Check
Fls == Flash
FlsEep == Flash EEProm Emulation
NvM == None volatile Memory

## Useful Links:
- [Overall OSAR-Artifactory](http://riddiks.ddns.net:8082/artifactory/webapp/#/artifacts/browse/tree/General/prj-open-system-architecture)
- [OSAR - NvM releases](http://riddiks.ddns.net:8082/artifactory/list/prj-open-system-architecture/Osar_EmbeddedSW/BSW/OSAR_Nvm/)
- [OSAR - Documents](http://riddiks.ddns.net:8082/artifactory/list/prj-open-system-architecture/Osar_Documents/)
- [OSAR - Hyperspace](http://riddiks.ddns.net:8082/artifactory/list/prj-open-system-architecture/Osar_Hyperspace/)
  - Main-Tool to setup an OSAR Embedded Software Project.
- [OSAR - Tools](http://riddiks.ddns.net:8082/artifactory/list/prj-open-system-architecture/Osar_Tools/)
