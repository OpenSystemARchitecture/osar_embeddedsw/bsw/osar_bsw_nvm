/*****************************************************************************************************************************
* @file        Stubs.c                                                                                                      *
* @author      OSAR Team                                                                                                    *
* @date        20.02.2018 10:49:55                                                                                          *
* @brief       Implementation of functionalities from the "Stubs" module.                                                   *
*****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 Start of include area                        << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#include "stubs.h"
#include <vcruntime.h>
#include "cmocka.h"
#include "Std_Types.h"
#include "FlsEep.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                  End of include area                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of private data types                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of private data types                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            Start of private definitions area                 << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of private definitions area                  << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of global variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of global variables                      << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of private variables                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of private variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>           Start of private function prototypes               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            End of private function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>        Start of private function implementation              << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>         End of private function implementation               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>          Start of public function implementation             << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
Std_ReturnType Det_ReportError(uint8 moduleId, uint8 errorId)
{
  function_called();
  return E_NOT_OK;
}


/**
* @brief           Trigger a write action into the Flash
* @param[in]       uint16 FlsEep configured block id
* @param[in]       uin8* pointer to data which shall be written
* @retval          Std_ReturnType
*                  > E_OK        >> Request accepted
*                  > E_NOT_OK    >> Request failed.
* @details         Interface function to trigger a write action. This Function dose not perform the write action. The status
*                  of the active block has to be additional requested.
*/
Std_ReturnType FlsEep_RequestWriteData(uint16 flsBlockId, uint8* pWriteData)
{
  function_called();
  //TODO: Add Test Implementation
  return E_OK;
}

/**
* @brief           Trigger a read action into the Flash
* @param[in]       uint16 FlsEep configured block id
* @param[in]       uint8* pointer to data where the read data shall be stored
* @retval          Std_ReturnType
*                  > E_OK        >> Request accepted
*                  > E_NOT_OK    >> Request failed.
* @details         Interface function to trigger a read action. This Function dose not perform the write action. The status
*                  of the active block has to be additional requested. The FlsEep module would write the read data into the given pointer location.
*/
Std_ReturnType FlsEep_RequestReadData(uint16 flsBlockId, uint8* pReadData)
{
  function_called();
  //TODO: Add Test Implementation
  return E_OK;
}

/**
* @brief           Request the actual block status
* @param[in]       uint16 FlsEep configured block id
* @retval          Data_StatusType
*                  > DATA_E_OK            >> Block is valid
*                  > DATA_E_PENDING       >> Block processing is still in progress
*                  > DATA_E_INVALIDATE    >> Block data is not available
*                  > DATA_E_SKIPPED       >> Request failed / not accepted
* @details         Interface function to request the actual block status.
*/
Data_StatusType FlsEep_RequestBlockStatus(uint16 flsBlockId)
{
  function_called();
  //TODO: Add Test Implementation
  return E_OK;
}

/**
* @brief           Request the actual module status
* @param[in]       None
* @retval          FlsEep_ReturnType
*                  > E_OK            >> Module is in idle mode
*                  > E_PENDING       >> Module is still processing data
*                  > E_NOT_OK        >> Module is in safe state
* @details         Interface function to request the actual module status.
*/
Std_ReturnType FlsEep_RequestModuleStatus(void)
{
  function_called();
  //TODO: Add Test Implementation
  return E_OK;
}

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>          End of public function implementation               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/**
* @}
*/

