/*****************************************************************************************************************************
 * @file        Nvm_Int.h                                                                                                    *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        25.08.2018 15:58:25                                                                                          *
 * @brief       Implementation of definitions / interface function prototypes / datatypes and generic module interface       *
 *              informations of the "Nvm" module.                                                                            *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
 * @libNote     OSAR Source File Library v.1.0.0.0                                                                           *
*****************************************************************************************************************************/
#ifndef __NVM_INT_H
#define __NVM_INT_H
/**
 * @defgroup Nvm TODO >> Add module description for doxygen
 */

/**
 * @addtogroup Nvm
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 Start of include area                        << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#include "Nvm_Types.h"
#include "Nvm_PBCfg.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                  End of include area                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of public data types                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of public data types                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            Start of public definition area                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of public definition area                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of global variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of global variables                      << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>           Start of public function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/**
 * @brief           Module global initialization function.
 * @param[in]       None
 * @retval          None
 * @details         This function shall be called before using the module and after the memory initialization.
 */
void Nvm_Int_Init( void );

/**
* @brief           Function to modify the interface block status
* @param[in]       uint16 id of the corresponding nvm block
* @param[in]       Nvm_BlockStatusType new status the corresponding nvm block
*                  > NVM_BLK_OK                  >> Nvm block is valid
*                  > NVM_BLK_PENDING             >> Nvm block is ia current in progress
*                  > NVM_BLK_SKIPPED             >> Nvm block processing has been skipped
*                  > NVM_BLK_INTEGRITY_FAILED    >> Nvm block data is invalid
*                  > NVM_BLK_INVALIDATE          >> Nvm block not found in memory
* @retval          Nvm_ReturnType
*                  > NVM_E_OK
*                  > NVM_E_NOT_OK
*/
Nvm_ReturnType Nvm_Int_SetNvmBlockStatus(uint16 nvmBlockId, Nvm_BlockStatusType nvmBlockStatus);

/**
* @brief           Function to get the interface block status
* @param[in]       uint16 id of the corresponding nvm block
* @param[out]      Nvm_BlockStatusType* ponter to store the corresponding nvm block status
*                  > NVM_BLK_OK                  >> Nvm block is valid
*                  > NVM_BLK_PENDING             >> Nvm block is ia current in progress
*                  > NVM_BLK_SKIPPED             >> Nvm block processing has been skipped
*                  > NVM_BLK_INTEGRITY_FAILED    >> Nvm block data is invalid
*                  > NVM_BLK_INVALIDATE          >> Nvm block not found in memory
* @retval          Nvm_ReturnType
*                  > NVM_E_OK
*                  > NVM_E_NOT_OK
*/
Nvm_ReturnType Nvm_Int_GetNvmBlockStatus(uint16 nvmBlockId, Nvm_BlockStatusType* nvmBlockStatus);

/**
* @brief           Function to process active / peding data element in request buffer
* @param[in]       None
* @retval          None
* @details         Performed actions:
*                  > Process complete request buffer
*                  >> Check pending request status in lower interfac module
*                  >>> Read requests >> Evaluate data
*                  >>> Write requests >> OK
*                  >> Update global status buffer
*                  >> Invalidate buffer element
*/
void Nvm_Int_ProcessActiveRequests(void);

/**
* @brief           Function to process new data element in request buffer
* @param[in]       None
* @retval          None
* @details         Performed actions:
*                  > Process request buffer till lower if module buffer is full
*                  >> Set new request elements to lower if module
*                  >> Update global status buffer
*/
void Nvm_Int_ProcessNewRequests(void);
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of public function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/**
 * @}
 */

#endif /* __NVM_INT_H */
