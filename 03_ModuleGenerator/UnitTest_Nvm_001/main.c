/*****************************************************************************************************************************
* @file        main.cpp                                                                                                     *
* @author      OSAR Team                                                                                                    *
* @date        20.02.2018 10:49:55                                                                                          *
* @brief       Implementation of unit test functionalities from the "Dummy" module.                                         *
*****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 Start of include area                        << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <vcruntime.h>
#include <cmocka.h>
#include <stdio.h>
#include "stubs.h"

#include "..\..\02_Software\04_StaticData\src\Nvm_RequestList.c"

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                  End of include area                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of private data types                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of private data types                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            Start of private definitions area                 << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of private definitions area                  << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/



/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>           Start of private function prototypes               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/* Unit tests of Group x */
static void UTest_Nvm_RequestList_Init(void **state);
static void UTest_Nvm_RequestList_InsertWrite_001(void **state);
static void UTest_Nvm_RequestList_InsertWrite_002(void **state);
static void UTest_Nvm_RequestList_InsertWrite_003(void **state);
static void UTest_Nvm_RequestList_InsertRead_001(void **state);
static void UTest_Nvm_RequestList_InsertRead_002(void **state);
static void UTest_Nvm_RequestList_InsertRead_003(void **state);
static void UTest_Nvm_RequestList_InsertErase_001(void **state);
static void UTest_Nvm_RequestList_InsertErase_002(void **state);
static void UTest_Nvm_RequestList_InsertErase_003(void **state);
static void UTest_Nvm_RequestList_InsertMultiple(void **state);


/* Unit tests of Group x */
/*...*/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            End of private function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of global variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
extern Nvm_RequestListEntryType nvmRequestList[];

/* Creating Unit Test Group ... */
const struct CMUnitTest tests[] = {
  cmocka_unit_test(UTest_Nvm_RequestList_Init),
  cmocka_unit_test(UTest_Nvm_RequestList_InsertWrite_001),
  cmocka_unit_test(UTest_Nvm_RequestList_InsertWrite_002),
  cmocka_unit_test(UTest_Nvm_RequestList_InsertWrite_003),
  cmocka_unit_test(UTest_Nvm_RequestList_InsertRead_001),
  cmocka_unit_test(UTest_Nvm_RequestList_InsertRead_002),
  cmocka_unit_test(UTest_Nvm_RequestList_InsertRead_003),
  cmocka_unit_test(UTest_Nvm_RequestList_InsertErase_001),
  cmocka_unit_test(UTest_Nvm_RequestList_InsertErase_002),
  cmocka_unit_test(UTest_Nvm_RequestList_InsertErase_003),
  cmocka_unit_test(UTest_Nvm_RequestList_InsertMultiple),
};
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of global variables                      << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of private variables                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of private variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>        Start of private function implementation              << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/*--------------------------------------------------------------------------------------------------------------------------*/
/*------------------------------------------------ Unit Tests of group ... -------------------------------------------------*/
/*--------------------------------------------------------------------------------------------------------------------------*/
static void UTest_Nvm_RequestList_Init(void **state)
{
  uint8 idx = 0;
  uint16 idx2 = 0;

  /*----------------------------------------------------------------------------*/
  /*------------------------------- Test Setup ---------------------------------*/
  /*----------------------------------------------------------------------------*/
  printf("[ DESCRIP  ] >> Check the init functionality of the Nvm sub module \"RequestList\"<< \r\n");

  for (idx = 0; idx < NVM_REQUEST_LIST_SIZE; idx++)
  {
    nvmRequestList[idx].nvmBlockId = 0xAA99;
    nvmRequestList[idx].nvmRequestStatus = NVM_BLK_INVALIDATE;
    nvmRequestList[idx].nvmRequestType = NVM_REQ_WRITE;
    nvmRequestList[idx].nvmSafe = 0xBB;
    nvmRequestList[idx].requestDataPointer = (void*)0x012345678;
    nvmRequestList[idx].nvmData[0] = 0xCC;
    nvmRequestList[idx].nvmData[NVM_MAX_REQUEST_DATA_SIZE-1] = 0x00;
  }

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Execution ------------------------------*/
  /*----------------------------------------------------------------------------*/
  Nvm_RequestList_Init();

  /* Check Data */
  for (idx = 0; idx < NVM_REQUEST_LIST_SIZE; idx++)
  {
    printf("[          ] >> Check Element \r\n");
    assert_int_equal(nvmRequestList[idx].nvmBlockId, 0xFFFF);
    assert_int_equal(nvmRequestList[idx].nvmRequestStatus, NVM_BLK_EMPTY);
    assert_int_equal(nvmRequestList[idx].requestDataPointer, NULL_PTR);
    assert_int_equal(nvmRequestList[idx].nvmRequestType, NVM_REQ_ERASE);
    assert_int_equal(nvmRequestList[idx].nvmSafe, 0x00);
    printf("[          ] >> ");
    for (idx2 = 0; idx2 < NVM_MAX_REQUEST_DATA_SIZE; idx2++)
    {
      printf(".");
      assert_int_equal(nvmRequestList[idx].nvmData[idx2], 0x00);
    }
    printf("\r\n");
  }
  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Cleanup --------------------------------*/
  /*----------------------------------------------------------------------------*/
  (void)state; /* unused */
}

static void UTest_Nvm_RequestList_InsertWrite_001(void **state)
{
  uint8 idx = 0;
  uint16 idx2 = 0;
  Nvm_ReturnType retVal;
  uint8 testWriteData[10] = { 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA };
  /*----------------------------------------------------------------------------*/
  /*------------------------------- Test Setup ---------------------------------*/
  /*----------------------------------------------------------------------------*/
  printf("[ DESCRIP  ] >> Unit test check the request buffer insert new write request all ok with CRC32 request << \r\n");

  Nvm_RequestList_Init();

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Execution ------------------------------*/
  /*----------------------------------------------------------------------------*/
  retVal = Nvm_RequestList_InsertANewWriteRequest(NVM_BLK_ID_DEMO_BLK, &testWriteData);

  assert_int_equal(NVM_E_OK, retVal);
  assert_int_equal(nvmRequestList[0].nvmBlockId, NVM_BLK_ID_DEMO_BLK);
  assert_int_equal(nvmRequestList[0].nvmRequestStatus, NVM_BLK_WAITING);
  assert_int_equal(nvmRequestList[0].requestDataPointer, &testWriteData);
  assert_int_equal(nvmRequestList[0].nvmRequestType, NVM_REQ_WRITE);
  assert_int_equal(nvmRequestList[0].nvmSafe, 0x00);

  assert_int_equal(nvmRequestList[0].nvmData[0], 0x11);
  assert_int_equal(nvmRequestList[0].nvmData[1], 0x22);
  assert_int_equal(nvmRequestList[0].nvmData[2], 0x33);
  assert_int_equal(nvmRequestList[0].nvmData[3], 0x44);
  assert_int_equal(nvmRequestList[0].nvmData[4], 0x55);
  assert_int_equal(nvmRequestList[0].nvmData[5], 0x66);
  assert_int_equal(nvmRequestList[0].nvmData[6], 0x77);
  assert_int_equal(nvmRequestList[0].nvmData[7], 0x88);
  assert_int_equal(nvmRequestList[0].nvmData[8], 0x99);
  assert_int_equal(nvmRequestList[0].nvmData[9], 0xAA);
  assert_int_equal(nvmRequestList[0].nvmData[10], 0x60);
  assert_int_equal(nvmRequestList[0].nvmData[11], 0x9F);
  assert_int_equal(nvmRequestList[0].nvmData[12], 0x98);
  assert_int_equal(nvmRequestList[0].nvmData[13], 0xF9);
  assert_int_equal(nvmRequestList[0].nvmData[14], 0x00);
  assert_int_equal(nvmRequestList[0].nvmData[15], 0x00);
  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Cleanup --------------------------------*/
  /*----------------------------------------------------------------------------*/
  (void)state; /* unused */
}

static void UTest_Nvm_RequestList_InsertWrite_002(void **state)
{
  uint8 idx = 0;
  uint16 idx2 = 0;
  Nvm_ReturnType retVal;
  uint8 testWriteData[10] = { 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA };
  /*----------------------------------------------------------------------------*/
  /*------------------------------- Test Setup ---------------------------------*/
  /*----------------------------------------------------------------------------*/
  printf("[ DESCRIP  ] >> Unit test check the request buffer insert new write request all ok with NO CRC request << \r\n");

  Nvm_RequestList_Init();

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Execution ------------------------------*/
  /*----------------------------------------------------------------------------*/
  retVal = Nvm_RequestList_InsertANewWriteRequest(NVM_BLK_ID_DEMO_BLK2, &testWriteData);

  assert_int_equal(NVM_E_OK, retVal);
  assert_int_equal(nvmRequestList[0].nvmBlockId, NVM_BLK_ID_DEMO_BLK2);
  assert_int_equal(nvmRequestList[0].nvmRequestStatus, NVM_BLK_WAITING);
  assert_int_equal(nvmRequestList[0].requestDataPointer, &testWriteData);
  assert_int_equal(nvmRequestList[0].nvmRequestType, NVM_REQ_WRITE);
  assert_int_equal(nvmRequestList[0].nvmSafe, 0x00);

  assert_int_equal(nvmRequestList[0].nvmData[0], 0x11);
  assert_int_equal(nvmRequestList[0].nvmData[1], 0x22);
  assert_int_equal(nvmRequestList[0].nvmData[2], 0x33);
  assert_int_equal(nvmRequestList[0].nvmData[3], 0x44);
  assert_int_equal(nvmRequestList[0].nvmData[4], 0x55);
  assert_int_equal(nvmRequestList[0].nvmData[5], 0x66);
  assert_int_equal(nvmRequestList[0].nvmData[6], 0x77);
  assert_int_equal(nvmRequestList[0].nvmData[7], 0x88);
  assert_int_equal(nvmRequestList[0].nvmData[8], 0x99);
  assert_int_equal(nvmRequestList[0].nvmData[9], 0xAA);
  assert_int_equal(nvmRequestList[0].nvmData[10], 0x00);
  assert_int_equal(nvmRequestList[0].nvmData[11], 0x00);
  assert_int_equal(nvmRequestList[0].nvmData[12], 0x00);
  assert_int_equal(nvmRequestList[0].nvmData[13], 0x00);
  assert_int_equal(nvmRequestList[0].nvmData[14], 0x00);
  assert_int_equal(nvmRequestList[0].nvmData[15], 0x00);
  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Cleanup --------------------------------*/
  /*----------------------------------------------------------------------------*/
  (void)state; /* unused */
}

static void UTest_Nvm_RequestList_InsertWrite_003(void **state)
{
  uint8 idx = 0;
  uint16 idx2 = 0;
  Nvm_ReturnType retVal;
  uint8 testWriteData[10] = { 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA };
  /*----------------------------------------------------------------------------*/
  /*------------------------------- Test Setup ---------------------------------*/
  /*----------------------------------------------------------------------------*/
  printf("[ DESCRIP  ] >> Unit test check the request buffer insert new write request but list is full << \r\n");

  Nvm_RequestList_Init();

  for (idx = 0; idx < NVM_REQUEST_LIST_SIZE; idx++)
  {
    nvmRequestList[idx].nvmBlockId = 0xAA99;
    nvmRequestList[idx].nvmRequestStatus = NVM_BLK_WAITING;
    nvmRequestList[idx].nvmRequestType = NVM_REQ_WRITE;
    nvmRequestList[idx].nvmSafe = 0xBB;
    nvmRequestList[idx].requestDataPointer = (void*)0x012345678;
    nvmRequestList[idx].nvmData[0] = 0xCC;
    nvmRequestList[idx].nvmData[NVM_MAX_REQUEST_DATA_SIZE - 1] = 0x00;
  }

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Execution ------------------------------*/
  /*----------------------------------------------------------------------------*/
  retVal = Nvm_RequestList_InsertANewWriteRequest(NVM_BLK_ID_DEMO_BLK2, &testWriteData);

  assert_int_equal(NVM_E_SKIPPED, retVal);

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Cleanup --------------------------------*/
  /*----------------------------------------------------------------------------*/
  (void)state; /* unused */
}


static void UTest_Nvm_RequestList_InsertRead_001(void **state)
{
  uint8 idx = 0;
  uint16 idx2 = 0;
  Nvm_ReturnType retVal;
  uint8 testWriteData[10] = { 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA };
  /*----------------------------------------------------------------------------*/
  /*------------------------------- Test Setup ---------------------------------*/
  /*----------------------------------------------------------------------------*/
  printf("[ DESCRIP  ] >> Unit test check the request buffer insert new read request all ok with CRC32 request << \r\n");

  Nvm_RequestList_Init();

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Execution ------------------------------*/
  /*----------------------------------------------------------------------------*/
  retVal = Nvm_RequestList_InsertANewReadRequest(NVM_BLK_ID_DEMO_BLK, &testWriteData);

  assert_int_equal(NVM_E_OK, retVal);
  assert_int_equal(nvmRequestList[0].nvmBlockId, NVM_BLK_ID_DEMO_BLK);
  assert_int_equal(nvmRequestList[0].nvmRequestStatus, NVM_BLK_WAITING);
  assert_int_equal(nvmRequestList[0].requestDataPointer, &testWriteData);
  assert_int_equal(nvmRequestList[0].nvmRequestType, NVM_REQ_READ);
  assert_int_equal(nvmRequestList[0].nvmSafe, 0x00);

  assert_int_equal(nvmRequestList[0].nvmData[0], 0x00);
  assert_int_equal(nvmRequestList[0].nvmData[1], 0x00);
  assert_int_equal(nvmRequestList[0].nvmData[2], 0x00);
  assert_int_equal(nvmRequestList[0].nvmData[3], 0x00);
  assert_int_equal(nvmRequestList[0].nvmData[4], 0x00);
  assert_int_equal(nvmRequestList[0].nvmData[5], 0x00);
  assert_int_equal(nvmRequestList[0].nvmData[6], 0x00);
  assert_int_equal(nvmRequestList[0].nvmData[7], 0x00);
  assert_int_equal(nvmRequestList[0].nvmData[8], 0x00);
  assert_int_equal(nvmRequestList[0].nvmData[9], 0x00);
  assert_int_equal(nvmRequestList[0].nvmData[10], 0x00);
  assert_int_equal(nvmRequestList[0].nvmData[11], 0x00);
  assert_int_equal(nvmRequestList[0].nvmData[12], 0x00);
  assert_int_equal(nvmRequestList[0].nvmData[13], 0x00);
  assert_int_equal(nvmRequestList[0].nvmData[14], 0x00);
  assert_int_equal(nvmRequestList[0].nvmData[15], 0x00);
  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Cleanup --------------------------------*/
  /*----------------------------------------------------------------------------*/
  (void)state; /* unused */
}

static void UTest_Nvm_RequestList_InsertRead_002(void **state)
{
  uint8 idx = 0;
  uint16 idx2 = 0;
  Nvm_ReturnType retVal;
  uint8 testWriteData[10] = { 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA };
  /*----------------------------------------------------------------------------*/
  /*------------------------------- Test Setup ---------------------------------*/
  /*----------------------------------------------------------------------------*/
  printf("[ DESCRIP  ] >> Unit test check the request buffer insert new read request all ok with NO CRC request << \r\n");

  Nvm_RequestList_Init();

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Execution ------------------------------*/
  /*----------------------------------------------------------------------------*/
  retVal = Nvm_RequestList_InsertANewReadRequest(NVM_BLK_ID_DEMO_BLK2, &testWriteData);

  assert_int_equal(NVM_E_OK, retVal);
  assert_int_equal(nvmRequestList[0].nvmBlockId, NVM_BLK_ID_DEMO_BLK2);
  assert_int_equal(nvmRequestList[0].nvmRequestStatus, NVM_BLK_WAITING);
  assert_int_equal(nvmRequestList[0].requestDataPointer, &testWriteData);
  assert_int_equal(nvmRequestList[0].nvmRequestType, NVM_REQ_READ);
  assert_int_equal(nvmRequestList[0].nvmSafe, 0x00);

  assert_int_equal(nvmRequestList[0].nvmData[0], 0x00);
  assert_int_equal(nvmRequestList[0].nvmData[1], 0x00);
  assert_int_equal(nvmRequestList[0].nvmData[2], 0x00);
  assert_int_equal(nvmRequestList[0].nvmData[3], 0x00);
  assert_int_equal(nvmRequestList[0].nvmData[4], 0x00);
  assert_int_equal(nvmRequestList[0].nvmData[5], 0x00);
  assert_int_equal(nvmRequestList[0].nvmData[6], 0x00);
  assert_int_equal(nvmRequestList[0].nvmData[7], 0x00);
  assert_int_equal(nvmRequestList[0].nvmData[8], 0x00);
  assert_int_equal(nvmRequestList[0].nvmData[9], 0x00);
  assert_int_equal(nvmRequestList[0].nvmData[10], 0x00);
  assert_int_equal(nvmRequestList[0].nvmData[11], 0x00);
  assert_int_equal(nvmRequestList[0].nvmData[12], 0x00);
  assert_int_equal(nvmRequestList[0].nvmData[13], 0x00);
  assert_int_equal(nvmRequestList[0].nvmData[14], 0x00);
  assert_int_equal(nvmRequestList[0].nvmData[15], 0x00);
  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Cleanup --------------------------------*/
  /*----------------------------------------------------------------------------*/
  (void)state; /* unused */
}

static void UTest_Nvm_RequestList_InsertRead_003(void **state)
{
  uint8 idx = 0;
  uint16 idx2 = 0;
  Nvm_ReturnType retVal;
  uint8 testWriteData[10] = { 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA };
  /*----------------------------------------------------------------------------*/
  /*------------------------------- Test Setup ---------------------------------*/
  /*----------------------------------------------------------------------------*/
  printf("[ DESCRIP  ] >> Unit test check the request buffer insert new read request but list is full << \r\n");

  Nvm_RequestList_Init();

  for (idx = 0; idx < NVM_REQUEST_LIST_SIZE; idx++)
  {
    nvmRequestList[idx].nvmBlockId = 0xAA99;
    nvmRequestList[idx].nvmRequestStatus = NVM_BLK_WAITING;
    nvmRequestList[idx].nvmRequestType = NVM_REQ_WRITE;
    nvmRequestList[idx].nvmSafe = 0xBB;
    nvmRequestList[idx].requestDataPointer = (void*)0x012345678;
    nvmRequestList[idx].nvmData[0] = 0xCC;
    nvmRequestList[idx].nvmData[NVM_MAX_REQUEST_DATA_SIZE - 1] = 0x00;
  }

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Execution ------------------------------*/
  /*----------------------------------------------------------------------------*/
  retVal = Nvm_RequestList_InsertANewReadRequest(NVM_BLK_ID_DEMO_BLK2, &testWriteData);

  assert_int_equal(NVM_E_SKIPPED, retVal);
  assert_int_equal(nvmRequestList[0].nvmBlockId, 0xAA99);
  assert_int_equal(nvmRequestList[0].nvmRequestStatus, NVM_BLK_WAITING);
  assert_int_equal(nvmRequestList[0].requestDataPointer, 0x012345678);
  assert_int_equal(nvmRequestList[0].nvmRequestType, NVM_REQ_WRITE);
  assert_int_equal(nvmRequestList[0].nvmSafe, 0xBB);

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Cleanup --------------------------------*/
  /*----------------------------------------------------------------------------*/
  (void)state; /* unused */
}


static void UTest_Nvm_RequestList_InsertErase_001(void **state)
{
  uint8 idx = 0;
  uint16 idx2 = 0;
  Nvm_ReturnType retVal;
  uint8 testWriteData[10] = { 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA };
  /*----------------------------------------------------------------------------*/
  /*------------------------------- Test Setup ---------------------------------*/
  /*----------------------------------------------------------------------------*/
  printf("[ DESCRIP  ] >> Unit test check the request buffer insert new erase request all ok with CRC32 request << \r\n");

  Nvm_RequestList_Init();

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Execution ------------------------------*/
  /*----------------------------------------------------------------------------*/
  retVal = Nvm_RequestList_InsertANewEraseRequest(NVM_BLK_ID_DEMO_BLK);

  assert_int_equal(NVM_E_OK, retVal);
  assert_int_equal(nvmRequestList[0].nvmBlockId, NVM_BLK_ID_DEMO_BLK);
  assert_int_equal(nvmRequestList[0].nvmRequestStatus, NVM_BLK_WAITING);
  assert_int_equal(nvmRequestList[0].requestDataPointer, NULL_PTR);
  assert_int_equal(nvmRequestList[0].nvmRequestType, NVM_REQ_ERASE);
  assert_int_equal(nvmRequestList[0].nvmSafe, 0x00);

  assert_int_equal(nvmRequestList[0].nvmData[0], 0x00);
  assert_int_equal(nvmRequestList[0].nvmData[1], 0x00);
  assert_int_equal(nvmRequestList[0].nvmData[2], 0x00);
  assert_int_equal(nvmRequestList[0].nvmData[3], 0x00);
  assert_int_equal(nvmRequestList[0].nvmData[4], 0x00);
  assert_int_equal(nvmRequestList[0].nvmData[5], 0x00);
  assert_int_equal(nvmRequestList[0].nvmData[6], 0x00);
  assert_int_equal(nvmRequestList[0].nvmData[7], 0x00);
  assert_int_equal(nvmRequestList[0].nvmData[8], 0x00);
  assert_int_equal(nvmRequestList[0].nvmData[9], 0x00);
  assert_int_equal(nvmRequestList[0].nvmData[10], 0x00);
  assert_int_equal(nvmRequestList[0].nvmData[11], 0x00);
  assert_int_equal(nvmRequestList[0].nvmData[12], 0x00);
  assert_int_equal(nvmRequestList[0].nvmData[13], 0x00);
  assert_int_equal(nvmRequestList[0].nvmData[14], 0x00);
  assert_int_equal(nvmRequestList[0].nvmData[15], 0x00);
  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Cleanup --------------------------------*/
  /*----------------------------------------------------------------------------*/
  (void)state; /* unused */
}

static void UTest_Nvm_RequestList_InsertErase_002(void **state)
{
  uint8 idx = 0;
  uint16 idx2 = 0;
  Nvm_ReturnType retVal;
  uint8 testWriteData[10] = { 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA };
  /*----------------------------------------------------------------------------*/
  /*------------------------------- Test Setup ---------------------------------*/
  /*----------------------------------------------------------------------------*/
  printf("[ DESCRIP  ] >> Unit test check the request buffer insert new erase request all ok with NO CRC request << \r\n");

  Nvm_RequestList_Init();

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Execution ------------------------------*/
  /*----------------------------------------------------------------------------*/
  retVal = Nvm_RequestList_InsertANewEraseRequest(NVM_BLK_ID_DEMO_BLK2);

  assert_int_equal(NVM_E_OK, retVal);
  assert_int_equal(nvmRequestList[0].nvmBlockId, NVM_BLK_ID_DEMO_BLK2);
  assert_int_equal(nvmRequestList[0].nvmRequestStatus, NVM_BLK_WAITING);
  assert_int_equal(nvmRequestList[0].requestDataPointer, NULL_PTR);
  assert_int_equal(nvmRequestList[0].nvmRequestType, NVM_REQ_ERASE);
  assert_int_equal(nvmRequestList[0].nvmSafe, 0x00);


  assert_int_equal(nvmRequestList[0].nvmData[0], 0x00);
  assert_int_equal(nvmRequestList[0].nvmData[1], 0x00);
  assert_int_equal(nvmRequestList[0].nvmData[2], 0x00);
  assert_int_equal(nvmRequestList[0].nvmData[3], 0x00);
  assert_int_equal(nvmRequestList[0].nvmData[4], 0x00);
  assert_int_equal(nvmRequestList[0].nvmData[5], 0x00);
  assert_int_equal(nvmRequestList[0].nvmData[6], 0x00);
  assert_int_equal(nvmRequestList[0].nvmData[7], 0x00);
  assert_int_equal(nvmRequestList[0].nvmData[8], 0x00);
  assert_int_equal(nvmRequestList[0].nvmData[9], 0x00);
  assert_int_equal(nvmRequestList[0].nvmData[10], 0x00);
  assert_int_equal(nvmRequestList[0].nvmData[11], 0x00);
  assert_int_equal(nvmRequestList[0].nvmData[12], 0x00);
  assert_int_equal(nvmRequestList[0].nvmData[13], 0x00);
  assert_int_equal(nvmRequestList[0].nvmData[14], 0x00);
  assert_int_equal(nvmRequestList[0].nvmData[15], 0x00);
  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Cleanup --------------------------------*/
  /*----------------------------------------------------------------------------*/
  (void)state; /* unused */
}

static void UTest_Nvm_RequestList_InsertErase_003(void **state)
{
  uint8 idx = 0;
  uint16 idx2 = 0;
  Nvm_ReturnType retVal;
  uint8 testWriteData[10] = { 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA };
  /*----------------------------------------------------------------------------*/
  /*------------------------------- Test Setup ---------------------------------*/
  /*----------------------------------------------------------------------------*/
  printf("[ DESCRIP  ] >> Unit test check the request buffer insert new erase request but list is full << \r\n");

  Nvm_RequestList_Init();

  for (idx = 0; idx < NVM_REQUEST_LIST_SIZE; idx++)
  {
    nvmRequestList[idx].nvmBlockId = 0xAA99;
    nvmRequestList[idx].nvmRequestStatus = NVM_BLK_WAITING;
    nvmRequestList[idx].nvmRequestType = NVM_REQ_WRITE;
    nvmRequestList[idx].nvmSafe = 0xBB;
    nvmRequestList[idx].requestDataPointer = (void*)0x012345678;
    nvmRequestList[idx].nvmData[0] = 0xCC;
    nvmRequestList[idx].nvmData[NVM_MAX_REQUEST_DATA_SIZE - 1] = 0x00;
  }

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Execution ------------------------------*/
  /*----------------------------------------------------------------------------*/
  retVal = Nvm_RequestList_InsertANewEraseRequest(NVM_BLK_ID_DEMO_BLK);

  assert_int_equal(NVM_E_SKIPPED, retVal);
  assert_int_equal(nvmRequestList[0].nvmBlockId, 0xAA99);
  assert_int_equal(nvmRequestList[0].nvmRequestStatus, NVM_BLK_WAITING);
  assert_int_equal(nvmRequestList[0].requestDataPointer, 0x012345678);
  assert_int_equal(nvmRequestList[0].nvmRequestType, NVM_REQ_WRITE);
  assert_int_equal(nvmRequestList[0].nvmSafe, 0xBB);

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Cleanup --------------------------------*/
  /*----------------------------------------------------------------------------*/
  (void)state; /* unused */
}



static void UTest_Nvm_RequestList_InsertMultiple(void **state)
{
  uint8 idx = 0;
  uint16 idx2 = 0;
  Nvm_ReturnType retVal;
  uint8 testWriteData[10] = { 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA };
  /*----------------------------------------------------------------------------*/
  /*------------------------------- Test Setup ---------------------------------*/
  /*----------------------------------------------------------------------------*/
  printf("[ DESCRIP  ] >> Unit test check the request buffer insert mutiple request << \r\n");

  Nvm_RequestList_Init();

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Execution ------------------------------*/
  /*----------------------------------------------------------------------------*/
  retVal = Nvm_RequestList_InsertANewWriteRequest(NVM_BLK_ID_DEMO_BLK, &testWriteData);

  assert_int_equal(NVM_E_OK, retVal);
  assert_int_equal(nvmRequestList[0].nvmBlockId, NVM_BLK_ID_DEMO_BLK);
  assert_int_equal(nvmRequestList[0].nvmRequestStatus, NVM_BLK_WAITING);
  assert_int_equal(nvmRequestList[0].requestDataPointer, &testWriteData);
  assert_int_equal(nvmRequestList[0].nvmRequestType, NVM_REQ_WRITE);
  assert_int_equal(nvmRequestList[0].nvmSafe, 0x00);

  assert_int_equal(nvmRequestList[0].nvmData[0], 0x11);
  assert_int_equal(nvmRequestList[0].nvmData[1], 0x22);
  assert_int_equal(nvmRequestList[0].nvmData[2], 0x33);
  assert_int_equal(nvmRequestList[0].nvmData[3], 0x44);
  assert_int_equal(nvmRequestList[0].nvmData[4], 0x55);
  assert_int_equal(nvmRequestList[0].nvmData[5], 0x66);
  assert_int_equal(nvmRequestList[0].nvmData[6], 0x77);
  assert_int_equal(nvmRequestList[0].nvmData[7], 0x88);
  assert_int_equal(nvmRequestList[0].nvmData[8], 0x99);
  assert_int_equal(nvmRequestList[0].nvmData[9], 0xAA);
  assert_int_equal(nvmRequestList[0].nvmData[10], 0x60);
  assert_int_equal(nvmRequestList[0].nvmData[11], 0x9F);
  assert_int_equal(nvmRequestList[0].nvmData[12], 0x98);
  assert_int_equal(nvmRequestList[0].nvmData[13], 0xF9);
  assert_int_equal(nvmRequestList[0].nvmData[14], 0x00);
  assert_int_equal(nvmRequestList[0].nvmData[15], 0x00);



  retVal = Nvm_RequestList_InsertANewReadRequest(NVM_BLK_ID_DEMO_BLK2, &testWriteData);

  assert_int_equal(NVM_E_OK, retVal);
  assert_int_equal(nvmRequestList[1].nvmBlockId, NVM_BLK_ID_DEMO_BLK2);
  assert_int_equal(nvmRequestList[1].nvmRequestStatus, NVM_BLK_WAITING);
  assert_int_equal(nvmRequestList[1].requestDataPointer, &testWriteData);
  assert_int_equal(nvmRequestList[1].nvmRequestType, NVM_REQ_READ);
  assert_int_equal(nvmRequestList[1].nvmSafe, 0x00);

  assert_int_equal(nvmRequestList[1].nvmData[0], 0x00);
  assert_int_equal(nvmRequestList[1].nvmData[1], 0x00);
  assert_int_equal(nvmRequestList[1].nvmData[2], 0x00);
  assert_int_equal(nvmRequestList[1].nvmData[3], 0x00);
  assert_int_equal(nvmRequestList[1].nvmData[4], 0x00);
  assert_int_equal(nvmRequestList[1].nvmData[5], 0x00);
  assert_int_equal(nvmRequestList[1].nvmData[6], 0x00);
  assert_int_equal(nvmRequestList[1].nvmData[7], 0x00);
  assert_int_equal(nvmRequestList[1].nvmData[8], 0x00);
  assert_int_equal(nvmRequestList[1].nvmData[9], 0x00);
  assert_int_equal(nvmRequestList[1].nvmData[10], 0x00);
  assert_int_equal(nvmRequestList[1].nvmData[11], 0x00);
  assert_int_equal(nvmRequestList[1].nvmData[12], 0x00);
  assert_int_equal(nvmRequestList[1].nvmData[13], 0x00);
  assert_int_equal(nvmRequestList[1].nvmData[14], 0x00);
  assert_int_equal(nvmRequestList[1].nvmData[15], 0x00);



  retVal = Nvm_RequestList_InsertANewEraseRequest(NVM_BLK_ID_DEMO_BLK);

  assert_int_equal(NVM_E_OK, retVal);
  assert_int_equal(nvmRequestList[2].nvmBlockId, NVM_BLK_ID_DEMO_BLK);
  assert_int_equal(nvmRequestList[2].nvmRequestStatus, NVM_BLK_WAITING);
  assert_int_equal(nvmRequestList[2].requestDataPointer, NULL_PTR);
  assert_int_equal(nvmRequestList[2].nvmRequestType, NVM_REQ_ERASE);
  assert_int_equal(nvmRequestList[2].nvmSafe, 0x00);

  assert_int_equal(nvmRequestList[2].nvmData[0], 0x00);
  assert_int_equal(nvmRequestList[2].nvmData[1], 0x00);
  assert_int_equal(nvmRequestList[2].nvmData[2], 0x00);
  assert_int_equal(nvmRequestList[2].nvmData[3], 0x00);
  assert_int_equal(nvmRequestList[2].nvmData[4], 0x00);
  assert_int_equal(nvmRequestList[2].nvmData[5], 0x00);
  assert_int_equal(nvmRequestList[2].nvmData[6], 0x00);
  assert_int_equal(nvmRequestList[2].nvmData[7], 0x00);
  assert_int_equal(nvmRequestList[2].nvmData[8], 0x00);
  assert_int_equal(nvmRequestList[2].nvmData[9], 0x00);
  assert_int_equal(nvmRequestList[2].nvmData[10], 0x00);
  assert_int_equal(nvmRequestList[2].nvmData[11], 0x00);
  assert_int_equal(nvmRequestList[2].nvmData[12], 0x00);
  assert_int_equal(nvmRequestList[2].nvmData[13], 0x00);
  assert_int_equal(nvmRequestList[2].nvmData[14], 0x00);
  assert_int_equal(nvmRequestList[2].nvmData[15], 0x00);



  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Cleanup --------------------------------*/
  /*----------------------------------------------------------------------------*/
  (void)state; /* unused */
}


/*--------------------------------------------------------------------------------------------------------------------------*/
/*------------------------------------------------ Unit Tests of group ... -------------------------------------------------*/
/*--------------------------------------------------------------------------------------------------------------------------*/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>         End of private function implementation               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>          Start of public function implementation             << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#define CNT_OF_TEST_GROUPS 1
int main()
{
  int result[CNT_OF_TEST_GROUPS], idx = 0;
  /* Setup Console for Test Output */
  printf("Startup of Module Tests for Module xyz. \r\nTest Framework: CMocka 1.1.1 \r\n");

  /*#################################### Run CMocka group tests #############################################*/
  result[0] = cmocka_run_group_tests(tests, NULL, NULL);

  /* Print result */
  printf("\r\n\r\n=====================================================================================\r\n");
  printf("Test summary:\r\n\r\n");
  for (idx = 0; idx < CNT_OF_TEST_GROUPS; idx++)
  {
    printf("Testgroup %d >> Cnt of errors: %d\r\n", idx, result[idx]);
  }
  printf("=====================================================================================\r\n\r\n");

  /* wait for user key to shutdown system */
  printf("Pres any key to exit test environment \r\n");
  
  getch();
    return 0;
}
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>          End of public function implementation               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

