/*****************************************************************************************************************************
 * @file        Nvm_PBCfg.c                                                                                                  *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        25.08.2018 15:58:25                                                                                          *
 * @brief       Generated source file data of the Nvm module.                                                                *
 * @details     Implementation of general generated data.                                                                    *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
 * @libNote     OSAR Source File Library v.1.0.0.0                                                                           *
*****************************************************************************************************************************/
/**
 * @addtogroup Nvm
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 Start of include area                        << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#include "Det.h"
#include "Nvm_Types.h"
#include "Nvm_PBCfg.h"
#include "FlsEep.h"         //TODO: NOTE: This include would be dynamic generated for each configured sub module
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                  End of include area                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of private data types                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of private data types                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            Start of private definitions area                 << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of private definitions area                  << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of global variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/*------------------------------------------- Map variables into constand memory -------------------------------------------*/
#define Nvm_START_SEC_CONST
#include "Nvm_MemMap.h"
const Nvm_BlockCfgType nvmBlkCfgList[NVM_CNT_CONFIGURED_BLOCKS] = {
  {FALSE, FALSE, 0, 10, FLSEEP_BLK_DEMOBLK, NULL_PTR, NVM_CRC_CRC32, NVM_FLSEEP },
  {FALSE, FALSE, 0, 10, FLSEEP_BLK_DEMOBLK2, NULL_PTR, NVM_CRC_NONE, NVM_FLSEEP }
};

const Nvm_BaseModeCfgList nvmBaseModuleList[NVM_CNT_CONFIGURED_BASEMODULES] = {
  {&FlsEep_RequestWriteData, &FlsEep_RequestReadData, &FlsEep_RequestBlockStatus, &FlsEep_RequestModuleStatus}
};

#define Nvm_STOP_SEC_CONST
#include "Nvm_MemMap.h"
/*---------------------------------------- Map variables into uninitialized memory ----------------------------------------*/
#define Nvm_START_SEC_NOINIT_VAR
#include "Nvm_MemMap.h"
#define Nvm_STOP_SEC_NOINIT_VAR
#include "Nvm_MemMap.h"
/*----------------------------------------- Map variables into initialized memory -----------------------------------------*/
#define Nvm_START_SEC_INIT_VAR
#include "Nvm_MemMap.h"
#define Nvm_STOP_SEC_INIT_VAR
#include "Nvm_MemMap.h"
/*--------------------------------------- Map variables into zero initialized memory ---------------------------------------*/
#define Nvm_START_SEC_ZERO_INIT_VAR
#include "Nvm_MemMap.h"
#define Nvm_STOP_SEC_ZERO_INIT_VAR
#include "Nvm_MemMap.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of global variables                      << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of private variables                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/*------------------------------------------- Map variables into constand memory -------------------------------------------*/
#define Nvm_START_SEC_CONST
#include "Nvm_MemMap.h"
#define Nvm_STOP_SEC_CONST
#include "Nvm_MemMap.h"
/*---------------------------------------- Map variables into uninitialized memory ----------------------------------------*/
#define Nvm_START_SEC_NOINIT_VAR
#include "Nvm_MemMap.h"
#define Nvm_STOP_SEC_NOINIT_VAR
#include "Nvm_MemMap.h"
/*----------------------------------------- Map variables into initialized memory -----------------------------------------*/
#define Nvm_START_SEC_INIT_VAR
#include "Nvm_MemMap.h"
#define Nvm_STOP_SEC_INIT_VAR
#include "Nvm_MemMap.h"
/*--------------------------------------- Map variables into zero initialized memory ---------------------------------------*/
#define Nvm_START_SEC_ZERO_INIT_VAR
#include "Nvm_MemMap.h"
#define Nvm_STOP_SEC_ZERO_INIT_VAR
#include "Nvm_MemMap.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of private variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>           Start of private function prototypes               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            End of private function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>        Start of private function implementation              << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/*--------------------------------------------- Map function into code memory ---------------------------------------------*/
#define Nvm_START_SEC_CODE
#include "Nvm_MemMap.h"
#define Nvm_STOP_SEC_CODE
#include "Nvm_MemMap.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>         End of private function implementation               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>          Start of public function implementation             << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/*--------------------------------------------- Map function into code memory ---------------------------------------------*/
#define Nvm_START_SEC_CODE
#include "Nvm_MemMap.h"
#define Nvm_STOP_SEC_CODE
#include "Nvm_MemMap.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>          End of public function implementation               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/**
 * @}
 */




