#pragma once

#include "Std_Types.h"
#include "Rte_Cfg.h"

/**
 * @brief           Rte generated standard user data type >> Rte_ErrorType
 * @details         New data type based on standard data type: "uint16" with UUID: 82ae6b94f3a249aa853f79d1c3a83810
 * @uuid            bcf2b9942c4511e9b210d663bd873d93
 */
typedef uint16 Rte_ErrorType;

/**
 * @brief           Rte generated user enumeration data type >> Nvm_BlockStatusType
 * @uuid            c75650ab8eb94f37a27899ce64c49768
 */
typedef enum {
  NVM_BLK_OK = 0,
  NVM_BLK_PENDING = 1,
  NVM_BLK_SKIPPED = 2,
  NVM_BLK_INTEGRITY_FAILED = 3,
  NVM_BLK_INVALIDATE = 4,
  NVM_BLK_WAITING = 100,
  NVM_BLK_EMPTY = 255
}Nvm_BlockStatusType;