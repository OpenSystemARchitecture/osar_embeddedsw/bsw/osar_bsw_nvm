/******************************************************************************
  * @file    FlsEep.h 
  * @author  Reinemuth Sebastian
  * @date    20-02-2018
  * @brief   stubs-Definitions
  * last checkin :
  * $Author: sreinemuth $ @ $Date: 2017-02-18 12:31:21 +0100 (Sa, 18 Feb 2017) $ , Revision: $Rev: 14 $
  *****************************************************************************/
#ifndef __FLSEEP_H
#define __FLSEEP_H



/* Public includes -----------------------------------------------------------*/
#include "Std_Types.h"

/* Public Defines ------------------------------------------------------------*/
#define FLSEEP_BLK_DEMOBLK  1

/* Public functions prototypes -----------------------------------------------*/
/**
* @brief           Trigger a write action into the Flash
* @param[in]       uint16 FlsEep configured block id
* @param[in]       uin8* pointer to data which shall be written
* @retval          Std_ReturnType
*                  > E_OK        >> Request accepted
*                  > E_NOT_OK    >> Request failed.
* @details         Interface function to trigger a write action. This Function dose not perform the write action. The status
*                  of the active block has to be additional requested.
*/
Std_ReturnType FlsEep_RequestWriteData(uint32 flsBlockId, uint8* pWriteData);

/**
* @brief           Trigger a read action into the Flash
* @param[in]       uint32 FlsEep configured block id
* @param[in]       uint8* pointer to data where the read data shall be stored
* @retval          Std_ReturnType
*                  > E_OK        >> Request accepted
*                  > E_NOT_OK    >> Request failed.
* @details         Interface function to trigger a read action. This Function dose not perform the write action. The status
*                  of the active block has to be additional requested. The FlsEep module would write the read data into the given pointer location.
*/
Std_ReturnType FlsEep_RequestReadData(uint32 flsBlockId, uint8* pReadData);

/**
* @brief           Request the actual block status
* @param[in]       uint32 FlsEep configured block id
* @retval          Data_StatusType
*                  > DATA_E_OK            >> Block is valid
*                  > DATA_E_PENDING       >> Block processing is still in progress
*                  > DATA_E_INVALIDATE    >> Block data is not available
*                  > DATA_E_SKIPPED       >> Request failed / not accepted
* @details         Interface function to request the actual block status.
*/
Data_StatusType FlsEep_RequestBlockStatus(uint32 flsBlockId);

/**
* @brief           Request the actual module status
* @param[in]       None
* @retval          FlsEep_ReturnType
*                  > E_OK            >> Module is in idle mode
*                  > E_PENDING       >> Module is still processing data
*                  > E_NOT_OK        >> Module is in safe state
* @details         Interface function to request the actual module status.
*/
Std_ReturnType FlsEep_RequestModuleStatus(void);

#endif