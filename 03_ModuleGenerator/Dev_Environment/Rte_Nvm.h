/*****************************************************************************************************************************
 * @file        Rte_Nvm.h                                                                                                    *
 * @author      OSAR S.Reinemuth                                                                                             *
 * @date        03.01.2020 10:58:09                                                                                          *
 * @brief       Generated Rte Module Interface Header File: Rte_Nvm.h                                                        *
 * @version     1.0.0                                                                                                        *
 * @generator   OSAR Rte Module Generator v.0.1.8.24                                                                         *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
 * @libNote     OSAR Source File Library v.1.1.8.1                                                                           *
*****************************************************************************************************************************/

#ifndef __RTE_NVM_H
#define __RTE_NVM_H

/**
 * @addtogroup OSAR_BSW 
 * @{
 */
/**
 * @addtogroup Rte 
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 Start of include area                        << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#include "Rte_Types.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                  End of include area                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of public data types                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of public data types                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            Start of public definition area                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/*------------------------------------------------------------  ------------------------------------------------------------*/
/* ++++++++++++++++++++++++++++++++ Generation of available Server Port Error Information ++++++++++++++++++++++++++++++ */
#define Rte_ErrorType_PpGeneralNvm_Nvm_GetErrorStatus_NotOk 100
#define Rte_ErrorType_PpGeneralNvm_Nvm_WriteBlock_NotOk     100
#define Rte_ErrorType_PpGeneralNvm_Nvm_WriteBlock_Skipped   101
#define Rte_ErrorType_PpGeneralNvm_Nvm_ReadBlock_NotOk      100
#define Rte_ErrorType_PpGeneralNvm_Nvm_ReadBlock_Skipped    101
#define Rte_ErrorType_PpGeneralNvm_Nvm_EraseNvBlock_NotOk   100
#define Rte_ErrorType_PpGeneralNvm_Nvm_EraseNvBlock_Skipped 101
/*------------------------------------------------------------  ------------------------------------------------------------*/
/* ++++++++++++++++++++++++++++++++ Generation of available Server Port Error Information ++++++++++++++++++++++++++++++ */
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of public definition area                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of global variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of global variables                      << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>           Start of public function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/*------------------------------------------------------------  ------------------------------------------------------------*/
/* ++++++++++++++++++++++++++++++++++++++++ Generation of Init Runnable Prototypes ++++++++++++++++++++++++++++++++++++++++ */
VAR(void) Nvm_Init( VAR(void) );

/*------------------------------------------------------------  ------------------------------------------------------------*/
/* +++++++++++++++++++++++++++++++++++++++ Generation of Cyclic Runnable Prototypes +++++++++++++++++++++++++++++++++++++++ */
VAR(void) Nvm_Mainfunction( VAR(void) );

/*------------------------------------------------------------  ------------------------------------------------------------*/
/* +++++++++++++++++++++++++++++++++++++ Generation of Server Port Runnable Prototypes ++++++++++++++++++++++++++++++++++++ */

/*------------------------------------------------------------  ------------------------------------------------------------*/
/* ++++++++++++++++++++++++++++++++ Generation of Client Port Runnable Prototype Information ++++++++++++++++++++++++++++++ */
/*------------------------------------------------------------  ------------------------------------------------------------*/
/* +++++++++++++++++++++++++++ Generation of Sender Receiver Port Runnable Prototype Information ++++++++++++++++++++++++++ */
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of public function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/**
 * @}
 */
/**
 * @}
 */

#endif /* __RTE_NVM_H*/
