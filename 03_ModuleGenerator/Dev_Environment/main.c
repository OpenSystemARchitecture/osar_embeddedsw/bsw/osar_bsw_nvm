/******************************************************************************
* @file    main.c
* @author  Reinemuth Sebastian
* @date    17-02-2016
* @brief   main-Functions
* last checkin :
* $Author: sreinemuth $ @ $Date: 2017-02-18 12:31:21 +0100 (Sa, 18 Feb 2017) $ , Revision: $Rev: 14 $
*****************************************************************************/

/* Private includes ----------------------------------------------------------*/
#include "Std_Types.h"
#include "Nvm.h"
#include "Crc.h"
/* Global variables ----------------------------------------------------------*/
volatile Nvm_BlockStatusType testNvmBlkStatus;
volatile Std_ReturnType testRetVal;
/* Private function prototypes -----------------------------------------------*/

/* Functions -----------------------------------------------------------------*/
int main()
{
  
  uint8 demoDataWrite[10] = { 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA };
  Nvm_InitMemory();
  Crc_InitMemory();
  Nvm_Init();
  Crc_Init();

  testRetVal = Nvm_GetErrorStatus(NVM_BLK_ID_DEMOBLK, &testNvmBlkStatus);

  /* Set List Entry */
  Nvm_WriteBlock(NVM_BLK_ID_DEMOBLK, &demoDataWrite[0]);

  testRetVal = Nvm_GetErrorStatus(NVM_BLK_ID_DEMOBLK, &testNvmBlkStatus);

  Nvm_Mainfunction();



  while (1);
}