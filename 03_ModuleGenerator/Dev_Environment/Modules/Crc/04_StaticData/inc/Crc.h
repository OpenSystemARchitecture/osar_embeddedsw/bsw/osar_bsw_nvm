/*****************************************************************************************************************************
 * @file        Crc.h                                                                                                        *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        07.04.2018 11:39:40                                                                                          *
 * @brief       Implementation of definitions / interface function prototypes / datatypes and generic module interface       *
 *              informations of the "Crc" module.                                                                            *
 *                                                                                                                           *
 * @details     The Crc module implements different Algorithms to perform an cyclic redundancy check.                        *
 *                                                                                                                           *
 * version      1.0.0                                                                                                        *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
 * @libNote     OSAR Source File Library v.0.1.2.15                                                                          *
*****************************************************************************************************************************/
#ifndef __CRC_H
#define __CRC_H
/**
 * @defgroup Crc TODO >> Add module description for doxygen
 */

/**
 * @addtogroup Crc
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 Start of include area                        << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#include "Crc_Types.h"
#include "Crc_PBCfg.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                  End of include area                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of public data types                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of public data types                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            Start of public definition area                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of public definition area                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of global variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of global variables                      << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>           Start of public function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/**
 * @brief           Module global memory initialization function.
 * @param[in]       None
 * @retval          None
 * @details         This function shall be called during system startup. And before calling the init and mainfuntion.
 */
void Crc_InitMemory( void );

/**
 * @brief           Module global initialization function.
 * @param[in]       None
 * @retval          None
 * @details         This function shall be called before using the module and after the memory initialization.
 */
void Crc_Init( void );

/**
 * @brief           Module global mainfunction.
 * @param[in]       None
 * @retval          None
 * @details         This function shall be called form the actual runtime environment within an fixed cycle.
 */
void Crc_Mainfunction( void );


/* ######################################################################################################################## */
/* ########################################### API Interfaces of CRC32 Algorithms  ######################################## */
/* ######################################################################################################################## */
#if (CRC_USE_SOFT_CALC_CRC32_0X04C11DB7 == STD_ON)
/**
* @brief           API to compute a CRC 32 Checksum using soft calculation with Polynom 0x04C11DB7.
*                  This function implements the standard Ethernat Polynom within IEEE802.3
* @param[in]       uint8* pointer to data where the crc shall be calculated
* @param[in]       uint16 length of data where the crc shall be calculated
* @param[out]      uint32* pointer to write the crc32 result data.
* @retval          Crc_ReturnType
*                  > CRC_E_OK
*                  > CRC_E_NULL_POINTER
*                  > CRC_E_INVALID_DATA_LENGTH
* @details         This function use the cpu to calculate the crc ond not an look up table.
*/
extern Crc_ReturnType Crc32_0x04C11DB7_Compute(uint8* pData, uint16 lengthOfData, uint32 *crcResult);
#endif

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of public function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/**
 * @}
 */

#endif /* __CRC_H*/
