/******************************************************************************
  * @file    stubs.h 
  * @author  Reinemuth Sebastian
  * @date    20-02-2018
  * @brief   stubs-Definitions
  * last checkin :
  * $Author: sreinemuth $ @ $Date: 2017-02-18 12:31:21 +0100 (Sa, 18 Feb 2017) $ , Revision: $Rev: 14 $
  *****************************************************************************/
#ifndef __STUBS_H
#define __STUBS_H



/* Public includes -----------------------------------------------------------*/
#include "Std_Types.h"

/* Public Defines ------------------------------------------------------------*/

/* Public functions prototypes -----------------------------------------------*/


#endif