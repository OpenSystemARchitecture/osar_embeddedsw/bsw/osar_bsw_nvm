/*****************************************************************************************************************************
* @file        Stubs.c                                                                                                      *
* @author      OSAR Team                                                                                                    *
* @date        20.02.2018 10:49:55                                                                                          *
* @brief       Implementation of functionalities from the "Stubs" module.                                                   *
*****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 Start of include area                        << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#include "stubs.h"
#include <vcruntime.h>
#include "cmocka.h"
#include "Nvm_Types.h"
#include "Nvm_PBCfg.h"
#include "FlsEep.h"
#include "Nvm_RequestList.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                  End of include area                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of private data types                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of private data types                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            Start of private definitions area                 << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of private definitions area                  << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of global variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
Nvm_BlockStatusType nvmBlockStatusList[NVM_CNT_CONFIGURED_BLOCKS];
Nvm_RequestListEntryType nvmRequestList[NVM_REQUEST_LIST_SIZE];
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of global variables                      << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of private variables                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of private variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>         Start of helper function implementation              << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
void Nvm_RequestList_Init(void)
{
  uint8 idx;

  /* Initialize Request List */
  for (idx = 0; idx < NVM_REQUEST_LIST_SIZE; idx++)
  {
    expect_function_call(Nvm_RequestList_HlpInvalidateListElement);
    will_return(Nvm_RequestList_HlpInvalidateListElement, idx);
    Nvm_RequestList_HlpInvalidateListElement(idx);
  }
}
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>          End of helper function implementation               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>          Start of public function implementation             << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
Std_ReturnType Det_ReportError(uint8 moduleId, uint8 errorId)
{
  function_called();
  return E_NOT_OK;
}


/**
* @brief           Trigger a write action into the Flash
* @param[in]       uint16 FlsEep configured block id
* @param[in]       uin8* pointer to data which shall be written
* @retval          Std_ReturnType
*                  > E_OK        >> Request accepted
*                  > E_NOT_OK    >> Request failed.
* @details         Interface function to trigger a write action. This Function dose not perform the write action. The status
*                  of the active block has to be additional requested.
*/
Std_ReturnType FlsEep_RequestWriteData(uint16 flsBlockId, uint8* pWriteData)
{
  function_called();
  assert_int_equal(flsBlockId, (uint16)mock());
  assert_int_equal(pWriteData, (uint8*)mock());
  return (Std_ReturnType)mock();
}

/**
* @brief           Trigger a read action into the Flash
* @param[in]       uint16 FlsEep configured block id
* @param[in]       uint8* pointer to data where the read data shall be stored
* @retval          Std_ReturnType
*                  > E_OK        >> Request accepted
*                  > E_NOT_OK    >> Request failed.
* @details         Interface function to trigger a read action. This Function dose not perform the write action. The status
*                  of the active block has to be additional requested. The FlsEep module would write the read data into the given pointer location.
*/
Std_ReturnType FlsEep_RequestReadData(uint16 flsBlockId, uint8* pReadData)
{
  function_called();
  assert_int_equal(flsBlockId, (uint16)mock());
  assert_int_equal(pReadData, (uint8*)mock());
  return (Std_ReturnType)mock();
}

/**
* @brief           Request the actual block status
* @param[in]       uint16 FlsEep configured block id
* @retval          Data_StatusType
*                  > DATA_E_OK            >> Block is valid
*                  > DATA_E_PENDING       >> Block processing is still in progress
*                  > DATA_E_INVALIDATE    >> Block data is not available
*                  > DATA_E_SKIPPED       >> Request failed / not accepted
* @details         Interface function to request the actual block status.
*/
Data_StatusType FlsEep_RequestBlockStatus(uint16 flsBlockId)
{
  function_called();
  assert_int_equal(flsBlockId, (uint16)mock());

  return (Data_StatusType)mock();
}

/**
* @brief           Request the actual module status
* @param[in]       None
* @retval          FlsEep_ReturnType
*                  > E_OK            >> Module is in idle mode
*                  > E_PENDING       >> Module is still processing data
*                  > E_NOT_OK        >> Module is in safe state
* @details         Interface function to request the actual module status.
*/
Std_ReturnType FlsEep_RequestModuleStatus(void)
{
  function_called();
  //TODO: Add Test Implementation
  return E_OK;
}

/**
* @brief           Helper function to invalidate a specific list element
* @param[in]       uint8 List index to be invalidated
* @retval          None
* @details         This function shall be called before using the module and after the memory initialization.
*/
void Nvm_RequestList_HlpInvalidateListElement(uint8 listIdx)
{
  function_called();
  assert_int_equal(listIdx, (uint8)mock());

  uint16 idx;
  nvmRequestList[listIdx].nvmBlockId = 0xFFFF;
  nvmRequestList[listIdx].requestDataPointer = NULL_PTR;
  nvmRequestList[listIdx].nvmRequestStatus = NVM_BLK_EMPTY;
  nvmRequestList[listIdx].nvmRequestType = NVM_REQ_ERASE;
  nvmRequestList[listIdx].nvmSafe = 0x00;

  for (idx = 0; idx < NVM_MAX_REQUEST_DATA_SIZE; idx++)
  {
    nvmRequestList[listIdx].nvmData[idx] = 0x00;
  }
}

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>          End of public function implementation               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/**
* @}
*/

