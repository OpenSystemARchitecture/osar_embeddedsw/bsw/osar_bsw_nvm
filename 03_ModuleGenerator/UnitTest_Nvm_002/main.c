/*****************************************************************************************************************************
* @file        main.cpp                                                                                                     *
* @author      OSAR Team                                                                                                    *
* @date        20.02.2018 10:49:55                                                                                          *
* @brief       Implementation of unit test functionalities from the "Dummy" module.                                         *
*****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 Start of include area                        << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <vcruntime.h>
#include <cmocka.h>
#include <stdio.h>
#include "stubs.h"
#include "FlsEep.h"
#include "Std_Types.h"

#include "..\..\02_Software\04_StaticData\src\Nvm_Int.c"

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                  End of include area                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of private data types                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of private data types                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            Start of private definitions area                 << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of private definitions area                  << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/



/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>           Start of private function prototypes               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/* Unit tests of Group Standard */
static void UTest_Nvm_Int_Init(void **state);
static void UTest_Nvm_Int_SetBlkStatus(void **state);
static void UTest_Nvm_Int_GetBlkStatus(void **state);


/* Unit tests of Group Processing */
static void UTest_Nvm_Int_ProcessingActiveRequests_001(void **state);
static void UTest_Nvm_Int_ProcessingActiveRequests_002(void **state);
static void UTest_Nvm_Int_ProcessingActiveRequests_003(void **state);
static void UTest_Nvm_Int_ProcessingActiveRequests_004(void **state);
static void UTest_Nvm_Int_ProcessingActiveRequests_005(void **state);
static void UTest_Nvm_Int_ProcessingActiveRequests_006(void **state);
static void UTest_Nvm_Int_ProcessingActiveRequests_007(void **state);
static void UTest_Nvm_Int_ProcessingActiveRequests_008(void **state);
static void UTest_Nvm_Int_ProcessingActiveRequests_009(void **state);
static void UTest_Nvm_Int_ProcessingActiveRequests_010(void **state);
static void UTest_Nvm_Int_ProcessingActiveRequests_011(void **state);
static void UTest_Nvm_Int_ProcessingActiveRequests_012(void **state);

static void UTest_Nvm_Int_ProcessingNewRequests_101(void **state);
static void UTest_Nvm_Int_ProcessingNewRequests_102(void **state);
static void UTest_Nvm_Int_ProcessingNewRequests_103(void **state);
static void UTest_Nvm_Int_ProcessingNewRequests_104(void **state);
static void UTest_Nvm_Int_ProcessingNewRequests_105(void **state);
static void UTest_Nvm_Int_ProcessingNewRequests_106(void **state);
/*...*/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            End of private function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of global variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
//extern Nvm_RequestListEntryType nvmRequestList[];

/* Creating Unit Test Group ... */
const struct CMUnitTest tests[] = {
  cmocka_unit_test(UTest_Nvm_Int_Init),
  cmocka_unit_test(UTest_Nvm_Int_SetBlkStatus),
  cmocka_unit_test(UTest_Nvm_Int_GetBlkStatus),
};

/* Creating Unit Test Group Processing */
const struct CMUnitTest testsProcessing[] = {
  cmocka_unit_test(UTest_Nvm_Int_ProcessingActiveRequests_001),
  cmocka_unit_test(UTest_Nvm_Int_ProcessingActiveRequests_002),
  cmocka_unit_test(UTest_Nvm_Int_ProcessingActiveRequests_003),
  cmocka_unit_test(UTest_Nvm_Int_ProcessingActiveRequests_004),
  cmocka_unit_test(UTest_Nvm_Int_ProcessingActiveRequests_005),
  cmocka_unit_test(UTest_Nvm_Int_ProcessingActiveRequests_006),
  cmocka_unit_test(UTest_Nvm_Int_ProcessingActiveRequests_007),
  cmocka_unit_test(UTest_Nvm_Int_ProcessingActiveRequests_008),
  cmocka_unit_test(UTest_Nvm_Int_ProcessingActiveRequests_009),
  cmocka_unit_test(UTest_Nvm_Int_ProcessingActiveRequests_010),
  cmocka_unit_test(UTest_Nvm_Int_ProcessingActiveRequests_011),
  cmocka_unit_test(UTest_Nvm_Int_ProcessingActiveRequests_012),

  cmocka_unit_test(UTest_Nvm_Int_ProcessingNewRequests_101),
  cmocka_unit_test(UTest_Nvm_Int_ProcessingNewRequests_102),
  cmocka_unit_test(UTest_Nvm_Int_ProcessingNewRequests_103),
  cmocka_unit_test(UTest_Nvm_Int_ProcessingNewRequests_104),
  cmocka_unit_test(UTest_Nvm_Int_ProcessingNewRequests_105),
  cmocka_unit_test(UTest_Nvm_Int_ProcessingNewRequests_106),
};
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of global variables                      << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of private variables                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of private variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>        Start of private function implementation              << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/*--------------------------------------------------------------------------------------------------------------------------*/
/*------------------------------------------------ Unit Tests of group ... -------------------------------------------------*/
/*--------------------------------------------------------------------------------------------------------------------------*/
static void UTest_Nvm_Int_Init(void **state)
{
  uint8 idx = 0;
  uint16 idx2 = 0;

  /*----------------------------------------------------------------------------*/
  /*------------------------------- Test Setup ---------------------------------*/
  /*----------------------------------------------------------------------------*/
  printf("[ DESCRIP  ] >> Check the init functionality of the Nvm sub module \"Int\"<< \r\n");

  /* Invalidate data */
  for (idx = 0; idx < NVM_CNT_CONFIGURED_BLOCKS; idx++)
  {
    nvmBlockStatusList[idx] = NVM_BLK_WAITING;
  }
  nvmProcessNewReq_LastIdx = 0xFF;

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Execution ------------------------------*/
  /*----------------------------------------------------------------------------*/
  Nvm_Int_Init();

  for (idx = 0; idx < NVM_CNT_CONFIGURED_BLOCKS; idx++)
  {
    assert_int_equal(nvmBlockStatusList[idx], NVM_BLK_OK);
  }
  assert_int_equal(nvmProcessNewReq_LastIdx, 0x00);
  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Cleanup --------------------------------*/
  /*----------------------------------------------------------------------------*/
  (void)state; /* unused */
}

static void UTest_Nvm_Int_SetBlkStatus(void **state)
{
  uint8 idx = 0;
  uint16 idx2 = 0;
  Nvm_ReturnType fncRetVal;
  uint16 testBlkId;
  Nvm_BlockStatusType testBlkStatus;

  /*----------------------------------------------------------------------------*/
  /*------------------------------- Test Setup ---------------------------------*/
  /*----------------------------------------------------------------------------*/
  printf("[ DESCRIP  ] >> Check the set user block status functionality of the Nvm sub module \"Int\"<< \r\n");
  printf("[          ] >> Check multiple good and faile data set at once. \r\n");


  expect_function_call(Det_ReportError);
  expect_function_call(Det_ReportError);
  expect_function_call(Det_ReportError);

  Nvm_Int_Init();

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Execution ------------------------------*/
  /*----------------------------------------------------------------------------*/
  testBlkId = NVM_BLK_ID_DEMO_BLK;
  testBlkStatus = NVM_BLK_PENDING;
  fncRetVal = Nvm_Int_SetNvmBlockStatus(testBlkId, testBlkStatus);
  assert_int_equal(fncRetVal, NVM_E_OK);
  assert_int_equal(nvmBlockStatusList[testBlkId], testBlkStatus);

  testBlkId = NVM_BLK_ID_DEMO_BLK2;
  testBlkStatus = NVM_BLK_SKIPPED;
  fncRetVal = Nvm_Int_SetNvmBlockStatus(testBlkId, testBlkStatus);
  assert_int_equal(fncRetVal, NVM_E_OK);
  assert_int_equal(nvmBlockStatusList[testBlkId], testBlkStatus);

  testBlkId = NVM_BLK_ID_DEMO_BLK3;
  testBlkStatus = NVM_BLK_INTEGRITY_FAILED;
  fncRetVal = Nvm_Int_SetNvmBlockStatus(testBlkId, testBlkStatus);
  assert_int_equal(fncRetVal, NVM_E_OK);
  assert_int_equal(nvmBlockStatusList[testBlkId], testBlkStatus);

  testBlkId = NVM_BLK_ID_DEMO_BLK3;
  testBlkStatus = NVM_BLK_INVALIDATE;
  fncRetVal = Nvm_Int_SetNvmBlockStatus(testBlkId, testBlkStatus);
  assert_int_equal(fncRetVal, NVM_E_OK);
  assert_int_equal(nvmBlockStatusList[testBlkId], testBlkStatus);

  testBlkId = NVM_BLK_ID_DEMO_BLK3;
  testBlkStatus = NVM_BLK_OK;
  fncRetVal = Nvm_Int_SetNvmBlockStatus(testBlkId, testBlkStatus);
  assert_int_equal(fncRetVal, NVM_E_OK);
  assert_int_equal(nvmBlockStatusList[testBlkId], testBlkStatus);

  testBlkId = NVM_BLK_ID_DEMO_BLK3 + 1;
  testBlkStatus = NVM_BLK_INVALIDATE;
  fncRetVal = Nvm_Int_SetNvmBlockStatus(testBlkId, testBlkStatus);
  assert_int_equal(fncRetVal, NVM_E_NOT_OK);
  assert_int_equal(nvmBlockStatusList[NVM_BLK_ID_DEMO_BLK3], NVM_BLK_OK);

  testBlkId = NVM_BLK_ID_DEMO_BLK3;
  testBlkStatus = NVM_BLK_EMPTY;
  fncRetVal = Nvm_Int_SetNvmBlockStatus(testBlkId, testBlkStatus);
  assert_int_equal(fncRetVal, NVM_E_NOT_OK);
  assert_int_equal(nvmBlockStatusList[testBlkId], NVM_BLK_OK);

  testBlkId = NVM_BLK_ID_DEMO_BLK3;
  testBlkStatus = NVM_BLK_WAITING;
  fncRetVal = Nvm_Int_SetNvmBlockStatus(testBlkId, testBlkStatus);
  assert_int_equal(fncRetVal, NVM_E_NOT_OK);
  assert_int_equal(nvmBlockStatusList[testBlkId], NVM_BLK_OK);
  
  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Cleanup --------------------------------*/
  /*----------------------------------------------------------------------------*/
  (void)state; /* unused */
}

static void UTest_Nvm_Int_GetBlkStatus(void **state)
{
  uint8 idx = 0;
  uint16 idx2 = 0;
  Nvm_ReturnType fncRetVal;
  uint16 testBlkId;
  Nvm_BlockStatusType testBlkStatus, testBlkStatus2;

  /*----------------------------------------------------------------------------*/
  /*------------------------------- Test Setup ---------------------------------*/
  /*----------------------------------------------------------------------------*/
  printf("[ DESCRIP  ] >> Check the get user block status functionality of the Nvm sub module \"Int\"<< \r\n");
  printf("[          ] >> Check multiple good and faile data set at once. \r\n");


  expect_function_call(Det_ReportError);
  expect_function_call(Det_ReportError);

  Nvm_Int_Init();

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Execution ------------------------------*/
  /*----------------------------------------------------------------------------*/
  testBlkId = NVM_BLK_ID_DEMO_BLK;
  testBlkStatus = NVM_BLK_PENDING;
  Nvm_Int_SetNvmBlockStatus(testBlkId, testBlkStatus);
  fncRetVal = Nvm_Int_GetNvmBlockStatus(testBlkId, &testBlkStatus2);
  assert_int_equal(fncRetVal, NVM_E_OK);
  assert_int_equal(nvmBlockStatusList[testBlkId], testBlkStatus2);

  testBlkId = NVM_BLK_ID_DEMO_BLK2;
  testBlkStatus = NVM_BLK_SKIPPED;
  Nvm_Int_SetNvmBlockStatus(testBlkId, testBlkStatus);
  fncRetVal = Nvm_Int_GetNvmBlockStatus(testBlkId, &testBlkStatus2);
  assert_int_equal(fncRetVal, NVM_E_OK);
  assert_int_equal(nvmBlockStatusList[testBlkId], testBlkStatus2);

  testBlkId = NVM_BLK_ID_DEMO_BLK3;
  testBlkStatus = NVM_BLK_INTEGRITY_FAILED;
  Nvm_Int_SetNvmBlockStatus(testBlkId, testBlkStatus);
  fncRetVal = Nvm_Int_GetNvmBlockStatus(testBlkId, &testBlkStatus2);
  assert_int_equal(fncRetVal, NVM_E_OK);
  assert_int_equal(nvmBlockStatusList[testBlkId], testBlkStatus2);

  testBlkId = NVM_BLK_ID_DEMO_BLK3 +1;
  testBlkStatus = NVM_BLK_INVALIDATE;
  Nvm_Int_SetNvmBlockStatus(testBlkId, testBlkStatus);
  fncRetVal = Nvm_Int_GetNvmBlockStatus(testBlkId, &testBlkStatus2);
  assert_int_equal(fncRetVal, NVM_E_NOT_OK);
  assert_int_equal(nvmBlockStatusList[NVM_BLK_ID_DEMO_BLK3], NVM_BLK_INTEGRITY_FAILED);

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Cleanup --------------------------------*/
  /*----------------------------------------------------------------------------*/
  (void)state; /* unused */
}

/*--------------------------------------------------------------------------------------------------------------------------*/
/*--------------------------------------------- Unit Tests of group Processing ---------------------------------------------*/
/*--------------------------------------------------------------------------------------------------------------------------*/
static void UTest_Nvm_Int_ProcessingActiveRequests_001(void **state)
{
  uint8 listIdx = 0;
  uint16 idx = 0;
  Nvm_ReturnType fncRetVal;
  uint16 testBlkId;
  Nvm_BlockStatusType testBlkStatus, testBlkStatus2;

  /*----------------------------------------------------------------------------*/
  /*------------------------------- Test Setup ---------------------------------*/
  /*----------------------------------------------------------------------------*/
  printf("[ DESCRIP  ] >> Check the processing active requests functionality of the Nvm sub module \"Int\"<< \r\n");
  printf("[          ] >> Check functionality if no element is pending >> List is empty \r\n");

  Nvm_Int_Init();
  Nvm_RequestList_Init();

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Execution ------------------------------*/
  /*----------------------------------------------------------------------------*/
  Nvm_Int_ProcessActiveRequests();

  /* Check request list */
  for (listIdx = 0; listIdx < NVM_REQUEST_LIST_SIZE; listIdx++)
  {
    assert_int_equal(nvmRequestList[listIdx].nvmBlockId , 0xFFFF);
    assert_int_equal(nvmRequestList[listIdx].requestDataPointer , NULL_PTR);
    assert_int_equal(nvmRequestList[listIdx].nvmRequestStatus , NVM_BLK_EMPTY);
    assert_int_equal(nvmRequestList[listIdx].nvmRequestType , NVM_REQ_ERASE);
    assert_int_equal(nvmRequestList[listIdx].nvmSafe , 0x00);

    for (idx = 0; idx < NVM_MAX_REQUEST_DATA_SIZE; idx++)
    {
      assert_int_equal(nvmRequestList[listIdx].nvmData[idx] , 0x00);
    }
  }

  /* Check status list */
  for (idx = 0; idx < NVM_CNT_CONFIGURED_BLOCKS; idx++)
  {
    assert_int_equal(nvmBlockStatusList[idx], NVM_BLK_OK);
  }
  assert_int_equal(nvmProcessNewReq_LastIdx, 0x00);

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Cleanup --------------------------------*/
  /*----------------------------------------------------------------------------*/
  (void)state; /* unused */
}

static void UTest_Nvm_Int_ProcessingActiveRequests_002(void **state)
{
  uint8 listIdx = 0;
  uint16 idx = 0;
  Nvm_ReturnType fncRetVal;
  uint16 testBlkId;
  Nvm_BlockStatusType testBlkStatus, testBlkStatus2;

  /*----------------------------------------------------------------------------*/
  /*------------------------------- Test Setup ---------------------------------*/
  /*----------------------------------------------------------------------------*/
  printf("[ DESCRIP  ] >> Check the processing active requests functionality of the Nvm sub module \"Int\"<< \r\n");
  printf("[          ] >> Check functionality if no element is pending >> List elements are waiting to be processed\r\n");

  Nvm_Int_Init();
  Nvm_RequestList_Init();

  for (listIdx = 0; listIdx < NVM_REQUEST_LIST_SIZE; listIdx++)
  {
    nvmRequestList[listIdx].nvmRequestStatus = NVM_BLK_WAITING;
  }

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Execution ------------------------------*/
  /*----------------------------------------------------------------------------*/
  Nvm_Int_ProcessActiveRequests();

  /* Check request list */
  for (listIdx = 0; listIdx < NVM_REQUEST_LIST_SIZE; listIdx++)
  {
    assert_int_equal(nvmRequestList[listIdx].nvmBlockId, 0xFFFF);
    assert_int_equal(nvmRequestList[listIdx].requestDataPointer, NULL_PTR);
    assert_int_equal(nvmRequestList[listIdx].nvmRequestStatus, NVM_BLK_WAITING);
    assert_int_equal(nvmRequestList[listIdx].nvmRequestType, NVM_REQ_ERASE);
    assert_int_equal(nvmRequestList[listIdx].nvmSafe, 0x00);

    for (idx = 0; idx < NVM_MAX_REQUEST_DATA_SIZE; idx++)
    {
      assert_int_equal(nvmRequestList[listIdx].nvmData[idx], 0x00);
    }
  }

  /* Check status list */
  for (idx = 0; idx < NVM_CNT_CONFIGURED_BLOCKS; idx++)
  {
    assert_int_equal(nvmBlockStatusList[idx], NVM_BLK_OK);
  }
  assert_int_equal(nvmProcessNewReq_LastIdx, 0x00);

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Cleanup --------------------------------*/
  /*----------------------------------------------------------------------------*/
  (void)state; /* unused */
}

static void UTest_Nvm_Int_ProcessingActiveRequests_003(void **state)
{
  uint8 listIdx = 0;
  uint16 idx = 0;
  Nvm_ReturnType fncRetVal;
  uint16 testBlkId;
  Nvm_BlockStatusType testBlkStatus, testBlkStatus2;
  uint8 testDataWriteBlk[10] = { 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA };
  uint8 testDataReadBlk[10];

  /*----------------------------------------------------------------------------*/
  /*------------------------------- Test Setup ---------------------------------*/
  /*----------------------------------------------------------------------------*/
  printf("[ DESCRIP  ] >> Check the processing active requests functionality of the Nvm sub module \"Int\"<< \r\n");
  printf("[          ] >> Check functionality if element is pending >> Sub module element is DATA_E_PENDING \r\n");

  Nvm_Int_Init();
  Nvm_RequestList_Init();

  expect_function_call(FlsEep_RequestBlockStatus);
  will_return(FlsEep_RequestBlockStatus, FLSEEP_BLK_DEMOBLK2); /* Check Argument */
  will_return(FlsEep_RequestBlockStatus, DATA_E_PENDING); /* Set return value */

  nvmRequestList[1].nvmBlockId = NVM_BLK_ID_DEMO_BLK2;
  nvmRequestList[1].nvmRequestStatus = NVM_BLK_PENDING;
  nvmRequestList[1].nvmRequestType = NVM_REQ_ERASE;
  nvmRequestList[1].requestDataPointer = NULL_PTR;

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Execution ------------------------------*/
  /*----------------------------------------------------------------------------*/
  Nvm_Int_ProcessActiveRequests();

  /* Check modified request list element */
  assert_int_equal(nvmRequestList[1].nvmBlockId, NVM_BLK_ID_DEMO_BLK2);
  assert_int_equal(nvmRequestList[1].requestDataPointer, NULL_PTR);
  assert_int_equal(nvmRequestList[1].nvmRequestStatus, NVM_BLK_PENDING);
  assert_int_equal(nvmRequestList[1].nvmRequestType, NVM_REQ_ERASE);
  assert_int_equal(nvmRequestList[1].nvmSafe, 0x00);

  for (idx = 0; idx < NVM_MAX_REQUEST_DATA_SIZE; idx++)
  {
    assert_int_equal(nvmRequestList[1].nvmData[idx], 0x00);
  }


  /* Check list element before and after the request list element */
  /* Before */
  assert_int_equal(nvmRequestList[0].nvmBlockId, 0xFFFF);
  assert_int_equal(nvmRequestList[0].requestDataPointer, NULL_PTR);
  assert_int_equal(nvmRequestList[0].nvmRequestStatus, NVM_BLK_EMPTY);
  assert_int_equal(nvmRequestList[0].nvmRequestType, NVM_REQ_ERASE);
  assert_int_equal(nvmRequestList[0].nvmSafe, 0x00);

  for (idx = 0; idx < NVM_MAX_REQUEST_DATA_SIZE; idx++)
  {
    assert_int_equal(nvmRequestList[0].nvmData[idx], 0x00);
  }

  /* After */
  assert_int_equal(nvmRequestList[2].nvmBlockId, 0xFFFF);
  assert_int_equal(nvmRequestList[2].requestDataPointer, NULL_PTR);
  assert_int_equal(nvmRequestList[2].nvmRequestStatus, NVM_BLK_EMPTY);
  assert_int_equal(nvmRequestList[2].nvmRequestType, NVM_REQ_ERASE);
  assert_int_equal(nvmRequestList[2].nvmSafe, 0x00);

  for (idx = 0; idx < NVM_MAX_REQUEST_DATA_SIZE; idx++)
  {
    assert_int_equal(nvmRequestList[1].nvmData[idx], 0x00);
  }

  /* Check status list elements */
  for (idx = 0; idx < NVM_CNT_CONFIGURED_BLOCKS; idx++)
  {
    assert_int_equal(nvmBlockStatusList[idx], NVM_BLK_OK);
  }
  assert_int_equal(nvmProcessNewReq_LastIdx, 0x00);

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Cleanup --------------------------------*/
  /*----------------------------------------------------------------------------*/
  (void)state; /* unused */
}

static void UTest_Nvm_Int_ProcessingActiveRequests_004(void **state)
{
  uint8 listIdx = 0;
  uint16 idx = 0;
  Nvm_ReturnType fncRetVal;
  uint16 testBlkId;
  Nvm_BlockStatusType testBlkStatus, testBlkStatus2;
  uint8 testDataWriteBlk[10] = { 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA };
  uint8 testDataReadBlk[10];

  /*----------------------------------------------------------------------------*/
  /*------------------------------- Test Setup ---------------------------------*/
  /*----------------------------------------------------------------------------*/
  printf("[ DESCRIP  ] >> Check the processing active requests functionality of the Nvm sub module \"Int\"<< \r\n");
  printf("[          ] >> Check functionality if element is pending >> Sub module element is DATA_E_SKIPPED \r\n");

  Nvm_Int_Init();
  Nvm_RequestList_Init();

  expect_function_call(FlsEep_RequestBlockStatus);
  will_return(FlsEep_RequestBlockStatus, FLSEEP_BLK_DEMOBLK2); /* Check Argument */
  will_return(FlsEep_RequestBlockStatus, DATA_E_SKIPPED); /* Set return value */

  nvmRequestList[1].nvmBlockId = NVM_BLK_ID_DEMO_BLK2;
  nvmRequestList[1].nvmRequestStatus = NVM_BLK_PENDING;
  nvmRequestList[1].nvmRequestType = NVM_REQ_ERASE;
  nvmRequestList[1].requestDataPointer = NULL_PTR;

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Execution ------------------------------*/
  /*----------------------------------------------------------------------------*/
  Nvm_Int_ProcessActiveRequests();

  /* Check modified request list element */
  assert_int_equal(nvmRequestList[1].nvmBlockId, NVM_BLK_ID_DEMO_BLK2);
  assert_int_equal(nvmRequestList[1].requestDataPointer, NULL_PTR);
  assert_int_equal(nvmRequestList[1].nvmRequestStatus, NVM_BLK_WAITING);
  assert_int_equal(nvmRequestList[1].nvmRequestType, NVM_REQ_ERASE);
  assert_int_equal(nvmRequestList[1].nvmSafe, 0x00);

  for (idx = 0; idx < NVM_MAX_REQUEST_DATA_SIZE; idx++)
  {
    assert_int_equal(nvmRequestList[1].nvmData[idx], 0x00);
  }


  /* Check list element before and after the request list element */
  /* Before */
  assert_int_equal(nvmRequestList[0].nvmBlockId, 0xFFFF);
  assert_int_equal(nvmRequestList[0].requestDataPointer, NULL_PTR);
  assert_int_equal(nvmRequestList[0].nvmRequestStatus, NVM_BLK_EMPTY);
  assert_int_equal(nvmRequestList[0].nvmRequestType, NVM_REQ_ERASE);
  assert_int_equal(nvmRequestList[0].nvmSafe, 0x00);

  for (idx = 0; idx < NVM_MAX_REQUEST_DATA_SIZE; idx++)
  {
    assert_int_equal(nvmRequestList[0].nvmData[idx], 0x00);
  }

  /* After */
  assert_int_equal(nvmRequestList[2].nvmBlockId, 0xFFFF);
  assert_int_equal(nvmRequestList[2].requestDataPointer, NULL_PTR);
  assert_int_equal(nvmRequestList[2].nvmRequestStatus, NVM_BLK_EMPTY);
  assert_int_equal(nvmRequestList[2].nvmRequestType, NVM_REQ_ERASE);
  assert_int_equal(nvmRequestList[2].nvmSafe, 0x00);

  for (idx = 0; idx < NVM_MAX_REQUEST_DATA_SIZE; idx++)
  {
    assert_int_equal(nvmRequestList[1].nvmData[idx], 0x00);
  }

  /* Check status list elements */
  for (idx = 0; idx < NVM_CNT_CONFIGURED_BLOCKS; idx++)
  {
    assert_int_equal(nvmBlockStatusList[idx], NVM_BLK_OK);
  }
  assert_int_equal(nvmProcessNewReq_LastIdx, 0x00);

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Cleanup --------------------------------*/
  /*----------------------------------------------------------------------------*/
  (void)state; /* unused */
}

static void UTest_Nvm_Int_ProcessingActiveRequests_005(void **state)
{
  uint8 listIdx = 0;
  uint16 idx = 0;
  Nvm_ReturnType fncRetVal;
  uint16 testBlkId;
  Nvm_BlockStatusType testBlkStatus, testBlkStatus2;
  uint8 testDataWriteBlk[10] = { 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA };
  uint8 testDataReadBlk[10];

  /*----------------------------------------------------------------------------*/
  /*------------------------------- Test Setup ---------------------------------*/
  /*----------------------------------------------------------------------------*/
  printf("[ DESCRIP  ] >> Check the processing active requests functionality of the Nvm sub module \"Int\"<< \r\n");
  printf("[          ] >> Check functionality if element is pending >> Sub module element is DATA_E_INTEGRITY_FAILED \r\n");

  Nvm_Int_Init();
  Nvm_RequestList_Init();

  expect_function_call(FlsEep_RequestBlockStatus);
  will_return(FlsEep_RequestBlockStatus, FLSEEP_BLK_DEMOBLK2); /* Check Argument */
  will_return(FlsEep_RequestBlockStatus, DATA_E_INTEGRITY_FAILED); /* Set return value */

  nvmRequestList[1].nvmBlockId = NVM_BLK_ID_DEMO_BLK2;
  nvmRequestList[1].nvmRequestStatus = NVM_BLK_PENDING;
  nvmRequestList[1].nvmRequestType = NVM_REQ_ERASE;
  nvmRequestList[1].requestDataPointer = NULL_PTR;

  /* Ecpect list element cleanup */
  expect_function_call(Nvm_RequestList_HlpInvalidateListElement);
  will_return(Nvm_RequestList_HlpInvalidateListElement, 1);

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Execution ------------------------------*/
  /*----------------------------------------------------------------------------*/
  Nvm_Int_ProcessActiveRequests();

  /* Check modified request list element */
  assert_int_equal(nvmRequestList[0].nvmBlockId, 0xFFFF);
  assert_int_equal(nvmRequestList[0].requestDataPointer, NULL_PTR);
  assert_int_equal(nvmRequestList[0].nvmRequestStatus, NVM_BLK_EMPTY);
  assert_int_equal(nvmRequestList[0].nvmRequestType, NVM_REQ_ERASE);
  assert_int_equal(nvmRequestList[0].nvmSafe, 0x00);

  for (idx = 0; idx < NVM_MAX_REQUEST_DATA_SIZE; idx++)
  {
    assert_int_equal(nvmRequestList[1].nvmData[idx], 0x00);
  }


  /* Check list element before and after the request list element */
  /* Before */
  assert_int_equal(nvmRequestList[0].nvmBlockId, 0xFFFF);
  assert_int_equal(nvmRequestList[0].requestDataPointer, NULL_PTR);
  assert_int_equal(nvmRequestList[0].nvmRequestStatus, NVM_BLK_EMPTY);
  assert_int_equal(nvmRequestList[0].nvmRequestType, NVM_REQ_ERASE);
  assert_int_equal(nvmRequestList[0].nvmSafe, 0x00);

  for (idx = 0; idx < NVM_MAX_REQUEST_DATA_SIZE; idx++)
  {
    assert_int_equal(nvmRequestList[0].nvmData[idx], 0x00);
  }

  /* After */
  assert_int_equal(nvmRequestList[2].nvmBlockId, 0xFFFF);
  assert_int_equal(nvmRequestList[2].requestDataPointer, NULL_PTR);
  assert_int_equal(nvmRequestList[2].nvmRequestStatus, NVM_BLK_EMPTY);
  assert_int_equal(nvmRequestList[2].nvmRequestType, NVM_REQ_ERASE);
  assert_int_equal(nvmRequestList[2].nvmSafe, 0x00);

  for (idx = 0; idx < NVM_MAX_REQUEST_DATA_SIZE; idx++)
  {
    assert_int_equal(nvmRequestList[1].nvmData[idx], 0x00);
  }

  /* Check status list elements before >> requested >> after */
  assert_int_equal(nvmBlockStatusList[0], NVM_BLK_OK);
  assert_int_equal(nvmBlockStatusList[1], NVM_BLK_INTEGRITY_FAILED);
  assert_int_equal(nvmBlockStatusList[2], NVM_BLK_OK);
  assert_int_equal(nvmProcessNewReq_LastIdx, 0x00);

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Cleanup --------------------------------*/
  /*----------------------------------------------------------------------------*/
  (void)state; /* unused */
}

static void UTest_Nvm_Int_ProcessingActiveRequests_006(void **state)
{
  uint8 listIdx = 0;
  uint16 idx = 0;
  Nvm_ReturnType fncRetVal;
  uint16 testBlkId;
  Nvm_BlockStatusType testBlkStatus, testBlkStatus2;
  uint8 testDataWriteBlk[10] = { 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA };
  uint8 testDataReadBlk[10];

  /*----------------------------------------------------------------------------*/
  /*------------------------------- Test Setup ---------------------------------*/
  /*----------------------------------------------------------------------------*/
  printf("[ DESCRIP  ] >> Check the processing active requests functionality of the Nvm sub module \"Int\"<< \r\n");
  printf("[          ] >> Check functionality if element is pending >> Sub module element is DATA_E_INVALIDATE \r\n");

  Nvm_Int_Init();
  Nvm_RequestList_Init();

  expect_function_call(FlsEep_RequestBlockStatus);
  will_return(FlsEep_RequestBlockStatus, FLSEEP_BLK_DEMOBLK2); /* Check Argument */
  will_return(FlsEep_RequestBlockStatus, DATA_E_INVALIDATE); /* Set return value */

  nvmRequestList[1].nvmBlockId = NVM_BLK_ID_DEMO_BLK2;
  nvmRequestList[1].nvmRequestStatus = NVM_BLK_PENDING;
  nvmRequestList[1].nvmRequestType = NVM_REQ_ERASE;
  nvmRequestList[1].requestDataPointer = NULL_PTR;

  /* Ecpect list element cleanup */
  expect_function_call(Nvm_RequestList_HlpInvalidateListElement);
  will_return(Nvm_RequestList_HlpInvalidateListElement, 1);

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Execution ------------------------------*/
  /*----------------------------------------------------------------------------*/
  Nvm_Int_ProcessActiveRequests();

  /* Check modified request list element */
  assert_int_equal(nvmRequestList[0].nvmBlockId, 0xFFFF);
  assert_int_equal(nvmRequestList[0].requestDataPointer, NULL_PTR);
  assert_int_equal(nvmRequestList[0].nvmRequestStatus, NVM_BLK_EMPTY);
  assert_int_equal(nvmRequestList[0].nvmRequestType, NVM_REQ_ERASE);
  assert_int_equal(nvmRequestList[0].nvmSafe, 0x00);

  for (idx = 0; idx < NVM_MAX_REQUEST_DATA_SIZE; idx++)
  {
    assert_int_equal(nvmRequestList[1].nvmData[idx], 0x00);
  }


  /* Check list element before and after the request list element */
  /* Before */
  assert_int_equal(nvmRequestList[0].nvmBlockId, 0xFFFF);
  assert_int_equal(nvmRequestList[0].requestDataPointer, NULL_PTR);
  assert_int_equal(nvmRequestList[0].nvmRequestStatus, NVM_BLK_EMPTY);
  assert_int_equal(nvmRequestList[0].nvmRequestType, NVM_REQ_ERASE);
  assert_int_equal(nvmRequestList[0].nvmSafe, 0x00);

  for (idx = 0; idx < NVM_MAX_REQUEST_DATA_SIZE; idx++)
  {
    assert_int_equal(nvmRequestList[0].nvmData[idx], 0x00);
  }

  /* After */
  assert_int_equal(nvmRequestList[2].nvmBlockId, 0xFFFF);
  assert_int_equal(nvmRequestList[2].requestDataPointer, NULL_PTR);
  assert_int_equal(nvmRequestList[2].nvmRequestStatus, NVM_BLK_EMPTY);
  assert_int_equal(nvmRequestList[2].nvmRequestType, NVM_REQ_ERASE);
  assert_int_equal(nvmRequestList[2].nvmSafe, 0x00);

  for (idx = 0; idx < NVM_MAX_REQUEST_DATA_SIZE; idx++)
  {
    assert_int_equal(nvmRequestList[1].nvmData[idx], 0x00);
  }

  /* Check status list elements before >> requested >> after */
  assert_int_equal(nvmBlockStatusList[0], NVM_BLK_OK);
  assert_int_equal(nvmBlockStatusList[1], NVM_BLK_INVALIDATE);
  assert_int_equal(nvmBlockStatusList[2], NVM_BLK_OK);
  assert_int_equal(nvmProcessNewReq_LastIdx, 0x00);

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Cleanup --------------------------------*/
  /*----------------------------------------------------------------------------*/
  (void)state; /* unused */
}

static void UTest_Nvm_Int_ProcessingActiveRequests_007(void **state)
{
  uint8 listIdx = 0;
  uint16 idx = 0;
  Nvm_ReturnType fncRetVal;
  uint16 testBlkId;
  Nvm_BlockStatusType testBlkStatus, testBlkStatus2;
  uint8 testDataWriteBlk[10] = { 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA };
  uint8 testDataReadBlk[10];

  /*----------------------------------------------------------------------------*/
  /*------------------------------- Test Setup ---------------------------------*/
  /*----------------------------------------------------------------------------*/
  printf("[ DESCRIP  ] >> Check the processing active requests functionality of the Nvm sub module \"Int\"<< \r\n");
  printf("[          ] >> Check functionality if element is pending >> Sub module element is DATA_E_OK \r\n");
  printf("[          ] >> Check functionality if element is requested as writing.\r\n");

  Nvm_Int_Init();
  Nvm_RequestList_Init();

  expect_function_call(FlsEep_RequestBlockStatus);
  will_return(FlsEep_RequestBlockStatus, FLSEEP_BLK_DEMOBLK2); /* Check Argument */
  will_return(FlsEep_RequestBlockStatus, DATA_E_OK); /* Set return value */

  nvmRequestList[1].nvmBlockId = NVM_BLK_ID_DEMO_BLK2;
  nvmRequestList[1].nvmRequestStatus = NVM_BLK_PENDING;
  nvmRequestList[1].nvmRequestType = NVM_REQ_WRITE;
  nvmRequestList[1].requestDataPointer = &testDataWriteBlk;

  /* Ecpect list element cleanup */
  expect_function_call(Nvm_RequestList_HlpInvalidateListElement);
  will_return(Nvm_RequestList_HlpInvalidateListElement, 1);

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Execution ------------------------------*/
  /*----------------------------------------------------------------------------*/
  Nvm_Int_ProcessActiveRequests();

  /* Check modified request list element */
  assert_int_equal(nvmRequestList[0].nvmBlockId, 0xFFFF);
  assert_int_equal(nvmRequestList[0].requestDataPointer, NULL_PTR);
  assert_int_equal(nvmRequestList[0].nvmRequestStatus, NVM_BLK_EMPTY);
  assert_int_equal(nvmRequestList[0].nvmRequestType, NVM_REQ_ERASE);
  assert_int_equal(nvmRequestList[0].nvmSafe, 0x00);

  for (idx = 0; idx < NVM_MAX_REQUEST_DATA_SIZE; idx++)
  {
    assert_int_equal(nvmRequestList[1].nvmData[idx], 0x00);
  }


  /* Check list element before and after the request list element */
  /* Before */
  assert_int_equal(nvmRequestList[0].nvmBlockId, 0xFFFF);
  assert_int_equal(nvmRequestList[0].requestDataPointer, NULL_PTR);
  assert_int_equal(nvmRequestList[0].nvmRequestStatus, NVM_BLK_EMPTY);
  assert_int_equal(nvmRequestList[0].nvmRequestType, NVM_REQ_ERASE);
  assert_int_equal(nvmRequestList[0].nvmSafe, 0x00);

  for (idx = 0; idx < NVM_MAX_REQUEST_DATA_SIZE; idx++)
  {
    assert_int_equal(nvmRequestList[0].nvmData[idx], 0x00);
  }

  /* After */
  assert_int_equal(nvmRequestList[2].nvmBlockId, 0xFFFF);
  assert_int_equal(nvmRequestList[2].requestDataPointer, NULL_PTR);
  assert_int_equal(nvmRequestList[2].nvmRequestStatus, NVM_BLK_EMPTY);
  assert_int_equal(nvmRequestList[2].nvmRequestType, NVM_REQ_ERASE);
  assert_int_equal(nvmRequestList[2].nvmSafe, 0x00);

  for (idx = 0; idx < NVM_MAX_REQUEST_DATA_SIZE; idx++)
  {
    assert_int_equal(nvmRequestList[1].nvmData[idx], 0x00);
  }

  /* Check status list elements before >> requested >> after */
  assert_int_equal(nvmBlockStatusList[0], NVM_BLK_OK);
  assert_int_equal(nvmBlockStatusList[1], NVM_BLK_OK);
  assert_int_equal(nvmBlockStatusList[2], NVM_BLK_OK);
  assert_int_equal(nvmProcessNewReq_LastIdx, 0x00);

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Cleanup --------------------------------*/
  /*----------------------------------------------------------------------------*/
  (void)state; /* unused */
}

static void UTest_Nvm_Int_ProcessingActiveRequests_008(void **state)
{
  uint8 listIdx = 0;
  uint16 idx = 0;
  Nvm_ReturnType fncRetVal;
  uint16 testBlkId;
  Nvm_BlockStatusType testBlkStatus, testBlkStatus2;
  uint8 testDataWriteBlk[10] = { 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA };
  uint8 testDataReadBlk[10];

  /*----------------------------------------------------------------------------*/
  /*------------------------------- Test Setup ---------------------------------*/
  /*----------------------------------------------------------------------------*/
  printf("[ DESCRIP  ] >> Check the processing active requests functionality of the Nvm sub module \"Int\"<< \r\n");
  printf("[          ] >> Check functionality if element is pending >> Sub module element is DATA_E_OK \r\n");
  printf("[          ] >> Check functionality if element is requested as erasing .\r\n");

  Nvm_Int_Init();
  Nvm_RequestList_Init();

  expect_function_call(FlsEep_RequestBlockStatus);
  will_return(FlsEep_RequestBlockStatus, FLSEEP_BLK_DEMOBLK2); /* Check Argument */
  will_return(FlsEep_RequestBlockStatus, DATA_E_OK); /* Set return value */

  nvmRequestList[1].nvmBlockId = NVM_BLK_ID_DEMO_BLK2;
  nvmRequestList[1].nvmRequestStatus = NVM_BLK_PENDING;
  nvmRequestList[1].nvmRequestType = NVM_REQ_ERASE;
  nvmRequestList[1].requestDataPointer = &testDataWriteBlk;

  /* Ecpect list element cleanup */
  expect_function_call(Nvm_RequestList_HlpInvalidateListElement);
  will_return(Nvm_RequestList_HlpInvalidateListElement, 1);

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Execution ------------------------------*/
  /*----------------------------------------------------------------------------*/
  Nvm_Int_ProcessActiveRequests();

  /* Check modified request list element */
  assert_int_equal(nvmRequestList[0].nvmBlockId, 0xFFFF);
  assert_int_equal(nvmRequestList[0].requestDataPointer, NULL_PTR);
  assert_int_equal(nvmRequestList[0].nvmRequestStatus, NVM_BLK_EMPTY);
  assert_int_equal(nvmRequestList[0].nvmRequestType, NVM_REQ_ERASE);
  assert_int_equal(nvmRequestList[0].nvmSafe, 0x00);

  for (idx = 0; idx < NVM_MAX_REQUEST_DATA_SIZE; idx++)
  {
    assert_int_equal(nvmRequestList[1].nvmData[idx], 0x00);
  }


  /* Check list element before and after the request list element */
  /* Before */
  assert_int_equal(nvmRequestList[0].nvmBlockId, 0xFFFF);
  assert_int_equal(nvmRequestList[0].requestDataPointer, NULL_PTR);
  assert_int_equal(nvmRequestList[0].nvmRequestStatus, NVM_BLK_EMPTY);
  assert_int_equal(nvmRequestList[0].nvmRequestType, NVM_REQ_ERASE);
  assert_int_equal(nvmRequestList[0].nvmSafe, 0x00);

  for (idx = 0; idx < NVM_MAX_REQUEST_DATA_SIZE; idx++)
  {
    assert_int_equal(nvmRequestList[0].nvmData[idx], 0x00);
  }

  /* After */
  assert_int_equal(nvmRequestList[2].nvmBlockId, 0xFFFF);
  assert_int_equal(nvmRequestList[2].requestDataPointer, NULL_PTR);
  assert_int_equal(nvmRequestList[2].nvmRequestStatus, NVM_BLK_EMPTY);
  assert_int_equal(nvmRequestList[2].nvmRequestType, NVM_REQ_ERASE);
  assert_int_equal(nvmRequestList[2].nvmSafe, 0x00);

  for (idx = 0; idx < NVM_MAX_REQUEST_DATA_SIZE; idx++)
  {
    assert_int_equal(nvmRequestList[1].nvmData[idx], 0x00);
  }

  /* Check status list elements before >> requested >> after */
  assert_int_equal(nvmBlockStatusList[0], NVM_BLK_OK);
  assert_int_equal(nvmBlockStatusList[1], NVM_BLK_OK);
  assert_int_equal(nvmBlockStatusList[2], NVM_BLK_OK);
  assert_int_equal(nvmProcessNewReq_LastIdx, 0x00);

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Cleanup --------------------------------*/
  /*----------------------------------------------------------------------------*/
  (void)state; /* unused */
}

static void UTest_Nvm_Int_ProcessingActiveRequests_009(void **state)
{
  uint8 listIdx = 0;
  uint16 idx = 0;
  Nvm_ReturnType fncRetVal;
  uint16 testBlkId;
  Nvm_BlockStatusType testBlkStatus, testBlkStatus2;
  uint8 testDataWriteBlk[10] = { 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA };
  uint8 testDataReadBlk[10];

  /*----------------------------------------------------------------------------*/
  /*------------------------------- Test Setup ---------------------------------*/
  /*----------------------------------------------------------------------------*/
  printf("[ DESCRIP  ] >> Check the processing active requests functionality of the Nvm sub module \"Int\"<< \r\n");
  printf("[          ] >> Check functionality if element is pending >> Sub module element is DATA_E_OK \r\n");
  printf("[          ] >> Check functionality if element is requested as reading .\r\n");
  printf("[          ] >> Check functionality if element has not requested a CRC check.\r\n");

  Nvm_Int_Init();
  Nvm_RequestList_Init();

  expect_function_call(FlsEep_RequestBlockStatus);
  will_return(FlsEep_RequestBlockStatus, FLSEEP_BLK_DEMOBLK2); /* Check Argument */
  will_return(FlsEep_RequestBlockStatus, DATA_E_OK); /* Set return value */

  nvmRequestList[1].nvmBlockId = NVM_BLK_ID_DEMO_BLK2;
  nvmRequestList[1].nvmRequestStatus = NVM_BLK_PENDING;
  nvmRequestList[1].nvmRequestType = NVM_REQ_READ;
  nvmRequestList[1].requestDataPointer = &testDataReadBlk;

  /* Copy data into nvm request data buffer */
  for (idx = 0; idx < 10; idx++)
  {
    nvmRequestList[1].nvmData[idx] = testDataWriteBlk[idx];

  }

  /* Set user block status to pending */
  nvmBlockStatusList[1] = NVM_BLK_PENDING;

  /* Ecpect list element cleanup */
  expect_function_call(Nvm_RequestList_HlpInvalidateListElement);
  will_return(Nvm_RequestList_HlpInvalidateListElement, 1);

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Execution ------------------------------*/
  /*----------------------------------------------------------------------------*/
  Nvm_Int_ProcessActiveRequests();

  /* Check modified request list element */
  assert_int_equal(nvmRequestList[0].nvmBlockId, 0xFFFF);
  assert_int_equal(nvmRequestList[0].requestDataPointer, NULL_PTR);
  assert_int_equal(nvmRequestList[0].nvmRequestStatus, NVM_BLK_EMPTY);
  assert_int_equal(nvmRequestList[0].nvmRequestType, NVM_REQ_ERASE);
  assert_int_equal(nvmRequestList[0].nvmSafe, 0x00);

  for (idx = 0; idx < NVM_MAX_REQUEST_DATA_SIZE; idx++)
  {
    assert_int_equal(nvmRequestList[0].nvmData[idx], 0x00);
  }

  /* Check user data */
  for (idx = 0; idx < 10; idx++)
  {
    assert_int_equal(testDataReadBlk[idx], testDataWriteBlk[idx]);
  }


  /* Check list element before and after the request list element */
  /* Before */
  assert_int_equal(nvmRequestList[0].nvmBlockId, 0xFFFF);
  assert_int_equal(nvmRequestList[0].requestDataPointer, NULL_PTR);
  assert_int_equal(nvmRequestList[0].nvmRequestStatus, NVM_BLK_EMPTY);
  assert_int_equal(nvmRequestList[0].nvmRequestType, NVM_REQ_ERASE);
  assert_int_equal(nvmRequestList[0].nvmSafe, 0x00);

  for (idx = 0; idx < NVM_MAX_REQUEST_DATA_SIZE; idx++)
  {
    assert_int_equal(nvmRequestList[0].nvmData[idx], 0x00);
  }

  /* After */
  assert_int_equal(nvmRequestList[2].nvmBlockId, 0xFFFF);
  assert_int_equal(nvmRequestList[2].requestDataPointer, NULL_PTR);
  assert_int_equal(nvmRequestList[2].nvmRequestStatus, NVM_BLK_EMPTY);
  assert_int_equal(nvmRequestList[2].nvmRequestType, NVM_REQ_ERASE);
  assert_int_equal(nvmRequestList[2].nvmSafe, 0x00);

  for (idx = 0; idx < NVM_MAX_REQUEST_DATA_SIZE; idx++)
  {
    assert_int_equal(nvmRequestList[1].nvmData[idx], 0x00);
  }

  /* Check status list elements before >> requested >> after */
  assert_int_equal(nvmBlockStatusList[0], NVM_BLK_OK);
  assert_int_equal(nvmBlockStatusList[1], NVM_BLK_OK);
  assert_int_equal(nvmBlockStatusList[2], NVM_BLK_OK);
  assert_int_equal(nvmProcessNewReq_LastIdx, 0x00);

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Cleanup --------------------------------*/
  /*----------------------------------------------------------------------------*/
  (void)state; /* unused */
}

static void UTest_Nvm_Int_ProcessingActiveRequests_010(void **state)
{
  uint8 listIdx = 0;
  uint16 idx = 0;
  Nvm_ReturnType fncRetVal;
  uint16 testBlkId;
  Nvm_BlockStatusType testBlkStatus, testBlkStatus2;
  uint8 testDataWriteBlk[12] = { 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0x00, 0x00 };
  uint8 testDataReadBlk[12] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

  /*----------------------------------------------------------------------------*/
  /*------------------------------- Test Setup ---------------------------------*/
  /*----------------------------------------------------------------------------*/
  printf("[ DESCRIP  ] >> Check the processing active requests functionality of the Nvm sub module \"Int\"<< \r\n");
  printf("[          ] >> Check functionality if element is pending >> Sub module element is DATA_E_OK \r\n");
  printf("[          ] >> Check functionality if element is requested as reading .\r\n");
  printf("[          ] >> Check functionality if element has requested a CRC check >> CRC valid \r\n");

  Nvm_Int_Init();
  Nvm_RequestList_Init();

  expect_function_call(FlsEep_RequestBlockStatus);
  will_return(FlsEep_RequestBlockStatus, FLSEEP_BLK_DEMOBLK); /* Check Argument */
  will_return(FlsEep_RequestBlockStatus, DATA_E_OK); /* Set return value */

  nvmRequestList[1].nvmBlockId = NVM_BLK_ID_DEMO_BLK;
  nvmRequestList[1].nvmRequestStatus = NVM_BLK_PENDING;
  nvmRequestList[1].nvmRequestType = NVM_REQ_READ;
  nvmRequestList[1].requestDataPointer = &testDataReadBlk;

  /* Copy data into nvm request data buffer */
  for (idx = 0; idx < 10; idx++)
  {
    nvmRequestList[1].nvmData[idx] = testDataWriteBlk[idx];

  }
  nvmRequestList[1].nvmData[10] = 0x60;
  nvmRequestList[1].nvmData[11] = 0x9F;
  nvmRequestList[1].nvmData[12] = 0x98;
  nvmRequestList[1].nvmData[13] = 0xF9;

  /* Set user block status to pending */
  nvmBlockStatusList[0] = NVM_BLK_PENDING;

  /* Ecpect list element cleanup */
  expect_function_call(Nvm_RequestList_HlpInvalidateListElement);
  will_return(Nvm_RequestList_HlpInvalidateListElement, 1);

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Execution ------------------------------*/
  /*----------------------------------------------------------------------------*/
  Nvm_Int_ProcessActiveRequests();

  /* Check modified request list element */
  assert_int_equal(nvmRequestList[0].nvmBlockId, 0xFFFF);
  assert_int_equal(nvmRequestList[0].requestDataPointer, NULL_PTR);
  assert_int_equal(nvmRequestList[0].nvmRequestStatus, NVM_BLK_EMPTY);
  assert_int_equal(nvmRequestList[0].nvmRequestType, NVM_REQ_ERASE);
  assert_int_equal(nvmRequestList[0].nvmSafe, 0x00);

  for (idx = 0; idx < NVM_MAX_REQUEST_DATA_SIZE; idx++)
  {
    assert_int_equal(nvmRequestList[0].nvmData[idx], 0x00);
  }

  /* Check user data */
  for (idx = 0; idx < 12; idx++)
  {
    assert_int_equal(testDataReadBlk[idx], testDataWriteBlk[idx]);
  }


  /* Check list element before and after the request list element */
  /* Before */
  assert_int_equal(nvmRequestList[0].nvmBlockId, 0xFFFF);
  assert_int_equal(nvmRequestList[0].requestDataPointer, NULL_PTR);
  assert_int_equal(nvmRequestList[0].nvmRequestStatus, NVM_BLK_EMPTY);
  assert_int_equal(nvmRequestList[0].nvmRequestType, NVM_REQ_ERASE);
  assert_int_equal(nvmRequestList[0].nvmSafe, 0x00);

  for (idx = 0; idx < NVM_MAX_REQUEST_DATA_SIZE; idx++)
  {
    assert_int_equal(nvmRequestList[0].nvmData[idx], 0x00);
  }

  /* After */
  assert_int_equal(nvmRequestList[2].nvmBlockId, 0xFFFF);
  assert_int_equal(nvmRequestList[2].requestDataPointer, NULL_PTR);
  assert_int_equal(nvmRequestList[2].nvmRequestStatus, NVM_BLK_EMPTY);
  assert_int_equal(nvmRequestList[2].nvmRequestType, NVM_REQ_ERASE);
  assert_int_equal(nvmRequestList[2].nvmSafe, 0x00);

  for (idx = 0; idx < NVM_MAX_REQUEST_DATA_SIZE; idx++)
  {
    assert_int_equal(nvmRequestList[1].nvmData[idx], 0x00);
  }

  /* Check status list elements before >> requested >> after */
  assert_int_equal(nvmBlockStatusList[0], NVM_BLK_OK);
  assert_int_equal(nvmBlockStatusList[1], NVM_BLK_OK);
  assert_int_equal(nvmBlockStatusList[2], NVM_BLK_OK);
  assert_int_equal(nvmProcessNewReq_LastIdx, 0x00);

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Cleanup --------------------------------*/
  /*----------------------------------------------------------------------------*/
  (void)state; /* unused */
}

static void UTest_Nvm_Int_ProcessingActiveRequests_011(void **state)
{
  uint8 listIdx = 0;
  uint16 idx = 0;
  Nvm_ReturnType fncRetVal;
  uint16 testBlkId;
  Nvm_BlockStatusType testBlkStatus, testBlkStatus2;
  uint8 testDataWriteBlk[12] = { 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0x00, 0x00 };
  uint8 testDataReadBlk[12] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

  /*----------------------------------------------------------------------------*/
  /*------------------------------- Test Setup ---------------------------------*/
  /*----------------------------------------------------------------------------*/
  printf("[ DESCRIP  ] >> Check the processing active requests functionality of the Nvm sub module \"Int\"<< \r\n");
  printf("[          ] >> Check functionality if element is pending >> Sub module element is DATA_E_OK \r\n");
  printf("[          ] >> Check functionality if element is requested as reading .\r\n");
  printf("[          ] >> Check functionality if element has requested a CRC check >> CRC not valid \r\n");

  Nvm_Int_Init();
  Nvm_RequestList_Init();

  expect_function_call(FlsEep_RequestBlockStatus);
  will_return(FlsEep_RequestBlockStatus, FLSEEP_BLK_DEMOBLK); /* Check Argument */
  will_return(FlsEep_RequestBlockStatus, DATA_E_OK); /* Set return value */

  nvmRequestList[1].nvmBlockId = NVM_BLK_ID_DEMO_BLK;
  nvmRequestList[1].nvmRequestStatus = NVM_BLK_PENDING;
  nvmRequestList[1].nvmRequestType = NVM_REQ_READ;
  nvmRequestList[1].requestDataPointer = &testDataReadBlk;

  /* Copy data into nvm request data buffer */
  for (idx = 0; idx < 10; idx++)
  {
    nvmRequestList[1].nvmData[idx] = testDataWriteBlk[idx];

  }
  nvmRequestList[1].nvmData[10] = 0x60;
  nvmRequestList[1].nvmData[11] = 0x9F;
  nvmRequestList[1].nvmData[12] = 0x98;
  nvmRequestList[1].nvmData[13] = 0xFA;

  /* Set user block status to pending */
  nvmBlockStatusList[0] = NVM_BLK_PENDING;

  /* Ecpect list element cleanup */
  expect_function_call(Nvm_RequestList_HlpInvalidateListElement);
  will_return(Nvm_RequestList_HlpInvalidateListElement, 1);

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Execution ------------------------------*/
  /*----------------------------------------------------------------------------*/
  Nvm_Int_ProcessActiveRequests();

  /* Check modified request list element */
  assert_int_equal(nvmRequestList[0].nvmBlockId, 0xFFFF);
  assert_int_equal(nvmRequestList[0].requestDataPointer, NULL_PTR);
  assert_int_equal(nvmRequestList[0].nvmRequestStatus, NVM_BLK_EMPTY);
  assert_int_equal(nvmRequestList[0].nvmRequestType, NVM_REQ_ERASE);
  assert_int_equal(nvmRequestList[0].nvmSafe, 0x00);

  for (idx = 0; idx < NVM_MAX_REQUEST_DATA_SIZE; idx++)
  {
    assert_int_equal(nvmRequestList[0].nvmData[idx], 0x00);
  }

  /* Check user data */
  for (idx = 0; idx < 12; idx++)
  {
    assert_int_equal(testDataReadBlk[idx], 0x00);
  }


  /* Check list element before and after the request list element */
  /* Before */
  assert_int_equal(nvmRequestList[0].nvmBlockId, 0xFFFF);
  assert_int_equal(nvmRequestList[0].requestDataPointer, NULL_PTR);
  assert_int_equal(nvmRequestList[0].nvmRequestStatus, NVM_BLK_EMPTY);
  assert_int_equal(nvmRequestList[0].nvmRequestType, NVM_REQ_ERASE);
  assert_int_equal(nvmRequestList[0].nvmSafe, 0x00);

  for (idx = 0; idx < NVM_MAX_REQUEST_DATA_SIZE; idx++)
  {
    assert_int_equal(nvmRequestList[0].nvmData[idx], 0x00);
  }

  /* After */
  assert_int_equal(nvmRequestList[2].nvmBlockId, 0xFFFF);
  assert_int_equal(nvmRequestList[2].requestDataPointer, NULL_PTR);
  assert_int_equal(nvmRequestList[2].nvmRequestStatus, NVM_BLK_EMPTY);
  assert_int_equal(nvmRequestList[2].nvmRequestType, NVM_REQ_ERASE);
  assert_int_equal(nvmRequestList[2].nvmSafe, 0x00);

  for (idx = 0; idx < NVM_MAX_REQUEST_DATA_SIZE; idx++)
  {
    assert_int_equal(nvmRequestList[1].nvmData[idx], 0x00);
  }

  /* Check status list elements before >> requested >> after */
  assert_int_equal(nvmBlockStatusList[0], NVM_BLK_INTEGRITY_FAILED);
  assert_int_equal(nvmBlockStatusList[1], NVM_BLK_OK);
  assert_int_equal(nvmBlockStatusList[2], NVM_BLK_OK);
  assert_int_equal(nvmProcessNewReq_LastIdx, 0x00);

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Cleanup --------------------------------*/
  /*----------------------------------------------------------------------------*/
  (void)state; /* unused */
}

static void UTest_Nvm_Int_ProcessingActiveRequests_012(void **state)
{
  uint8 listIdx = 0;
  uint16 idx = 0;
  Nvm_ReturnType fncRetVal;
  uint16 testBlkId;
  Nvm_BlockStatusType testBlkStatus, testBlkStatus2;
  uint8 testDataWriteBlk[12] = { 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0x00, 0x00 };
  uint8 testDataReadBlk[12] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

  /*----------------------------------------------------------------------------*/
  /*------------------------------- Test Setup ---------------------------------*/
  /*----------------------------------------------------------------------------*/
  printf("[ DESCRIP  ] >> Check the processing active requests functionality of the Nvm sub module \"Int\"<< \r\n");
  printf("[          ] >> Check functionality if element is pending >> Sub module element is DATA_E_OK \r\n");
  printf("[          ] >> Check functionality if element is requested as reading .\r\n");
  printf("[          ] >> Check functionality if element has requested a CRC check >> CRC valid \r\n");
  printf("[          ] >> nvm data save element corrupted. \r\n");

  Nvm_Int_Init();
  Nvm_RequestList_Init();

  expect_function_call(FlsEep_RequestBlockStatus);
  will_return(FlsEep_RequestBlockStatus, FLSEEP_BLK_DEMOBLK); /* Check Argument */
  will_return(FlsEep_RequestBlockStatus, DATA_E_OK); /* Set return value */

  nvmRequestList[1].nvmBlockId = NVM_BLK_ID_DEMO_BLK;
  nvmRequestList[1].nvmRequestStatus = NVM_BLK_PENDING;
  nvmRequestList[1].nvmRequestType = NVM_REQ_READ;
  nvmRequestList[1].requestDataPointer = &testDataReadBlk;

  /* Copy data into nvm request data buffer */
  for (idx = 0; idx < 10; idx++)
  {
    nvmRequestList[1].nvmData[idx] = testDataWriteBlk[idx];

  }
  nvmRequestList[1].nvmData[10] = 0x60;
  nvmRequestList[1].nvmData[11] = 0x9F;
  nvmRequestList[1].nvmData[12] = 0x98;
  nvmRequestList[1].nvmData[13] = 0xF9;
  nvmRequestList[1].nvmSafe = 0xAA;

  /* Set user block status to pending */
  nvmBlockStatusList[0] = NVM_BLK_PENDING;

  /* Ecpect list element cleanup */
  expect_function_call(Nvm_RequestList_HlpInvalidateListElement);
  will_return(Nvm_RequestList_HlpInvalidateListElement, 1);

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Execution ------------------------------*/
  /*----------------------------------------------------------------------------*/
  Nvm_Int_ProcessActiveRequests();

  /* Check modified request list element */
  assert_int_equal(nvmRequestList[0].nvmBlockId, 0xFFFF);
  assert_int_equal(nvmRequestList[0].requestDataPointer, NULL_PTR);
  assert_int_equal(nvmRequestList[0].nvmRequestStatus, NVM_BLK_EMPTY);
  assert_int_equal(nvmRequestList[0].nvmRequestType, NVM_REQ_ERASE);
  assert_int_equal(nvmRequestList[0].nvmSafe, 0x00);

  for (idx = 0; idx < NVM_MAX_REQUEST_DATA_SIZE; idx++)
  {
    assert_int_equal(nvmRequestList[0].nvmData[idx], 0x00);
  }

  /* Check user data */
  for (idx = 0; idx < 12; idx++)
  {
    assert_int_equal(testDataReadBlk[idx], 0x00);
  }


  /* Check list element before and after the request list element */
  /* Before */
  assert_int_equal(nvmRequestList[0].nvmBlockId, 0xFFFF);
  assert_int_equal(nvmRequestList[0].requestDataPointer, NULL_PTR);
  assert_int_equal(nvmRequestList[0].nvmRequestStatus, NVM_BLK_EMPTY);
  assert_int_equal(nvmRequestList[0].nvmRequestType, NVM_REQ_ERASE);
  assert_int_equal(nvmRequestList[0].nvmSafe, 0x00);

  for (idx = 0; idx < NVM_MAX_REQUEST_DATA_SIZE; idx++)
  {
    assert_int_equal(nvmRequestList[0].nvmData[idx], 0x00);
  }

  /* After */
  assert_int_equal(nvmRequestList[2].nvmBlockId, 0xFFFF);
  assert_int_equal(nvmRequestList[2].requestDataPointer, NULL_PTR);
  assert_int_equal(nvmRequestList[2].nvmRequestStatus, NVM_BLK_EMPTY);
  assert_int_equal(nvmRequestList[2].nvmRequestType, NVM_REQ_ERASE);
  assert_int_equal(nvmRequestList[2].nvmSafe, 0x00);

  for (idx = 0; idx < NVM_MAX_REQUEST_DATA_SIZE; idx++)
  {
    assert_int_equal(nvmRequestList[1].nvmData[idx], 0x00);
  }

  /* Check status list elements before >> requested >> after */
  assert_int_equal(nvmBlockStatusList[0], NVM_BLK_INTEGRITY_FAILED);
  assert_int_equal(nvmBlockStatusList[1], NVM_BLK_OK);
  assert_int_equal(nvmBlockStatusList[2], NVM_BLK_OK);
  assert_int_equal(nvmProcessNewReq_LastIdx, 0x00);

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Cleanup --------------------------------*/
  /*----------------------------------------------------------------------------*/
  (void)state; /* unused */
}





static void UTest_Nvm_Int_ProcessingNewRequests_101(void **state)
{
  uint8 listIdx = 0;
  uint16 idx = 0;
  Nvm_ReturnType fncRetVal;
  uint16 testBlkId;
  Nvm_BlockStatusType testBlkStatus, testBlkStatus2;

  /*----------------------------------------------------------------------------*/
  /*------------------------------- Test Setup ---------------------------------*/
  /*----------------------------------------------------------------------------*/
  printf("[ DESCRIP  ] >> Check the processing active requests functionality of the Nvm sub module \"Int\"<< \r\n");
  printf("[          ] >> Check functionality if no element is waiting >> List is empty \r\n");

  Nvm_Int_Init();
  Nvm_RequestList_Init();

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Execution ------------------------------*/
  /*----------------------------------------------------------------------------*/
  Nvm_Int_ProcessNewRequests();

  /* Check request list */
  for (listIdx = 0; listIdx < NVM_REQUEST_LIST_SIZE; listIdx++)
  {
    assert_int_equal(nvmRequestList[listIdx].nvmBlockId, 0xFFFF);
    assert_int_equal(nvmRequestList[listIdx].requestDataPointer, NULL_PTR);
    assert_int_equal(nvmRequestList[listIdx].nvmRequestStatus, NVM_BLK_EMPTY);
    assert_int_equal(nvmRequestList[listIdx].nvmRequestType, NVM_REQ_ERASE);
    assert_int_equal(nvmRequestList[listIdx].nvmSafe, 0x00);

    for (idx = 0; idx < NVM_MAX_REQUEST_DATA_SIZE; idx++)
    {
      assert_int_equal(nvmRequestList[listIdx].nvmData[idx], 0x00);
    }
  }

  /* Check status list */
  for (idx = 0; idx < NVM_CNT_CONFIGURED_BLOCKS; idx++)
  {
    assert_int_equal(nvmBlockStatusList[idx], NVM_BLK_OK);
  }
  assert_int_equal(nvmProcessNewReq_LastIdx, 0x00);

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Cleanup --------------------------------*/
  /*----------------------------------------------------------------------------*/
  (void)state; /* unused */
}

static void UTest_Nvm_Int_ProcessingNewRequests_102(void **state)
{
  uint8 listIdx = 0;
  uint16 idx = 0;
  Nvm_ReturnType fncRetVal;
  uint16 testBlkId;
  Nvm_BlockStatusType testBlkStatus, testBlkStatus2;
  uint8 testDataWriteBlk[12] = { 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0x00, 0x00 };
  uint8 testDataReadBlk[12] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

  /*----------------------------------------------------------------------------*/
  /*------------------------------- Test Setup ---------------------------------*/
  /*----------------------------------------------------------------------------*/
  printf("[ DESCRIP  ] >> Check the processing active requests functionality of the Nvm sub module \"Int\"<< \r\n");
  printf("[          ] >> Check functionality if element is waiting >> Waiting Element Request NVM_REQ_ERASE \r\n");
  printf("[          ] >> Lower module request return value E_OK \r\n");

  Nvm_Int_Init();
  Nvm_RequestList_Init();

  nvmRequestList[1].nvmBlockId = NVM_BLK_ID_DEMO_BLK;
  nvmRequestList[1].nvmRequestStatus = NVM_BLK_WAITING;
  nvmRequestList[1].nvmRequestType = NVM_REQ_ERASE;
  nvmRequestList[1].requestDataPointer = NULL_PTR;

  /* Expect Write Data */
  expect_function_call(FlsEep_RequestWriteData);
  will_return(FlsEep_RequestWriteData, FLSEEP_BLK_DEMOBLK);
  will_return(FlsEep_RequestWriteData, &nvmRequestList[1].nvmData[0]);
  will_return(FlsEep_RequestWriteData, E_OK);


  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Execution ------------------------------*/
  /*----------------------------------------------------------------------------*/
  Nvm_Int_ProcessNewRequests();

  /* Check modified request list element */
  assert_int_equal(nvmRequestList[1].nvmBlockId, NVM_BLK_ID_DEMO_BLK);
  assert_int_equal(nvmRequestList[1].requestDataPointer, NULL_PTR);
  assert_int_equal(nvmRequestList[1].nvmRequestStatus, NVM_BLK_PENDING);
  assert_int_equal(nvmRequestList[1].nvmRequestType, NVM_REQ_ERASE);
  assert_int_equal(nvmRequestList[1].nvmSafe, 0x00);

  for (idx = 0; idx < NVM_MAX_REQUEST_DATA_SIZE; idx++)
  {
    assert_int_equal(nvmRequestList[1].nvmData[idx], 0x00);
  }

  /* Check list element before and after the request list element */
  /* Before */
  assert_int_equal(nvmRequestList[0].nvmBlockId, 0xFFFF);
  assert_int_equal(nvmRequestList[0].requestDataPointer, NULL_PTR);
  assert_int_equal(nvmRequestList[0].nvmRequestStatus, NVM_BLK_EMPTY);
  assert_int_equal(nvmRequestList[0].nvmRequestType, NVM_REQ_ERASE);
  assert_int_equal(nvmRequestList[0].nvmSafe, 0x00);

  for (idx = 0; idx < NVM_MAX_REQUEST_DATA_SIZE; idx++)
  {
    assert_int_equal(nvmRequestList[0].nvmData[idx], 0x00);
  }

  /* After */
  assert_int_equal(nvmRequestList[2].nvmBlockId, 0xFFFF);
  assert_int_equal(nvmRequestList[2].requestDataPointer, NULL_PTR);
  assert_int_equal(nvmRequestList[2].nvmRequestStatus, NVM_BLK_EMPTY);
  assert_int_equal(nvmRequestList[2].nvmRequestType, NVM_REQ_ERASE);
  assert_int_equal(nvmRequestList[2].nvmSafe, 0x00);

  for (idx = 0; idx < NVM_MAX_REQUEST_DATA_SIZE; idx++)
  {
    assert_int_equal(nvmRequestList[1].nvmData[idx], 0x00);
  }

  /* Check status list elements before >> requested >> after */
  assert_int_equal(nvmBlockStatusList[0], NVM_BLK_OK);
  assert_int_equal(nvmBlockStatusList[1], NVM_BLK_OK);
  assert_int_equal(nvmBlockStatusList[2], NVM_BLK_OK);
  assert_int_equal(nvmProcessNewReq_LastIdx, 0x00);

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Cleanup --------------------------------*/
  /*----------------------------------------------------------------------------*/
  (void)state; /* unused */
}

static void UTest_Nvm_Int_ProcessingNewRequests_103(void **state)
{
  uint8 listIdx = 0;
  uint16 idx = 0;
  Nvm_ReturnType fncRetVal;
  uint16 testBlkId;
  Nvm_BlockStatusType testBlkStatus, testBlkStatus2;
  uint8 testDataWriteBlk[12] = { 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0x00, 0x00 };
  uint8 testDataReadBlk[12] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

  /*----------------------------------------------------------------------------*/
  /*------------------------------- Test Setup ---------------------------------*/
  /*----------------------------------------------------------------------------*/
  printf("[ DESCRIP  ] >> Check the processing active requests functionality of the Nvm sub module \"Int\"<< \r\n");
  printf("[          ] >> Check functionality if element is waiting >> Waiting Element Request NVM_REQ_ERASE \r\n");
  printf("[          ] >> Lower module request return value E_SKIPPED \r\n");

  Nvm_Int_Init();
  Nvm_RequestList_Init();

  nvmRequestList[1].nvmBlockId = NVM_BLK_ID_DEMO_BLK;
  nvmRequestList[1].nvmRequestStatus = NVM_BLK_WAITING;
  nvmRequestList[1].nvmRequestType = NVM_REQ_ERASE;
  nvmRequestList[1].requestDataPointer = NULL_PTR;

  /* Expect Write Data */
  expect_function_call(FlsEep_RequestWriteData);
  will_return(FlsEep_RequestWriteData, FLSEEP_BLK_DEMOBLK);
  will_return(FlsEep_RequestWriteData, &nvmRequestList[1].nvmData[0]);
  will_return(FlsEep_RequestWriteData, E_SKIPPED);


  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Execution ------------------------------*/
  /*----------------------------------------------------------------------------*/
  Nvm_Int_ProcessNewRequests();

  /* Check modified request list element */
  assert_int_equal(nvmRequestList[1].nvmBlockId, NVM_BLK_ID_DEMO_BLK);
  assert_int_equal(nvmRequestList[1].requestDataPointer, NULL_PTR);
  assert_int_equal(nvmRequestList[1].nvmRequestStatus, NVM_BLK_WAITING);
  assert_int_equal(nvmRequestList[1].nvmRequestType, NVM_REQ_ERASE);
  assert_int_equal(nvmRequestList[1].nvmSafe, 0x00);

  for (idx = 0; idx < NVM_MAX_REQUEST_DATA_SIZE; idx++)
  {
    assert_int_equal(nvmRequestList[1].nvmData[idx], 0x00);
  }

  /* Check list element before and after the request list element */
  /* Before */
  assert_int_equal(nvmRequestList[0].nvmBlockId, 0xFFFF);
  assert_int_equal(nvmRequestList[0].requestDataPointer, NULL_PTR);
  assert_int_equal(nvmRequestList[0].nvmRequestStatus, NVM_BLK_EMPTY);
  assert_int_equal(nvmRequestList[0].nvmRequestType, NVM_REQ_ERASE);
  assert_int_equal(nvmRequestList[0].nvmSafe, 0x00);

  for (idx = 0; idx < NVM_MAX_REQUEST_DATA_SIZE; idx++)
  {
    assert_int_equal(nvmRequestList[0].nvmData[idx], 0x00);
  }

  /* After */
  assert_int_equal(nvmRequestList[2].nvmBlockId, 0xFFFF);
  assert_int_equal(nvmRequestList[2].requestDataPointer, NULL_PTR);
  assert_int_equal(nvmRequestList[2].nvmRequestStatus, NVM_BLK_EMPTY);
  assert_int_equal(nvmRequestList[2].nvmRequestType, NVM_REQ_ERASE);
  assert_int_equal(nvmRequestList[2].nvmSafe, 0x00);

  for (idx = 0; idx < NVM_MAX_REQUEST_DATA_SIZE; idx++)
  {
    assert_int_equal(nvmRequestList[1].nvmData[idx], 0x00);
  }

  /* Check status list elements before >> requested >> after */
  assert_int_equal(nvmBlockStatusList[0], NVM_BLK_OK);
  assert_int_equal(nvmBlockStatusList[1], NVM_BLK_OK);
  assert_int_equal(nvmBlockStatusList[2], NVM_BLK_OK);
  assert_int_equal(nvmProcessNewReq_LastIdx, 0x01);

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Cleanup --------------------------------*/
  /*----------------------------------------------------------------------------*/
  (void)state; /* unused */
}

static void UTest_Nvm_Int_ProcessingNewRequests_104(void **state)
{
  uint8 listIdx = 0;
  uint16 idx = 0;
  Nvm_ReturnType fncRetVal;
  uint16 testBlkId;
  Nvm_BlockStatusType testBlkStatus, testBlkStatus2;
  uint8 testDataWriteBlk[12] = { 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0x00, 0x00 };
  uint8 testDataReadBlk[12] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

  /*----------------------------------------------------------------------------*/
  /*------------------------------- Test Setup ---------------------------------*/
  /*----------------------------------------------------------------------------*/
  printf("[ DESCRIP  ] >> Check the processing active requests functionality of the Nvm sub module \"Int\"<< \r\n");
  printf("[          ] >> Check functionality if element is waiting >> Waiting Element Request NVM_REQ_ERASE \r\n");
  printf("[          ] >> Lower module request return value E_NOT_OK \r\n");

  Nvm_Int_Init();
  Nvm_RequestList_Init();

  nvmRequestList[1].nvmBlockId = NVM_BLK_ID_DEMO_BLK;
  nvmRequestList[1].nvmRequestStatus = NVM_BLK_WAITING;
  nvmRequestList[1].nvmRequestType = NVM_REQ_ERASE;
  nvmRequestList[1].requestDataPointer = NULL_PTR;

  /* Expect Write Data */
  expect_function_call(FlsEep_RequestWriteData);
  will_return(FlsEep_RequestWriteData, FLSEEP_BLK_DEMOBLK);
  will_return(FlsEep_RequestWriteData, &nvmRequestList[1].nvmData[0]);
  will_return(FlsEep_RequestWriteData, E_NOT_OK);

  expect_function_call(Det_ReportError);

  expect_function_call(Nvm_RequestList_HlpInvalidateListElement);
  will_return(Nvm_RequestList_HlpInvalidateListElement, 1);

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Execution ------------------------------*/
  /*----------------------------------------------------------------------------*/
  Nvm_Int_ProcessNewRequests();

  /* Check modified request list element */
  assert_int_equal(nvmRequestList[1].nvmBlockId, 0xFFFF);
  assert_int_equal(nvmRequestList[1].requestDataPointer, NULL_PTR);
  assert_int_equal(nvmRequestList[1].nvmRequestStatus, NVM_BLK_EMPTY);
  assert_int_equal(nvmRequestList[1].nvmRequestType, NVM_REQ_ERASE);
  assert_int_equal(nvmRequestList[1].nvmSafe, 0x00);

  for (idx = 0; idx < NVM_MAX_REQUEST_DATA_SIZE; idx++)
  {
    assert_int_equal(nvmRequestList[1].nvmData[idx], 0x00);
  }

  /* Check list element before and after the request list element */
  /* Before */
  assert_int_equal(nvmRequestList[0].nvmBlockId, 0xFFFF);
  assert_int_equal(nvmRequestList[0].requestDataPointer, NULL_PTR);
  assert_int_equal(nvmRequestList[0].nvmRequestStatus, NVM_BLK_EMPTY);
  assert_int_equal(nvmRequestList[0].nvmRequestType, NVM_REQ_ERASE);
  assert_int_equal(nvmRequestList[0].nvmSafe, 0x00);

  for (idx = 0; idx < NVM_MAX_REQUEST_DATA_SIZE; idx++)
  {
    assert_int_equal(nvmRequestList[0].nvmData[idx], 0x00);
  }

  /* After */
  assert_int_equal(nvmRequestList[2].nvmBlockId, 0xFFFF);
  assert_int_equal(nvmRequestList[2].requestDataPointer, NULL_PTR);
  assert_int_equal(nvmRequestList[2].nvmRequestStatus, NVM_BLK_EMPTY);
  assert_int_equal(nvmRequestList[2].nvmRequestType, NVM_REQ_ERASE);
  assert_int_equal(nvmRequestList[2].nvmSafe, 0x00);

  for (idx = 0; idx < NVM_MAX_REQUEST_DATA_SIZE; idx++)
  {
    assert_int_equal(nvmRequestList[1].nvmData[idx], 0x00);
  }

  /* Check status list elements before >> requested >> after */
  assert_int_equal(nvmBlockStatusList[0], NVM_BLK_SKIPPED);
  assert_int_equal(nvmBlockStatusList[1], NVM_BLK_OK);
  assert_int_equal(nvmBlockStatusList[2], NVM_BLK_OK);
  assert_int_equal(nvmProcessNewReq_LastIdx, 0x00);

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Cleanup --------------------------------*/
  /*----------------------------------------------------------------------------*/
  (void)state; /* unused */
}

static void UTest_Nvm_Int_ProcessingNewRequests_105(void **state)
{
  uint8 listIdx = 0;
  uint16 idx = 0;
  Nvm_ReturnType fncRetVal;
  uint16 testBlkId;
  Nvm_BlockStatusType testBlkStatus, testBlkStatus2;
  uint8 testDataWriteBlk[12] = { 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0x00, 0x00 };
  uint8 testDataReadBlk[12] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

  /*----------------------------------------------------------------------------*/
  /*------------------------------- Test Setup ---------------------------------*/
  /*----------------------------------------------------------------------------*/
  printf("[ DESCRIP  ] >> Check the processing active requests functionality of the Nvm sub module \"Int\"<< \r\n");
  printf("[          ] >> Check functionality if element is waiting >> Waiting Element Request NVM_REQ_WRITE \r\n");
  printf("[          ] >> Lower module request return value E_OK \r\n");

  Nvm_Int_Init();
  Nvm_RequestList_Init();

  nvmRequestList[1].nvmBlockId = NVM_BLK_ID_DEMO_BLK;
  nvmRequestList[1].nvmRequestStatus = NVM_BLK_WAITING;
  nvmRequestList[1].nvmRequestType = NVM_REQ_WRITE;
  nvmRequestList[1].requestDataPointer = NULL_PTR;

  /* Expect Write Data */
  expect_function_call(FlsEep_RequestWriteData);
  will_return(FlsEep_RequestWriteData, FLSEEP_BLK_DEMOBLK);
  will_return(FlsEep_RequestWriteData, &nvmRequestList[1].nvmData[0]);
  will_return(FlsEep_RequestWriteData, E_OK);


  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Execution ------------------------------*/
  /*----------------------------------------------------------------------------*/
  Nvm_Int_ProcessNewRequests();

  /* Check modified request list element */
  assert_int_equal(nvmRequestList[1].nvmBlockId, NVM_BLK_ID_DEMO_BLK);
  assert_int_equal(nvmRequestList[1].requestDataPointer, NULL_PTR);
  assert_int_equal(nvmRequestList[1].nvmRequestStatus, NVM_BLK_PENDING);
  assert_int_equal(nvmRequestList[1].nvmRequestType, NVM_REQ_WRITE);
  assert_int_equal(nvmRequestList[1].nvmSafe, 0x00);

  for (idx = 0; idx < NVM_MAX_REQUEST_DATA_SIZE; idx++)
  {
    assert_int_equal(nvmRequestList[1].nvmData[idx], 0x00);
  }

  /* Check list element before and after the request list element */
  /* Before */
  assert_int_equal(nvmRequestList[0].nvmBlockId, 0xFFFF);
  assert_int_equal(nvmRequestList[0].requestDataPointer, NULL_PTR);
  assert_int_equal(nvmRequestList[0].nvmRequestStatus, NVM_BLK_EMPTY);
  assert_int_equal(nvmRequestList[0].nvmRequestType, NVM_REQ_ERASE);
  assert_int_equal(nvmRequestList[0].nvmSafe, 0x00);

  for (idx = 0; idx < NVM_MAX_REQUEST_DATA_SIZE; idx++)
  {
    assert_int_equal(nvmRequestList[0].nvmData[idx], 0x00);
  }

  /* After */
  assert_int_equal(nvmRequestList[2].nvmBlockId, 0xFFFF);
  assert_int_equal(nvmRequestList[2].requestDataPointer, NULL_PTR);
  assert_int_equal(nvmRequestList[2].nvmRequestStatus, NVM_BLK_EMPTY);
  assert_int_equal(nvmRequestList[2].nvmRequestType, NVM_REQ_ERASE);
  assert_int_equal(nvmRequestList[2].nvmSafe, 0x00);

  for (idx = 0; idx < NVM_MAX_REQUEST_DATA_SIZE; idx++)
  {
    assert_int_equal(nvmRequestList[1].nvmData[idx], 0x00);
  }

  /* Check status list elements before >> requested >> after */
  assert_int_equal(nvmBlockStatusList[0], NVM_BLK_OK);
  assert_int_equal(nvmBlockStatusList[1], NVM_BLK_OK);
  assert_int_equal(nvmBlockStatusList[2], NVM_BLK_OK);
  assert_int_equal(nvmProcessNewReq_LastIdx, 0x00);

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Cleanup --------------------------------*/
  /*----------------------------------------------------------------------------*/
  (void)state; /* unused */
}

static void UTest_Nvm_Int_ProcessingNewRequests_106(void **state)
{
  uint8 listIdx = 0;
  uint16 idx = 0;
  Nvm_ReturnType fncRetVal;
  uint16 testBlkId;
  Nvm_BlockStatusType testBlkStatus, testBlkStatus2;
  uint8 testDataWriteBlk[12] = { 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0x00, 0x00 };
  uint8 testDataReadBlk[12] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

  /*----------------------------------------------------------------------------*/
  /*------------------------------- Test Setup ---------------------------------*/
  /*----------------------------------------------------------------------------*/
  printf("[ DESCRIP  ] >> Check the processing active requests functionality of the Nvm sub module \"Int\"<< \r\n");
  printf("[          ] >> Check functionality if element is waiting >> Waiting Element Request NVM_REQ_READ \r\n");
  printf("[          ] >> Lower module request return value E_OK \r\n");

  Nvm_Int_Init();
  Nvm_RequestList_Init();

  nvmRequestList[1].nvmBlockId = NVM_BLK_ID_DEMO_BLK;
  nvmRequestList[1].nvmRequestStatus = NVM_BLK_WAITING;
  nvmRequestList[1].nvmRequestType = NVM_REQ_READ;
  nvmRequestList[1].requestDataPointer = &testDataReadBlk[0];

  /* Expect Write Data */
  expect_function_call(FlsEep_RequestReadData);
  will_return(FlsEep_RequestReadData, FLSEEP_BLK_DEMOBLK);
  will_return(FlsEep_RequestReadData, &nvmRequestList[1].nvmData[0]);
  will_return(FlsEep_RequestReadData, E_OK);


  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Execution ------------------------------*/
  /*----------------------------------------------------------------------------*/
  Nvm_Int_ProcessNewRequests();

  /* Check modified request list element */
  assert_int_equal(nvmRequestList[1].nvmBlockId, NVM_BLK_ID_DEMO_BLK);
  assert_int_equal(nvmRequestList[1].requestDataPointer, &testDataReadBlk[0]);
  assert_int_equal(nvmRequestList[1].nvmRequestStatus, NVM_BLK_PENDING);
  assert_int_equal(nvmRequestList[1].nvmRequestType, NVM_REQ_READ);
  assert_int_equal(nvmRequestList[1].nvmSafe, 0x00);

  for (idx = 0; idx < NVM_MAX_REQUEST_DATA_SIZE; idx++)
  {
    assert_int_equal(nvmRequestList[1].nvmData[idx], 0x00);
  }

  /* Check list element before and after the request list element */
  /* Before */
  assert_int_equal(nvmRequestList[0].nvmBlockId, 0xFFFF);
  assert_int_equal(nvmRequestList[0].requestDataPointer, NULL_PTR);
  assert_int_equal(nvmRequestList[0].nvmRequestStatus, NVM_BLK_EMPTY);
  assert_int_equal(nvmRequestList[0].nvmRequestType, NVM_REQ_ERASE);
  assert_int_equal(nvmRequestList[0].nvmSafe, 0x00);

  for (idx = 0; idx < NVM_MAX_REQUEST_DATA_SIZE; idx++)
  {
    assert_int_equal(nvmRequestList[0].nvmData[idx], 0x00);
  }

  /* After */
  assert_int_equal(nvmRequestList[2].nvmBlockId, 0xFFFF);
  assert_int_equal(nvmRequestList[2].requestDataPointer, NULL_PTR);
  assert_int_equal(nvmRequestList[2].nvmRequestStatus, NVM_BLK_EMPTY);
  assert_int_equal(nvmRequestList[2].nvmRequestType, NVM_REQ_ERASE);
  assert_int_equal(nvmRequestList[2].nvmSafe, 0x00);

  for (idx = 0; idx < NVM_MAX_REQUEST_DATA_SIZE; idx++)
  {
    assert_int_equal(nvmRequestList[1].nvmData[idx], 0x00);
  }

  /* Check status list elements before >> requested >> after */
  assert_int_equal(nvmBlockStatusList[0], NVM_BLK_OK);
  assert_int_equal(nvmBlockStatusList[1], NVM_BLK_OK);
  assert_int_equal(nvmBlockStatusList[2], NVM_BLK_OK);
  assert_int_equal(nvmProcessNewReq_LastIdx, 0x00);

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Cleanup --------------------------------*/
  /*----------------------------------------------------------------------------*/
  (void)state; /* unused */
}



/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>         End of private function implementation               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>          Start of public function implementation             << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#define CNT_OF_TEST_GROUPS 2
int main()
{
  int result[CNT_OF_TEST_GROUPS], idx = 0;
  /* Setup Console for Test Output */
  printf("Startup of Module Tests for Module xyz. \r\nTest Framework: CMocka 1.1.1 \r\n");

  /*#################################### Run CMocka group tests #############################################*/
  result[0] = cmocka_run_group_tests(tests, NULL, NULL);
  result[1] = cmocka_run_group_tests(testsProcessing, NULL, NULL);

  /* Print result */
  printf("\r\n\r\n=====================================================================================\r\n");
  printf("Test summary:\r\n\r\n");
  for (idx = 0; idx < CNT_OF_TEST_GROUPS; idx++)
  {
    printf("Testgroup %d >> Cnt of errors: %d\r\n", idx, result[idx]);
  }
  printf("=====================================================================================\r\n\r\n");

  /* wait for user key to shutdown system */
  printf("Press any key to exit test environment \r\n");
  
  getch();
    return 0;
}
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>          End of public function implementation               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

