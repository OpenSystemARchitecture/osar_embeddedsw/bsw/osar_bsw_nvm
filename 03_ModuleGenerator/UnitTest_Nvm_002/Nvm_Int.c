/*****************************************************************************************************************************
 * @file        Nvm_Int.c                                                                                                    *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        25.08.2018 15:58:25                                                                                          *
 * @brief       Implementation of functionalities from the "Nvm" module.                                                     *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
 * @libNote     OSAR Source File Library v.1.0.0.0                                                                           *
*****************************************************************************************************************************/
/**
 * @addtogroup Nvm
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 Start of include area                        << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#include "Det.h"
#include "Nvm_Int.h"
#include "Nvm_RequestList.h"
#include "Crc.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                  End of include area                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of private data types                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of private data types                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            Start of private definitions area                 << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of private definitions area                  << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of global variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/*------------------------------------------- Map variables into constand memory -------------------------------------------*/
#define Nvm_START_SEC_CONST
#include "Nvm_MemMap.h"
#define Nvm_STOP_SEC_CONST
#include "Nvm_MemMap.h"
/*---------------------------------------- Map variables into uninitialized memory ----------------------------------------*/
#define Nvm_START_SEC_NOINIT_VAR
#include "Nvm_MemMap.h"
#define Nvm_STOP_SEC_NOINIT_VAR
#include "Nvm_MemMap.h"
/*----------------------------------------- Map variables into initialized memory -----------------------------------------*/
#define Nvm_START_SEC_INIT_VAR
#include "Nvm_MemMap.h"
#define Nvm_STOP_SEC_INIT_VAR
#include "Nvm_MemMap.h"
/*--------------------------------------- Map variables into zero initialized memory ---------------------------------------*/
#define Nvm_START_SEC_ZERO_INIT_VAR
#include "Nvm_MemMap.h"
#define Nvm_STOP_SEC_ZERO_INIT_VAR
#include "Nvm_MemMap.h"
/*-------------------------------------------------- External defined variables --------------------------------------------*/
extern Nvm_RequestListEntryType nvmRequestList[];
extern const Nvm_BlockCfgType nvmBlkCfgList[];
extern const Nvm_BaseModeCfgList nvmBaseModuleList[];
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of global variables                      << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of private variables                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/*------------------------------------------- Map variables into constand memory -------------------------------------------*/
#define Nvm_START_SEC_CONST
#include "Nvm_MemMap.h"
#define Nvm_STOP_SEC_CONST
#include "Nvm_MemMap.h"
/*---------------------------------------- Map variables into uninitialized memory ----------------------------------------*/
#define Nvm_START_SEC_NOINIT_VAR
#include "Nvm_MemMap.h"
Nvm_BlockStatusType nvmBlockStatusList[NVM_CNT_CONFIGURED_BLOCKS];
uint8 nvmProcessNewReq_LastIdx;
#define Nvm_STOP_SEC_NOINIT_VAR
#include "Nvm_MemMap.h"
/*----------------------------------------- Map variables into initialized memory -----------------------------------------*/
#define Nvm_START_SEC_INIT_VAR
#include "Nvm_MemMap.h"
#define Nvm_STOP_SEC_INIT_VAR
#include "Nvm_MemMap.h"
/*--------------------------------------- Map variables into zero initialized memory ---------------------------------------*/
#define Nvm_START_SEC_ZERO_INIT_VAR
#include "Nvm_MemMap.h"
#define Nvm_STOP_SEC_ZERO_INIT_VAR
#include "Nvm_MemMap.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of private variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>           Start of private function prototypes               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            End of private function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>        Start of private function implementation              << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/*--------------------------------------------- Map function into code memory ---------------------------------------------*/
#define Nvm_START_SEC_CODE
#include "Nvm_MemMap.h"
#define Nvm_STOP_SEC_CODE
#include "Nvm_MemMap.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>         End of private function implementation               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>          Start of public function implementation             << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/*------------------------------------------------------------  ------------------------------------------------------------*/
#define Nvm_START_SEC_CODE
#include "Nvm_MemMap.h"

/**
 * @brief           Module global initialization function.
 * @param[in]       None
 * @retval          None
 * @details         This function shall be called before using the module and after the memory initialization.
 */
void Nvm_Int_Init( void )
{
  uint16 idx = 0;
  /* Initalize the user nvm block status */
  for(idx = 0; idx < NVM_CNT_CONFIGURED_BLOCKS; idx++)
  { 
    nvmBlockStatusList[idx] = NVM_BLK_OK;
  }
  
  /* Initalie processing new Request index */
  nvmProcessNewReq_LastIdx = 0;


}

/**
* @brief           Function to modify the interface block status
* @param[in]       uint16 id of the corresponding nvm block
* @param[in]       Nvm_BlockStatusType new status the corresponding nvm block
*                  > NVM_BLK_OK                  >> Nvm block is valid
*                  > NVM_BLK_PENDING             >> Nvm block is ia current in progress
*                  > NVM_BLK_SKIPPED             >> Nvm block processing has been skipped
*                  > NVM_BLK_INTEGRITY_FAILED    >> Nvm block data is invalid
*                  > NVM_BLK_INVALIDATE          >> Nvm block not found in memory
* @retval          Nvm_ReturnType
*                  > NVM_E_OK
*                  > NVM_E_NOT_OK
*/
Nvm_ReturnType Nvm_Int_SetNvmBlockStatus(uint16 nvmBlockId, Nvm_BlockStatusType nvmBlockStatus)
{
  Nvm_ReturnType retVal = NVM_E_NOT_OK;
  /* Check input parameter */
  if (nvmBlockId >= NVM_CNT_CONFIGURED_BLOCKS)
  {
    #if(STD_ON == NVM_MODULE_USE_DET)
    Det_ReportError(NVM_DET_MODULE_ID, NVM_E_INVALID_ARGUMENT);
    #endif
    retVal = NVM_E_NOT_OK;
  }
  else
  {
    nvmBlockStatusList[nvmBlockId] = nvmBlockStatus;
    retVal = NVM_E_OK;
  }

  return retVal;
}

/**
* @brief           Function to get the interface block status
* @param[in]       uint16 id of the corresponding nvm block
* @param[out]      Nvm_BlockStatusType* ponter to store the corresponding nvm block status
*                  > NVM_BLK_OK                  >> Nvm block is valid
*                  > NVM_BLK_PENDING             >> Nvm block is ia current in progress
*                  > NVM_BLK_SKIPPED             >> Nvm block processing has been skipped
*                  > NVM_BLK_INTEGRITY_FAILED    >> Nvm block data is invalid
*                  > NVM_BLK_INVALIDATE          >> Nvm block not found in memory
* @retval          Nvm_ReturnType
*                  > NVM_E_OK
*                  > NVM_E_NOT_OK
*/
Nvm_ReturnType Nvm_Int_GetNvmBlockStatus(uint16 nvmBlockId, Nvm_BlockStatusType* nvmBlockStatus)
{
  Nvm_ReturnType retVal = NVM_E_NOT_OK;
  /* Check input parameter */
  if (nvmBlockId >= NVM_CNT_CONFIGURED_BLOCKS)
  {
    #if(STD_ON == NVM_MODULE_USE_DET)
    Det_ReportError(NVM_DET_MODULE_ID, NVM_E_INVALID_ARGUMENT);
    #endif
    retVal = NVM_E_NOT_OK;
  }
  else if (NULL_PTR == nvmBlockStatus)
  {
    #if(STD_ON == NVM_MODULE_USE_DET)
    Det_ReportError(NVM_DET_MODULE_ID, NVM_E_INVALID_ARGUMENT);
    #endif
    retVal = NVM_E_NOT_OK;
  }
  else
  {
    *nvmBlockStatus = nvmBlockStatusList[nvmBlockId];
    retVal = NVM_E_OK;
  }

  return retVal;
}

/**
* @brief           Function to process active / peding data element in request buffer
* @param[in]       None
* @retval          None
* @details         Performed actions:
*                  > Process complete request buffer
*                  >> Check pending request status in lower interfac module
*                  >>> Read requests >> Evaluate data
*                  >>> Write requests >> OK
*                  >> Update global status buffer
*                  >> Invalidate buffer element
*/
void Nvm_Int_ProcessActiveRequests(void)
{
  uint8 reqBufIdx, subModuleId;
  uint16 subModuleBlkId, dataIdx;
  Data_StatusType subModuleDataStatus;
  uint32 calcCrc = 0, *pReadCrc;
  Crc_ReturnType crcRetVal;
  boolean crcEqual = FALSE;

  for (reqBufIdx = 0; reqBufIdx < NVM_REQUEST_LIST_SIZE; reqBufIdx++)
  {
    if (NVM_BLK_PENDING == nvmRequestList[reqBufIdx].nvmRequestStatus)
    {
      subModuleId = nvmBlkCfgList[nvmRequestList[reqBufIdx].nvmBlockId].nvmBlockUsedBaseModule;
      subModuleBlkId = nvmBlkCfgList[nvmRequestList[reqBufIdx].nvmBlockId].nvmBaseModuleBlkId;
      subModuleDataStatus = nvmBaseModuleList[subModuleId].Nvm_BaseModule_RequestBlockStatus(subModuleBlkId);

      switch (subModuleDataStatus)
      {
      case DATA_E_OK:
        // Data processed >> Perform actions
      {
        switch (nvmRequestList[reqBufIdx].nvmRequestType)
        {
        case NVM_REQ_READ:
        {
          /* Calculate CRC if requested  */
          switch (nvmBlkCfgList[nvmRequestList[reqBufIdx].nvmBlockId].nvmBlockUseCrc)
          {
          case NVM_CRC_NONE:
            crcRetVal = CRC_E_OK;
            crcEqual = TRUE;
            break;

          case NVM_CRC_CRC32:
          {
            //DEBUG: Test this implementation >> Possition of CRC value
            crcRetVal = Crc32_0x04C11DB7_Compute(&nvmRequestList[reqBufIdx].nvmData[0], nvmBlkCfgList[nvmRequestList[reqBufIdx].nvmBlockId].nvmBlockDataLength, &calcCrc);
            pReadCrc = (uint32*)&nvmRequestList[reqBufIdx].nvmData[nvmBlkCfgList[nvmRequestList[reqBufIdx].nvmBlockId].nvmBlockDataLength];

            if (*pReadCrc == calcCrc)
            {
              crcEqual = TRUE;
            }
            else
            {
              crcEqual = FALSE;
            }
          }
            break;
            //TODO: Add more Crc algroorithems i future >> CRC 16

          default:
            #if(STD_ON == NVM_MODULE_USE_DET)
            Det_ReportError(NVM_DET_MODULE_ID, NVM_E_INVALID_CONFIGURATION);
            #endif
            crcRetVal = CRC_E_OK;
            crcEqual = TRUE;
            break;
          }

          /* Check if CRC is valid */
          if ((TRUE == crcEqual) && (CRC_E_OK == crcRetVal) && (0 == nvmRequestList[reqBufIdx].nvmSafe))
          {
            /* Copy read data to user data buffer */
            for (dataIdx = 0; dataIdx < nvmBlkCfgList[nvmRequestList[reqBufIdx].nvmBlockId].nvmBlockDataLength; dataIdx++)
            {
              nvmRequestList[reqBufIdx].requestDataPointer[dataIdx] = nvmRequestList[reqBufIdx].nvmData[dataIdx];
            }

            /* Data ok >> Update global status*/
            Nvm_Int_SetNvmBlockStatus(nvmRequestList[reqBufIdx].nvmBlockId, NVM_BLK_OK);
            Nvm_RequestList_HlpInvalidateListElement(reqBufIdx);
          }
          else
          {
            /* Data not ok >> Update global status*/
            Nvm_Int_SetNvmBlockStatus(nvmRequestList[reqBufIdx].nvmBlockId, NVM_BLK_INTEGRITY_FAILED);
            Nvm_RequestList_HlpInvalidateListElement(reqBufIdx);
          }

        }
          break;

        case NVM_REQ_WRITE:
          /* Data written >> Update global status*/
          Nvm_Int_SetNvmBlockStatus(nvmRequestList[reqBufIdx].nvmBlockId, NVM_BLK_OK);
          Nvm_RequestList_HlpInvalidateListElement(reqBufIdx);
          break;

        case NVM_REQ_ERASE:
          /* Data erased >> Update global status*/
          Nvm_Int_SetNvmBlockStatus(nvmRequestList[reqBufIdx].nvmBlockId, NVM_BLK_OK);
          Nvm_RequestList_HlpInvalidateListElement(reqBufIdx);
          break;

        default:
          #if(STD_ON == NVM_MODULE_USE_DET)
          Det_ReportError(NVM_DET_MODULE_ID, NVM_E_GENERIC_PROGRAMMING_FAILURE);
          #endif
          break;
        }
      }
        break;

      case DATA_E_PENDING:
        /* Data is pending >> Do nothing */
        break;

      case DATA_E_SKIPPED:
        /* Data is skipped >> Set back to waiting in Request Buffer */
        nvmRequestList[reqBufIdx].nvmRequestStatus = NVM_BLK_WAITING;
        break;

      case DATA_E_INTEGRITY_FAILED:
        /* Data invalid >> Cleanup List Element and set global status */
        Nvm_Int_SetNvmBlockStatus(nvmRequestList[reqBufIdx].nvmBlockId, NVM_BLK_INTEGRITY_FAILED);
        Nvm_RequestList_HlpInvalidateListElement(reqBufIdx);
        break;

      case DATA_E_INVALIDATE:
        /* Data invalid >> Cleanup List Element and set global status */
        Nvm_Int_SetNvmBlockStatus(nvmRequestList[reqBufIdx].nvmBlockId, NVM_BLK_INVALIDATE);
        Nvm_RequestList_HlpInvalidateListElement(reqBufIdx);
        break;

      default:
        #if(STD_ON == NVM_MODULE_USE_DET)
        Det_ReportError(NVM_DET_MODULE_ID, NVM_E_GENERIC_PROGRAMMING_FAILURE);
        #endif
        break;
      }
    }
  }
}

/**
* @brief           Function to process new data element in request buffer
* @param[in]       None
* @retval          None
* @details         Performed actions:
*                  > Process request buffer till lower if module buffer is full
*                  >> Set new request elements to lower if module
*                  >> Update global status buffer
*/
void Nvm_Int_ProcessNewRequests(void)
{
  uint8 idx = 0, subModuleId;
  uint16 subModuleBlkId;
  Std_ReturnType stdRetVal;
  Data_StatusType subModuleDataStatus;

  /* Processing all request if possible */
  for (idx = 0; idx < NVM_REQUEST_LIST_SIZE; idx++)
  {
    if (NVM_BLK_WAITING == nvmRequestList[nvmProcessNewReq_LastIdx].nvmRequestStatus)
    {
      subModuleId = nvmBlkCfgList[nvmRequestList[nvmProcessNewReq_LastIdx].nvmBlockId].nvmBlockUsedBaseModule;
      subModuleBlkId = nvmBlkCfgList[nvmRequestList[nvmProcessNewReq_LastIdx].nvmBlockId].nvmBaseModuleBlkId;

      /* Switch request types */
      switch (nvmRequestList[nvmProcessNewReq_LastIdx].nvmRequestType)
      {
      case NVM_REQ_READ:
        //NOTE: Evaluation of returntype neccessaray? 
        stdRetVal = nvmBaseModuleList[subModuleId].Nvm_BaseModule_RequestReadData(subModuleBlkId, nvmRequestList[nvmProcessNewReq_LastIdx].nvmData);
        break;

      case NVM_REQ_WRITE:
        //NOTE: Evaluation of returntype neccessaray? 
        stdRetVal = nvmBaseModuleList[subModuleId].Nvm_BaseModule_RequestWriteData(subModuleBlkId, nvmRequestList[nvmProcessNewReq_LastIdx].nvmData);
        break;

      case NVM_REQ_ERASE:
        //NOTE: Evaluation of returntype neccessaray? 
        stdRetVal = nvmBaseModuleList[subModuleId].Nvm_BaseModule_RequestWriteData(subModuleBlkId, nvmRequestList[nvmProcessNewReq_LastIdx].nvmData);
        break;

      default:
        #if(STD_ON == NVM_MODULE_USE_DET)
        Det_ReportError(NVM_DET_MODULE_ID, NVM_E_GENERIC_PROGRAMMING_FAILURE);
        #endif
        break;
      }
    }

    /* Check Return value */
    if (E_OK == stdRetVal)
    {
      nvmRequestList[nvmProcessNewReq_LastIdx].nvmRequestStatus = NVM_E_PENDING;
      
      /* Increment processing index */
      nvmProcessNewReq_LastIdx++;
      if (nvmProcessNewReq_LastIdx >= NVM_REQUEST_LIST_SIZE)
      {
        nvmProcessNewReq_LastIdx = 0;
      }
    }
    else
    {
      /* Check if block has been skipped */
      subModuleDataStatus = nvmBaseModuleList[subModuleId].Nvm_BaseModule_RequestBlockStatus(subModuleBlkId);
      if (DATA_E_SKIPPED == subModuleDataStatus)
      {
        /* DATA Procesisng skippt continue next cycle */
        break;
      }
      else
      {
        /* Generic Error >> Report Error */
        #if(STD_ON == NVM_MODULE_USE_DET)
        Det_ReportError(NVM_DET_MODULE_ID, NVM_E_GENERIC_PROGRAMMING_FAILURE);
        #endif
        /* Data invalid >> Cleanup List Element and set global status */
        Nvm_Int_SetNvmBlockStatus(nvmRequestList[nvmProcessNewReq_LastIdx].nvmBlockId, NVM_E_SKIPPED);
        Nvm_RequestList_HlpInvalidateListElement(nvmProcessNewReq_LastIdx);
      }
    }


  }
}

#define Nvm_STOP_SEC_CODE
#include "Nvm_MemMap.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>          End of public function implementation               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/**
 * @}
 */

