/*****************************************************************************************************************************
 * @file        Nvm.h                                                                                                        *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        25.08.2018 15:58:25                                                                                          *
 * @brief       Implementation of definitions / interface function prototypes / datatypes and generic module interface       *
 *              informations of the "Nvm" module.                                                                            *
 *                                                                                                                           *
 * @details     Implementation of an non volatile memory handling. The module provides an simple interface to create and     *
 *              access non volatile memory blocks without handling the non volatile memory sequence. Additional it           *
 *              implements some safety features like an block CRC to detect invalid data.                                    *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
 * @libNote     OSAR Source File Library v.1.0.0.0                                                                           *
*****************************************************************************************************************************/
#ifndef __NVM_H
#define __NVM_H
/**
 * @defgroup Nvm TODO >> Add module description for doxygen
 */

/**
 * @addtogroup Nvm
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 Start of include area                        << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#include "Nvm_Types.h"
#include "Nvm_PBCfg.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                  End of include area                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of public data types                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of public data types                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            Start of public definition area                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of public definition area                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of global variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of global variables                      << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>           Start of public function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/**
 * @brief           Module global memory initialization function.
 * @param[in]       None
 * @retval          None
 * @details         This function shall be called during system startup. And before calling the init and mainfuntion.
 */
void Nvm_InitMemory( void );

/**
 * @brief           Module global initialization function.
 * @param[in]       None
 * @retval          None
 * @details         This function shall be called before using the module and after the memory initialization.
 */
void Nvm_Init( void );

/**
 * @brief           Module global mainfunction.
 * @param[in]       None
 * @retval          None
 * @details         This function shall be called form the actual runtime environment within an fixed cycle.
 */
void Nvm_Mainfunction( void );

/**
 * @brief           API to request the error / current status of an specific Nvm block
 * @param[in]       uint16 nvm block id
 * @param[out]      Nvm_BlockStatusType* current nvm block status
 *                  > NVM_BLK_OK                  >> Nvm block is valid
 *                  > NVM_BLK_PENDING             >> Nvm block is ia current in progress
 *                  > NVM_BLK_SKIPPED             >> Nvm block processing has been skipped
 *                  > NVM_BLK_INTEGRITY_FAILED    >> Nvm block data is invalid
 *                  > NVM_BLK_INVALIDATE          >> Nvm block not found in memory
 * @retval          Std_ReturnType
 *                  > E_OK                        >> Request accepted
 *                  > E_NOT_OK                    >> Request not accepted
 */
Std_ReturnType Nvm_GetErrorStatus( uint16 nvmBlockId, Nvm_BlockStatusType* nvmBlockStatus );

/**
 * @brief           API to request an nvm block write
 * @param[in]       uint16 nvm block id
 * @param[in]       uint8* pointer to data which shall be written
 * @retval          Std_ReturnType
 *                  > E_OK                        >> Request accepted
 *                  > E_NOT_OK                    >> Request failed
 *                  > E_SKIPPED                   >> Request not accepted
 * @notes           The status of the active block must be additional requested.
 */
Std_ReturnType Nvm_WriteBlock( uint16 nvmBlockId, uint8* pNvmWriteData );

/**
 * @brief           API to request an nvm block read
 * @param[in]       uint16 nvm block id
 * @param[in]       uint8* pointer to data where the read data could be stored
 * @retval          Std_ReturnType
 *                  > E_OK                        >> Request accepted
 *                  > E_NOT_OK                    >> Request failed
 *                  > E_SKIPPED                   >> Request not accepted
 * @notes           The status of the active block must be additional requested.
 */
Std_ReturnType Nvm_ReadBlock( uint16 nvmBlockId, uint8* pNvmReadData );

/**
 * @brief           API to request an nvm block erase
 * @param[in]       uint16 nvm block id
 * @retval          Std_ReturnType
 *                  > E_OK                        >> Request accepted
 *                  > E_NOT_OK                    >> Request failed
 *                  > E_SKIPPED                   >> Request not accepted
 * @note            Nvm block data would be filled with 0x00. The status of the active block must be additional requested.
 */
Std_ReturnType Nvm_EraseNvBlock( uint16 nvmBlockId );

/**
 * @brief           API to read all nvm blocks from nvm memory into ram
 * @param[in]       None
 * @retval          Std_ReturnType
 *                  > E_OK                        >> Request accepted
 *                  > E_NOT_OK                    >> Request not accepted
 *                  > E_PENDING                   >> Request pending
 * @note            Only selected nvm blocks. This function must be called as long the function reports "E_PENDING".
 *                  Also the equal base module must be called to process the selected Nvm blocks.
 */
Std_ReturnType Nvm_ReadAll( void );

/**
 * @brief           API to write all nvm blocks to nvm memory from ram
 * @param[in]       None
 * @retval          Std_ReturnType
 *                  > E_OK                        >> Request accepted
 *                  > E_NOT_OK                    >> Request not accepted
 *                  > E_PENDING                   >> Request pending
 * @note            Only selected nvm blocks. This function must be called as long the function reports "E_PENDING".
 *                  Also the equal base module must be called to process the selected Nvm blocks.
 */
Std_ReturnType Nvm_WriteAll( void );
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of public function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/**
 * @}
 */

#endif /* __NVM_H*/
