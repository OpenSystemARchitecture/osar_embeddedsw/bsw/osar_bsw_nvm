/*****************************************************************************************************************************
 * @file        Nvm_Types.h                                                                                                  *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        25.08.2018 15:58:25                                                                                          *
 * @brief       Implementation of module global datatypes from the "Nvm" module.                                             *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
 * @libNote     OSAR Source File Library v.1.0.0.0                                                                           *
*****************************************************************************************************************************/
#ifndef __NVM_TYPES_H
#define __NVM_TYPES_H
/**
 * @addtogroup Nvm
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 Start of include area                        << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#include "Std_Types.h"
#include "Nvm_PBCfg.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                  End of include area                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of public data types                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/**
 * @brief           Available application errors of the Nvm
 * @details         This enumeration implements all available application return and error values within this specific 
 *                  module. The Application error could be used as DET information or as return type of internal functions. 
 */
typedef enum{
  NVM_E_OK = 0,
  NVM_E_NOT_OK = 1,
  NVM_E_PENDING = 2,
  NVM_E_NOT_IMPLEMENTED = 3,
  NVM_E_GENERIC_PROGRAMMING_FAILURE = 4,
  NVM_E_SKIPPED = 5,
  NVM_E_INVALID_ARGUMENT = 100,
  NVM_E_NULL_POINTER = 101,
  NVM_E_INVALID_CONFIGURATION = 102
}Nvm_ReturnType;

/**
 * @brief           Available application return values of the Nvm
 * @details         Redefinition of the Nvm_ReturnType as error type.
 */
typedef Nvm_ReturnType Nvm_ErrorType;

/**
 * @brief           Available Nvm block status types
 * @details         This enumeration implements all available nvm block status 
 */
typedef enum{
  NVM_BLK_OK = DATA_E_OK,
  NVM_BLK_PENDING = DATA_E_PENDING,
  NVM_BLK_SKIPPED = DATA_E_SKIPPED,
  NVM_BLK_INTEGRITY_FAILED = DATA_E_INTEGRITY_FAILED,
  NVM_BLK_INVALIDATE = DATA_E_INVALIDATE,
  NVM_BLK_WAITING = 100,
  NVM_BLK_EMPTY = 255,
}Nvm_BlockStatusType;

/**
 * @brief           Available Crc checksums for Nvm Blocks
 */
typedef enum {
  NVM_CRC_NONE = 0,
  NVM_CRC_CRC32 = 1,
}Nvm_AvailableCrcTypes;


/**
* @brief           Nvm block configuration type
*/
typedef struct{
  boolean nvmBlockReadAll;                        /*!< Select active block for the read all functionality */
  boolean nvmBlockWriteAll;                       /*!< Select active block for the write all functionality */
  uint16 nvmBlockId;                              /*!< Nvm module block id */
  uint16 nvmBlockDataLength;                      /*!< Nvm block data length */
  uint32 nvmBaseModuleBlkId;                      /*!< Sub module block id */
  uint8* nvmBlockUserRamVariable;                /*!< Pointer to ram variable >> Used for read and write all functionality */
  Nvm_AvailableCrcTypes nvmBlockUseCrc;           /*!< Nvm block integrity function >> Selectable integrity detection level */
  Nvm_BaseModules nvmBlockUsedBaseModule;         /*!< Nvm block used / stored base module */
}Nvm_BlockCfgType;

/**
* @brief           Nvm block configuration type
*/
typedef struct {
  Std_ReturnType(*Nvm_BaseModule_RequestWriteData)(uint16, uint8*);     /*!< Function pointer to the base module write request function */
  Std_ReturnType(*Nvm_BaseModule_RequestReadData)(uint16, uint8*);      /*!< Function pointer to the base module read request function */
  Data_StatusType(*Nvm_BaseModule_RequestBlockStatus)(uint16);          /*!< Function pointer to the base module request block status function */
  Std_ReturnType(*Nvm_BaseModule_RequestModuleStatus)(void);            /*!< Function pointer to the base module request module status function */
}Nvm_BaseModeCfgList;

/**
 * @brief           Available Nvm requests
 */
typedef enum {
  NVM_REQ_READ = 0,
  NVM_REQ_WRITE = 1,
  NVM_REQ_ERASE = 2
}Nvm_RequestType;

/**
 * @brief           Nvm Request List Enries
 */
typedef struct {
  uint16 nvmBlockId;                          /*!< Nvm Block Id */
  uint8* requestDataPointer;                  /*!< Request data pointer */
  Nvm_RequestType nvmRequestType;             /*!< Nvm Block action request */
  Nvm_BlockStatusType nvmRequestStatus;       /*!< Nvm Block action request */
  uint8 nvmData[NVM_MAX_REQUEST_DATA_SIZE];   /*!< Index of data list */
  uint8 nvmSafe;                              /*!< Safe data to detect an overflow */
}Nvm_RequestListEntryType;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of public data types                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            Start of public definition area                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of public definition area                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of global variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of global variables                      << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>           Start of public function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of public function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/**
 * @}
 */

#endif /* __NVM_TYPES_H*/
