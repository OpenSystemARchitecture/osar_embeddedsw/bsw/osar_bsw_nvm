﻿/*****************************************************************************************************************************
 * @file        Module.cs                                                                                                    *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        26.03.2019                                                                                                   *
 * @brief       Implementation of the Module Configuration Data Model                                                        *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
 * @addtogroup ModuleLibrary.Versions.v_1_0_0.Models
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OsarResources.XML;
using OsarResources.Generic;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name Space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace ModuleLibrary.Versions.v_1_0_0.Models
{
  public enum NvmCrcTypes
  {
    NVM_CRC_NONE,
    NVM_CRC_CRC32
  }

  public struct NvmBlkCfg
  {
    public String nvmBlockName;
    public UInt16 nvmBlockDataLength;
    public String nvmBlockRamVariable;
    public NvmCrcTypes nvmBlockUseCrc;
    public String nvmBlockUseSubModule;
    public Boolean nvmBlockUseForReadAll;
    public Boolean nvmBlockUseForWriteAll;
  }

  public class NvmXml
  {
    public XmlFileVersion xmlFileVersion;
    public UInt16 detModuleID;
    public SystemState detModuleUsage;

    public UInt16 miBMainfunctionCycleTimeMs;

    public Byte nvmRequestBufferSize;
    public List<String> nvmSubModuleList;
    public List<NvmBlkCfg> nvmBlkCfgList;
  }
}
/**
 * @}
 */
