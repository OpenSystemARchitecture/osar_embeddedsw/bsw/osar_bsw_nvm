﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ModuleLibrary.Versions.v_1_0_0.ViewModels;

namespace ModuleLibrary.Versions.v_1_0_0.Views
{
  /// <summary>
  /// Interaction logic for the ModuleCfgView.xaml
  /// </summary>
  public partial class ModuleCfgView : UserControl
  {
    ViewModels.Module_ViewModel newCfg;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="pathToCfgFile">Absolute Path to configuration file</param>
    /// <param name="absPathToModuleBaseFolder">Absolute Path to module base folder</param>
    public ModuleCfgView(string pathToCfgFile, string absPathToModuleBaseFolder)
    {
      newCfg = new ViewModels.Module_ViewModel(pathToCfgFile, absPathToModuleBaseFolder);

      InitializeComponent();

      /* Set Data Context where data is binded to*/
      this.DataContext = newCfg;
    }

    /// <summary>
    /// Button to add a new base module
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void B_AddNewBaseModule_Click(object sender, RoutedEventArgs e)
    {
      newCfg.AddNewBaseModule();
    }

    /// <summary>
    /// Button to remove the selected base module
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void B_RemoveSelectedBaseModule_Click(object sender, RoutedEventArgs e)
    {
      NvmBaseModuleVM removedBlock = (NvmBaseModuleVM)DG_NvmBaseModuleList.SelectedItem;
      newCfg.RemoveBaseModule(removedBlock);
    }

    /// <summary>
    /// Button to add a new Data Block
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void B_AddNewDataBlock_Click(object sender, RoutedEventArgs e)
    {
      newCfg.AddNewDataBlock();
    }

    /// <summary>
    /// Button to remove the selected data block
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void B_RemoveSelectedDataBlock_Click(object sender, RoutedEventArgs e)
    {
      NvmDataBlockVM removedBlock = (NvmDataBlockVM)DG_NvmDataBlockList.SelectedItem;
      newCfg.RemoveDataBlock(removedBlock);
    }
  }
}
