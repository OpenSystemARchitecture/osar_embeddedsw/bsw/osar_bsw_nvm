﻿/*****************************************************************************************************************************
 * @file        ModuleValidator.cs                                                                                           *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        06.04.2019                                                                                                   *
 * @brief       Implementation of the Module Validation Class                                                                *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
* @addtogroup ModuleLibrary.Versions.v_1_0_0.Generator
* @{
*/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OsarResources.Generator;
using OsarResources.XML;
using OsarResources.Generator.Resources;
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace ModuleLibrary.Versions.v_1_0_0.Generator
{
  internal class ModuleValidator
  {
    private Models.NvmXml xmlCfg;
    private string pathToConfiguratioFile;
    private GenInfoType info;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="cfgFile"> Config file which shall be generated </param>
    /// <param name="pathToCfgFile"> Path of Configuration file </param>
    public ModuleValidator(Models.NvmXml cfgFile, string pathToCfgFile)
    {
      xmlCfg = cfgFile;
      pathToConfiguratioFile = pathToCfgFile;

      info.info = new List<string>();
      info.log = new List<string>();
      info.warning = new List<string>();
      info.error = new List<string>();
    }

    /// <summary>
    /// Interface to validate the configuration
    /// </summary>
    /// <returns> Generation information </returns>
    public GenInfoType ValidateConfiguration()
    {
      UInt16 idx, idx2;

      info.AddLogMsg(ValidateResources.LogMsg_StartOfValidation);

      /* Validate configuration file version */
      XmlFileVersion defaultVersion = new XmlFileVersion();
      defaultVersion.MajorVersion = Convert.ToUInt16(DefResources.CfgFileMajorVersion);
      defaultVersion.MinorVersion = Convert.ToUInt16(DefResources.CfgFileMinorVersion);
      defaultVersion.PatchVersion = Convert.ToUInt16(DefResources.CfgFilePatchVersion);
      if (false == OsarResources.Generic.OsarGenericHelper.XmlFileVersionEqual(defaultVersion, xmlCfg.xmlFileVersion))
      {
        info.AddErrorMsg(Convert.ToUInt16(ErrorWarningCodes.Error_0000), ErrorWarningCodes.Error_0000_Msg);
      }

      /* ++++++++++ Check configured base modules. ++++++++++ */
      info.AddLogMsg(ValidateResources.LogMsg_CheckConfiguredBaseModules);
      if (xmlCfg.nvmSubModuleList == null)
      {
        info.AddErrorMsg(Convert.ToUInt16(GenErrorWarningCodes.Error_1000), GenErrorWarningCodes.Error_1000_Msg);
      }
      for (idx = 0; idx < xmlCfg.nvmSubModuleList.Count; idx++)
      {
        if (xmlCfg.nvmSubModuleList[idx].Contains(" "))
        {
          info.AddErrorMsg(Convert.ToUInt16(GenErrorWarningCodes.Error_1001), GenErrorWarningCodes.Error_1001_Msg);
        }
      }

      /* ++++++++++ Check configured request buffer size ++++++++++ */
      info.AddLogMsg(ValidateResources.LogMsg_CheckGeneralModuleSettings);
      if (xmlCfg.nvmRequestBufferSize == 0)
      {
        info.AddErrorMsg(Convert.ToUInt16(GenErrorWarningCodes.Error_1002), GenErrorWarningCodes.Error_1002_Msg);
      }

      if (xmlCfg.miBMainfunctionCycleTimeMs == 0)
      {
        info.AddErrorMsg(Convert.ToUInt16(GenErrorWarningCodes.Error_1003), GenErrorWarningCodes.Error_1003_Msg);
      }


      /* ++++++++++ Check configured base nvm blocks ++++++++++ */
      info.AddLogMsg(ValidateResources.LogMsg_CheckConfiguredNvmBlocks);
      if (xmlCfg.nvmBlkCfgList == null)
      {
        info.AddErrorMsg(Convert.ToUInt16(GenErrorWarningCodes.Error_1004), GenErrorWarningCodes.Error_1004_Msg);
      }

      /* Check each Nvm block for its valid configuration */
      for (idx = 0; idx < xmlCfg.nvmBlkCfgList.Count; idx++)
      {
        /* Check if block name exists */
        if (xmlCfg.nvmBlkCfgList[idx].nvmBlockName == null)
        {
          info.AddErrorMsg(Convert.ToUInt16(GenErrorWarningCodes.Error_1005), GenErrorWarningCodes.Error_1005_Msg + idx.ToString());
        }else if (xmlCfg.nvmBlkCfgList[idx].nvmBlockName.Contains(" ")) /* Check if block name has spaces */
        {
          info.AddErrorMsg(Convert.ToUInt16(GenErrorWarningCodes.Error_1006), GenErrorWarningCodes.Error_1006_Msg + idx.ToString() +
            " Block Name: \"" + xmlCfg.nvmBlkCfgList[idx].nvmBlockName + "\"");
        }

        /* Check minimum block length */
        if (xmlCfg.nvmBlkCfgList[idx].nvmBlockDataLength == 0)
        {
          info.AddErrorMsg(Convert.ToUInt16(GenErrorWarningCodes.Error_1007), GenErrorWarningCodes.Error_1007_Msg + xmlCfg.nvmBlkCfgList[idx].nvmBlockName);
        }

        if (xmlCfg.nvmBlkCfgList[idx].nvmBlockDataLength >= 65531)
        {
          info.AddErrorMsg(Convert.ToUInt16(GenErrorWarningCodes.Error_1008), GenErrorWarningCodes.Error_1008_Msg + xmlCfg.nvmBlkCfgList[idx].nvmBlockName);
        }

        /* Check if Sub Module has been configured */
        if (xmlCfg.nvmBlkCfgList[idx].nvmBlockUseSubModule == null)
        {
          info.AddErrorMsg(Convert.ToUInt16(GenErrorWarningCodes.Error_1009), GenErrorWarningCodes.Error_1009_Msg + xmlCfg.nvmBlkCfgList[idx].nvmBlockName);
        }

        /* Check if Sub Module exists */
        if (!( xmlCfg.nvmSubModuleList.Contains(xmlCfg.nvmBlkCfgList[idx].nvmBlockUseSubModule) ))
        {
          info.AddErrorMsg(Convert.ToUInt16(GenErrorWarningCodes.Error_1010), GenErrorWarningCodes.Error_1010_Msg + xmlCfg.nvmBlkCfgList[idx].nvmBlockName);
        }


        /* Check if ram variable has been configured of needed */
        if (( ( true == xmlCfg.nvmBlkCfgList[idx].nvmBlockUseForReadAll ) || ( true == xmlCfg.nvmBlkCfgList[idx].nvmBlockUseForWriteAll ) ) && ( xmlCfg.nvmBlkCfgList[idx].nvmBlockRamVariable == null ))
        {
          info.AddErrorMsg(Convert.ToUInt16(GenErrorWarningCodes.Error_1011), GenErrorWarningCodes.Error_1011_Msg + xmlCfg.nvmBlkCfgList[idx].nvmBlockName);
        }

        /* Check for duplicated Nvm block names */
        for (idx2 = 0; idx2 < xmlCfg.nvmBlkCfgList.Count; idx2++)
        {
          /* Skipp active element */
          if (idx != idx2)
          {
            /* Search for duplicated Nvm block names */
            if (xmlCfg.nvmBlkCfgList[idx].nvmBlockName == xmlCfg.nvmBlkCfgList[idx2].nvmBlockName)
            {
              info.AddErrorMsg(Convert.ToUInt16(GenErrorWarningCodes.Error_1012), GenErrorWarningCodes.Error_1012_Msg + idx.ToString() + 
                " && Block index 2: " + idx2.ToString());
            }
          }
        }
      }

      info.AddLogMsg(ValidateResources.LogMsg_ValidationDone);
      return info;
    }
  }
}
