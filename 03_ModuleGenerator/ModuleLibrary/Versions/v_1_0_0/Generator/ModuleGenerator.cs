﻿/*****************************************************************************************************************************
 * @file        ModuleGenerator.cs                                                                                           *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        06.04.2019                                                                                                   *
 * @brief       Implementation of the Module Generator Class                                                                 *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
* @addtogroup ModuleLibrary.Versions.v_1_0_0.Generator
* @{
*/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OsarResources.Generator;
using OsarResources.Generator.Resources;
using System.Reflection;
using System.Diagnostics;
using System.IO;
using System.Xml.Serialization;
using OsarSourceFileLib.DocumentationObjects.Doxygen;
using OsarSourceFileLib.CFile;
using OsarSourceFileLib.CFile.CFileObjects;
using OsarSourceFileLib.DocumentationObjects.General;
using RteLib;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace ModuleLibrary.Versions.v_1_0_0.Generator
{
  internal class ModuleGenerator
  {
    private Models.NvmXml xmlCfg;
    private string pathToConfiguratioFile;
    private string pathToModuleBaseFolder;
    private string moduleLibPath;
    private GenInfoType info;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="cfgFile"> Config file which shall be generated </param>
    /// <param name="pathToCfgFile"> Path of Configuration file </param>
    /// <param name="absPathToBaseModuleFolder"> Absolute path to module base folder</param>
    public ModuleGenerator(Models.NvmXml cfgFile, string pathToCfgFile, string absPathToBaseModuleFolder)
    {
      xmlCfg = cfgFile;
      pathToConfiguratioFile = pathToCfgFile;
      pathToModuleBaseFolder = absPathToBaseModuleFolder;

      moduleLibPath = new System.Uri(Assembly.GetExecutingAssembly().CodeBase).AbsolutePath;

      info.info = new List<string>();
      info.log = new List<string>();
      info.warning = new List<string>();
      info.error = new List<string>();
    }

    /// <summary>
    /// Interface to generate the configuration
    /// </summary>
    /// <returns> Generation information </returns>
    public GenInfoType GenerateConfiguration()
    {
      // Generate Memory-Map-Header-File
      GenerateMemoryMapHeaderFile();

      // Generate C-Source-File
      GenerateConfigurationSourceFile();

      // Generate C-Header-File
      GenerateConfigurationHeaderFile();

      // Generate Rte-Interface-XML-File
      GenerateConfigurationRteInterfaceXmlFile();

      return info;
    }

    /// <summary>
    /// Interface to generate the Memory-Map-Header-File
    /// </summary>
    /// <returns> Generation information </returns>
    public void GenerateMemoryMapHeaderFile()
    {
      info.AddLogMsg(GenResources.LogMsg_StartGenMemMap);

      /* +++++ Generate Memory Mapping file +++++ */
      OsarResources.Generic.OsarGenericHelper.generateModuleMemoryMappingFile(pathToModuleBaseFolder + GenResources.GenHeaderFilePath, DefResources.ModuleName, DefResources.GeneratorName);

      info.AddLogMsg(GenResources.LogMsg_MemMapGenerated);
    }

    /// <summary>
    /// Interface to generate the configuration C-Source-File
    /// </summary>
    /// <returns> Generation information </returns>
    public void GenerateConfigurationSourceFile()
    {
      info.AddLogMsg(GenResources.LogMsg_GenSourceCfgFile);

      CSourceFile modulePBCfgSource = new CSourceFile();
      DoxygenFileHeader doxygenFileHeader = new DoxygenFileHeader();
      DoxygenFileGroupOpener doxygenFileGroupOpener = new DoxygenFileGroupOpener();
      DoxygenFileGroupCloser doxygenFileGroupCloser = new DoxygenFileGroupCloser();
      GeneralStartGroupObject generalStartGroup = new GeneralStartGroupObject();
      GeneralEndGroupObject generalEndGroup = new GeneralEndGroupObject();
      CFileIncludeObjects sourceIncludes = new CFileIncludeObjects();
      CFileVariableObjects sourceVariables = new CFileVariableObjects();
      CFileFunctionObjects sourceFunction = new CFileFunctionObjects();
      DoxygenElementDescription doxygenDescription = new DoxygenElementDescription();
      int actualGroupIdx, idx;
      string tempString;

      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* ++++++++++++++++++++++++++++++++++++++++ Prepare general data sets ++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      doxygenFileHeader.AddFileBrief("Implementation of Nvm module *_PBCfg.c file");
      doxygenFileHeader.AddFileName(DefResources.ModuleName + "_PBCfg.c");
      doxygenFileHeader.AddFileNote(OsarResources.Generator.Resources.genericSource.CommentGeneratorDisclaimer);
      doxygenFileHeader.AddFileAuthor(DefResources.GenFileAuthor);
      doxygenFileHeader.AddFileGenerator(DefResources.GeneratorName + " v." + ModuleLibraryVersionClass.getCurrentVersion());
      doxygenFileHeader.AddFileVersion(DefResources.ModuleVersion);
      doxygenFileGroupOpener.AddToDoxygenMasterGroup(OsarResources.Generator.Resources.genericSource.DoxygenOsarMasterGroupBSW);
      doxygenFileGroupOpener.AddToDoxygenGroup(DefResources.ModuleName);

      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* ++++++++++++++++++++++++++++++++++++++++++ Prepare include strings ++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      sourceIncludes.AddIncludeFile(DefResources.ModuleName + "_Types.h");
      sourceIncludes.AddIncludeFile(DefResources.ModuleName + "_PBCfg.h");

      for (idx = 0; idx < xmlCfg.nvmSubModuleList.Count; idx++)
      {
        sourceIncludes.AddIncludeFile(xmlCfg.nvmSubModuleList[idx] + ".h");
      }

      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* ++++++++++++++++++++++++++++++++++++++++++++ Prepare Variables ++++++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

      /* Adding external variable references */
      generalStartGroup.AddGroupName("Adding external defined variables");
      generalStartGroup.AddCustomStartGroupString("");
      generalEndGroup.AddCustomEndGroupString("");
      actualGroupIdx = sourceVariables.AddCFileObjectGroup(generalStartGroup, generalEndGroup);
      for (idx = 0; idx < xmlCfg.nvmBlkCfgList.Count; idx++)
      {
        if (( xmlCfg.nvmBlkCfgList[idx].nvmBlockRamVariable == null ) || 
          ( xmlCfg.nvmBlkCfgList[idx].nvmBlockRamVariable == "" ))
        { /* Nothing to do */}
        else
        {
          sourceVariables.AddVariable("uint8", xmlCfg.nvmBlkCfgList[idx].nvmBlockRamVariable + 
            "[]", actualGroupIdx, "", "extern");
        }
      }

      /* ---------- Add variable informations ----------*/
      /* >> Add Basic Nvm data*/
      generalStartGroup.AddGroupName("Map variables into constant memory");
      generalStartGroup.AddOsarStartMemMapDefine(DefResources.ModuleName, OsarMemMapDataType.OSAR_CONST);
      generalEndGroup.AddOsarStopMemMapDefine(DefResources.ModuleName, OsarMemMapDataType.OSAR_CONST);
      actualGroupIdx = sourceVariables.AddCFileObjectGroup(generalStartGroup, generalEndGroup);

      /* Build Nvm block configuration list */
      tempString = "";
      for (idx = 0; idx < xmlCfg.nvmBlkCfgList.Count; idx++)
      {
        if (idx == 0)
        { tempString += "{"; }
        else
        { tempString += ", {"; }

        tempString += xmlCfg.nvmBlkCfgList[idx].nvmBlockUseForReadAll.ToString().ToUpper() + ",";
        tempString += xmlCfg.nvmBlkCfgList[idx].nvmBlockUseForWriteAll.ToString().ToUpper() + ",";
        tempString += idx.ToString() + ",";
        tempString += xmlCfg.nvmBlkCfgList[idx].nvmBlockDataLength.ToString() + ",";
        tempString += xmlCfg.nvmBlkCfgList[idx].nvmBlockUseSubModule.ToUpper() + "_BLK_" + 
          xmlCfg.nvmBlkCfgList[idx].nvmBlockName.ToUpper() + ",";
        if (( xmlCfg.nvmBlkCfgList[idx].nvmBlockRamVariable == null ) || 
          ( xmlCfg.nvmBlkCfgList[idx].nvmBlockRamVariable == "" ))
        { tempString += "NULL_PTR,"; }
        else
        { tempString += "&" + xmlCfg.nvmBlkCfgList[idx].nvmBlockRamVariable + "[0],"; }
        tempString += xmlCfg.nvmBlkCfgList[idx].nvmBlockUseCrc.ToString() + ",";
        tempString += "NVM_" + xmlCfg.nvmBlkCfgList[idx].nvmBlockUseSubModule.ToUpper();
      }
      sourceVariables.AddArray("Nvm_BlockCfgType", "nvmBlkCfgList", GenResources.GenDefineCntConfiguredNvmBlocks, 
        actualGroupIdx, tempString, "const", arrayRearrangeType.AUTO_DETECT);


      /* Build Nvm base module list */
      tempString = "";
      for (idx = 0; idx < xmlCfg.nvmSubModuleList.Count; idx++)
      {
        if (idx == 0)
        { tempString += "{"; }
        else
        { tempString += ", {"; }

        tempString += "&" + xmlCfg.nvmSubModuleList[idx] + "_RequestWriteData,";
        tempString += "&" + xmlCfg.nvmSubModuleList[idx] + "_RequestReadData,";
        tempString += "&" + xmlCfg.nvmSubModuleList[idx] + "_RequestBlockStatus,";
        tempString += "&" + xmlCfg.nvmSubModuleList[idx] + "_RequestModuleStatus";
      }
      sourceVariables.AddArray("Nvm_BaseModeCfgList", "nvmBaseModuleList",
        GenResources.GenDefineCntConfiguredNvmBaseModules, actualGroupIdx, tempString, "const",
        arrayRearrangeType.AUTO_DETECT);

      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++ Prepare Source Functions ++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* ++++++++++++++++++++++++++++++++++++++++ Build up and generate file +++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      modulePBCfgSource.AddFileName(DefResources.ModuleName + "_PBCfg.c");
      modulePBCfgSource.AddFilePath(pathToModuleBaseFolder + GenResources.GenSourceFilePath);
      modulePBCfgSource.AddFileCommentHeader(doxygenFileHeader);
      modulePBCfgSource.AddFileGroup(doxygenFileGroupOpener, doxygenFileGroupCloser);
      modulePBCfgSource.AddIncludeObject(sourceIncludes);
      modulePBCfgSource.AddGlobalVariableObject(sourceVariables);
      modulePBCfgSource.AddGlobalFunctionObject(sourceFunction);
      modulePBCfgSource.GenerateSourceFile();

      info.AddLogMsg(GenResources.LogMsg_SourceCfgFileGenerated);
    }

    /// <summary>
    /// Interface to generate the configuration C-Header-File
    /// </summary>
    /// <returns> Generation information </returns>
    public void GenerateConfigurationHeaderFile()
    {
      info.AddLogMsg(GenResources.LogMsg_GenHeaderCfgFile);

      CHeaderFile modulePBCfgHeader = new CHeaderFile();
      DoxygenFileHeader doxygenFileHeader = new DoxygenFileHeader();
      DoxygenFileGroupOpener doxygenFileGroupOpener = new DoxygenFileGroupOpener();
      DoxygenFileGroupCloser doxygenFileGroupCloser = new DoxygenFileGroupCloser();
      CFileDefinitionObjects headerDefinitions = new CFileDefinitionObjects();
      GeneralStartGroupObject generalStartGroup = new GeneralStartGroupObject();
      GeneralEndGroupObject generalEndGroup = new GeneralEndGroupObject();
      CFileIncludeObjects headerIncludes = new CFileIncludeObjects();
      CFileVariableObjects headerVariables = new CFileVariableObjects();
      DoxygenElementDescription doxygenDescription = new DoxygenElementDescription();
      CFileTypeObjects headerTypes = new CFileTypeObjects();

      int actualGroupIdx, idx, maxDataSize;

      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* ++++++++++++++++++++++++++++++++++++++++ Prepare general data sets ++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      doxygenFileHeader.AddFileBrief("Generated header file data of the Nvm module.");
      doxygenFileHeader.AddFileName(DefResources.ModuleName + "_PBCfg.h");
      doxygenFileHeader.AddFileNote(OsarResources.Generator.Resources.genericSource.CommentGeneratorDisclaimer);
      doxygenFileHeader.AddFileAuthor(DefResources.GenFileAuthor);
      doxygenFileHeader.AddFileGenerator(DefResources.GeneratorName + " v." + ModuleLibraryVersionClass.getCurrentVersion());
      doxygenFileHeader.AddFileVersion(DefResources.ModuleVersion);
      doxygenFileGroupOpener.AddToDoxygenMasterGroup(OsarResources.Generator.Resources.genericSource.DoxygenOsarMasterGroupBSW);
      doxygenFileGroupOpener.AddToDoxygenGroup(DefResources.ModuleName);

      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* ++++++++++++++++++++++++++++++++++++++++++ Prepare include strings ++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      //headerIncludes.AddIncludeFile("Std_Types.h");

      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* ++++++++++++++++++++++++++++++++++++++++++++ Prepare data types +++++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* Add data type */
      doxygenDescription.AddElementBrief("Nvm available base module definitions.");
      string[] availableSubModuleDef = new String[xmlCfg.nvmSubModuleList.Count];
      for (idx = 0; idx < xmlCfg.nvmSubModuleList.Count; idx++)
      {
        availableSubModuleDef[idx] = "NVM_" + xmlCfg.nvmSubModuleList[idx].ToUpper() + " = " + idx.ToString() + ",";
      }
      headerTypes.AddEnumerationTypeDefinition("Nvm_BaseModules", availableSubModuleDef, doxygenDescription);

      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++ Prepare Definitions +++++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* Create Det informations */
      generalStartGroup.AddGroupName("Nvm Module Det Information");
      actualGroupIdx = headerDefinitions.AddCFileObjectGroup(generalStartGroup, generalEndGroup);
      headerDefinitions.AddDefinition(GenResources.GenDefineDetModuleId,
        xmlCfg.detModuleID.ToString(), actualGroupIdx);
      headerDefinitions.AddDefinition(GenResources.GenDefineModuleUseDet,
        xmlCfg.detModuleUsage.ToString(), actualGroupIdx);


      /* Create general configuration */
      generalStartGroup.AddGroupName("General Nvm module configuration");
      actualGroupIdx = headerDefinitions.AddCFileObjectGroup(generalStartGroup, generalEndGroup);

      headerDefinitions.AddDefinition(GenResources.GenDefineCntConfiguredNvmBlocks,
        xmlCfg.nvmBlkCfgList.Count.ToString(), actualGroupIdx);
      headerDefinitions.AddDefinition(GenResources.GenDefineCntConfiguredNvmBaseModules,
        xmlCfg.nvmSubModuleList.Count.ToString(), actualGroupIdx);
      headerDefinitions.AddDefinition(GenResources.GenDefineRequestListSize,
        xmlCfg.nvmRequestBufferSize.ToString(), actualGroupIdx);

      maxDataSize = 0;
      for (idx = 0; idx < xmlCfg.nvmBlkCfgList.Count; idx++)
      {
        if (xmlCfg.nvmBlkCfgList[idx].nvmBlockDataLength > maxDataSize)
        {
          maxDataSize = xmlCfg.nvmBlkCfgList[idx].nvmBlockDataLength;
        }
      }
      maxDataSize += 4 + 2; // Adding CRC size plus 2 empty bytes.
      headerDefinitions.AddDefinition(GenResources.GenDefineRequestListDataSize, maxDataSize.ToString(),
        actualGroupIdx);

      for (idx = 0; idx < xmlCfg.nvmBlkCfgList.Count; idx++)
      {
        generalStartGroup.AddGroupName("Nvm User Nvm block configuration >> Block Name: \"" +
          xmlCfg.nvmBlkCfgList[idx].nvmBlockName + "\"");
        actualGroupIdx = headerDefinitions.AddCFileObjectGroup(generalStartGroup, generalEndGroup);
        headerDefinitions.AddDefinition(GenResources.GenDefineUserBlockId +
          xmlCfg.nvmBlkCfgList[idx].nvmBlockName.ToUpper(), idx.ToString(), actualGroupIdx);
        headerDefinitions.AddDefinition("Nvm_WriteBlock_" + xmlCfg.nvmBlkCfgList[idx].nvmBlockName + 
          "(pNvmWriteData)", "Nvm_WriteBlock(" + idx.ToString() + ", pNvmWriteData)", actualGroupIdx);
        headerDefinitions.AddDefinition("Nvm_ReadBlock_" + xmlCfg.nvmBlkCfgList[idx].nvmBlockName + 
          "(pNvmReadData)", "Nvm_ReadBlock(" + idx.ToString() + ", pNvmReadData)", actualGroupIdx);
        headerDefinitions.AddDefinition("Nvm_EraseBlock_" + xmlCfg.nvmBlkCfgList[idx].nvmBlockName + 
          "()", "Nvm_EraseNvBlock(" + idx.ToString() + ")", actualGroupIdx);
        headerDefinitions.AddDefinition("Nvm_GetErrorStatus_" + xmlCfg.nvmBlkCfgList[idx].nvmBlockName + 
          "(nvmBlockStatus)", "Nvm_GetErrorStatus(" + idx.ToString() + ", nvmBlockStatus)", actualGroupIdx);
        //TODO: Add Function redefinition.
      }

      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++ Prepare Header Functions ++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* ++++++++++++++++++++++++++++++++++++++++ Build up and generate file +++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      modulePBCfgHeader.AddFileName(DefResources.ModuleName + "_PBCfg.h");
      modulePBCfgHeader.AddFilePath(pathToModuleBaseFolder + GenResources.GenHeaderFilePath);
      modulePBCfgHeader.AddFileCommentHeader(doxygenFileHeader);
      modulePBCfgHeader.AddFileGroup(doxygenFileGroupOpener, doxygenFileGroupCloser);
      modulePBCfgHeader.AddDefinitionObject(headerDefinitions);
      modulePBCfgHeader.AddIncludeObject(headerIncludes);
      modulePBCfgHeader.AddGlobalVariableObject(headerVariables);
      modulePBCfgHeader.AddTypesObject(headerTypes);
      modulePBCfgHeader.GenerateSourceFile();

      info.AddLogMsg(GenResources.LogMsg_HeaderCfgFileGenerated);
    }

    /// <summary>
    /// Interface to generate the configuration Rte-Interface-XML-File
    /// </summary>
    /// <returns> Generation information </returns>
    public void GenerateConfigurationRteInterfaceXmlFile()
    {
      info.AddLogMsg(GenResources.LogMsg_GenRteMibCfgFile);
      info.AddLogMsg("Using RteLib version: v." + RteLib.VersionClass.major.ToString() + "." +
        RteLib.VersionClass.minor.ToString() + "." + RteLib.VersionClass.patch.ToString());

      // Create main instance of internal behavior file
      RteLib.RteModuleInternalBehavior.RteModuleInternalBehavior internalBehavior = new RteLib.RteModuleInternalBehavior.RteModuleInternalBehavior();
      
      RteLib.RteModuleInternalBehavior.RteRunnable initRunnable = new RteLib.RteModuleInternalBehavior.RteRunnable();
      
      RteLib.RteModuleInternalBehavior.RteCyclicRunnable mainfunctionRunnable = new RteLib.RteModuleInternalBehavior.RteCyclicRunnable();

      // Create Port information
      RteLib.RteModuleInternalBehavior.RteAvailableServerPort csI_PpGeneralNvm = new RteLib.RteModuleInternalBehavior.RteAvailableServerPort();


      // Create Interface Blueprint Instances
      // gii == General Interface Info
      // ibp == Interface Blue Print
      // cs == Client Server
      RteLib.RteInterface.RteCSInterfaceBlueprint cs_ipb_PpGeneralNvm = new RteLib.RteInterface.RteCSInterfaceBlueprint();
      RteLib.RteInterface.RteGeneralInterfaceInfo gii_ibp_PpGeneralNvm = new RteLib.RteInterface.RteGeneralInterfaceInfo();

      // Create Types
      // adt == Advanced Data Type
      RteLib.RteTypes.RteAdvancedDataTypeStructure adt_BlockStatusType = new RteLib.RteTypes.RteAdvancedDataTypeStructure();

      // Helper Objects
      RteLib.RteInterface.RteGeneralInterfaceInfo generalInterfaceInfo;
      RteLib.RteInterface.RteGeneralInterfaceInfo runabbleInfo = new RteLib.RteInterface.RteGeneralInterfaceInfo();
      RteLib.RteModuleInternalBehavior.RteRunnable rteRunnable;
      RteLib.RteInterface.RteFunctionBlueprint rteFncBlueprint;
      RteLib.RteInterface.RteFunctionArgumentBlueprint rteFncArgBlueprint;

      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++ Create Types ++++++++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      #region Adding Rte Types 
      RteLib.RteTypes.RteBaseTypesResource rteBaseTypesResource = new RteLib.RteTypes.RteBaseTypesResource();

      /* ########## Adding Server Port Interface Blueprint "Nvm_BlockStatusType" ########## */
      RteLib.RteTypes.RteBaseDataTypeStructure t_Nvm_BlockStatusType = new RteLib.RteTypes.RteBaseDataTypeStructure();
      t_Nvm_BlockStatusType.DataTypeName = "Nvm_BlockStatusType";
      t_Nvm_BlockStatusType.UUID = "c75650ab8eb94f37a27899ce64c49768";
      t_Nvm_BlockStatusType.DataAccessType = RteLib.RteTypes.RteDataAccessType.STANDARD;

      adt_BlockStatusType.DataType = t_Nvm_BlockStatusType;
      adt_BlockStatusType.DataTypeType = RteLib.RteTypes.RteAdvancedDataTypeType.ENUMERATION;
      adt_BlockStatusType.EnumElements = new List<RteLib.RteTypes.RteAdvancedEnumDataTypeStructure>();

      RteLib.RteTypes.RteAdvancedEnumDataTypeStructure aedt_element = new RteLib.RteTypes.RteAdvancedEnumDataTypeStructure();
      aedt_element.EnumElementName = "NVM_BLK_OK";
      aedt_element.EnumId = "0";
      adt_BlockStatusType.EnumElements.Add(aedt_element);

      aedt_element = new RteLib.RteTypes.RteAdvancedEnumDataTypeStructure();
      aedt_element.EnumElementName = "NVM_BLK_PENDING";
      aedt_element.EnumId = "1";
      adt_BlockStatusType.EnumElements.Add(aedt_element);

      aedt_element = new RteLib.RteTypes.RteAdvancedEnumDataTypeStructure();
      aedt_element.EnumElementName = "NVM_BLK_SKIPPED";
      aedt_element.EnumId = "2";
      adt_BlockStatusType.EnumElements.Add(aedt_element);

      aedt_element = new RteLib.RteTypes.RteAdvancedEnumDataTypeStructure();
      aedt_element.EnumElementName = "NVM_BLK_INTEGRITY_FAILED";
      aedt_element.EnumId = "3";
      adt_BlockStatusType.EnumElements.Add(aedt_element);

      aedt_element = new RteLib.RteTypes.RteAdvancedEnumDataTypeStructure();
      aedt_element.EnumElementName = "NVM_BLK_INVALIDATE";
      aedt_element.EnumId = "4";
      adt_BlockStatusType.EnumElements.Add(aedt_element);

      aedt_element = new RteLib.RteTypes.RteAdvancedEnumDataTypeStructure();
      aedt_element.EnumElementName = "NVM_BLK_WAITING";
      aedt_element.EnumId = "100";
      adt_BlockStatusType.EnumElements.Add(aedt_element);

      aedt_element = new RteLib.RteTypes.RteAdvancedEnumDataTypeStructure();
      aedt_element.EnumElementName = "NVM_BLK_EMPTY";
      aedt_element.EnumId = "255";
      adt_BlockStatusType.EnumElements.Add(aedt_element);

      #endregion
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* ++++++++++++++++++++++++++++++++++++++++ Create Blueprint Interfaces CS +++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* ########## Adding Server Port Interface Blueprint "PpGeneralNvm" ########## */
      #region Adding Server Port Interface Blueprint "PpGeneralNvm"
      // Create general prototype information
      gii_ibp_PpGeneralNvm.InterfaceName = "PiGeneralNvm";
      gii_ibp_PpGeneralNvm.UUID = "77a5ca48708c4a2eb86c6d22b8bb5ebc";
      cs_ipb_PpGeneralNvm.InterfaceInfo = gii_ibp_PpGeneralNvm; //Adding information to PpGeneralNvm Interface Blueprint
      cs_ipb_PpGeneralNvm.Functions = new List<RteLib.RteInterface.RteFunctionBlueprint>();

      #region Function Nvm_GetErrorStatus
      rteFncBlueprint = new RteLib.RteInterface.RteFunctionBlueprint();
      rteFncBlueprint.ReturnValue = rteBaseTypesResource.Rte_ErrorType_Type.DataType;
      rteFncBlueprint.AvailableErrorTypes = new List<string>();
      rteFncBlueprint.AvailableErrorTypes.Add("NotOk");
      rteFncBlueprint.FunctionName = "Nvm_GetErrorStatus";
      rteFncBlueprint.UUID = "9248831213ba4e4d974b7459402d0723";

      rteFncBlueprint.ArgList = new List<RteLib.RteInterface.RteFunctionArgumentBlueprint>();
      rteFncArgBlueprint = new RteLib.RteInterface.RteFunctionArgumentBlueprint();
      rteFncArgBlueprint.ArgumentName = "nvmBlockId";
      rteFncArgBlueprint.ArgumentType = rteBaseTypesResource.Uint16_Type;
      rteFncBlueprint.ArgList.Add(rteFncArgBlueprint);

      rteFncArgBlueprint = new RteLib.RteInterface.RteFunctionArgumentBlueprint();
      rteFncArgBlueprint.ArgumentName = "nvmBlockStatus";
      rteFncArgBlueprint.ArgumentType = adt_BlockStatusType.DataType;
      rteFncArgBlueprint.ArgumentType.DataAccessType = RteLib.RteTypes.RteDataAccessType.POINTER;
      rteFncBlueprint.ArgList.Add(rteFncArgBlueprint);

      cs_ipb_PpGeneralNvm.Functions.Add(rteFncBlueprint);
      #endregion

      #region Function Nvm_WriteBlock
      rteFncBlueprint = new RteLib.RteInterface.RteFunctionBlueprint();
      rteFncBlueprint.ReturnValue = rteBaseTypesResource.Rte_ErrorType_Type.DataType;
      rteFncBlueprint.AvailableErrorTypes = new List<string>();
      rteFncBlueprint.AvailableErrorTypes.Add("NotOk");
      rteFncBlueprint.AvailableErrorTypes.Add("Skipped");
      rteFncBlueprint.FunctionName = "Nvm_WriteBlock";
      rteFncBlueprint.UUID = "269c82f573aa41da83e6acf7f4df2e4e";

      rteFncBlueprint.ArgList = new List<RteLib.RteInterface.RteFunctionArgumentBlueprint>();
      rteFncArgBlueprint = new RteLib.RteInterface.RteFunctionArgumentBlueprint();
      rteFncArgBlueprint.ArgumentName = "nvmBlockId";
      rteFncArgBlueprint.ArgumentType = rteBaseTypesResource.Uint16_Type;
      rteFncBlueprint.ArgList.Add(rteFncArgBlueprint);

      rteFncArgBlueprint = new RteLib.RteInterface.RteFunctionArgumentBlueprint();
      rteFncArgBlueprint.ArgumentName = "pNvmWriteData";
      rteFncArgBlueprint.ArgumentType = rteBaseTypesResource.Uint8_Type;
      rteFncArgBlueprint.ArgumentType.DataAccessType = RteLib.RteTypes.RteDataAccessType.POINTER;
      rteFncBlueprint.ArgList.Add(rteFncArgBlueprint);

      cs_ipb_PpGeneralNvm.Functions.Add(rteFncBlueprint);
      #endregion

      #region Function Nvm_ReadBlock
      rteFncBlueprint = new RteLib.RteInterface.RteFunctionBlueprint();
      rteFncBlueprint.ReturnValue = rteBaseTypesResource.Rte_ErrorType_Type.DataType;
      rteFncBlueprint.AvailableErrorTypes = new List<string>();
      rteFncBlueprint.AvailableErrorTypes.Add("NotOk");
      rteFncBlueprint.AvailableErrorTypes.Add("Skipped");
      rteFncBlueprint.FunctionName = "Nvm_ReadBlock";
      rteFncBlueprint.UUID = "352cb060069648b9b74209b6d6a55838";

      rteFncBlueprint.ArgList = new List<RteLib.RteInterface.RteFunctionArgumentBlueprint>();
      rteFncArgBlueprint = new RteLib.RteInterface.RteFunctionArgumentBlueprint();
      rteFncArgBlueprint.ArgumentName = "nvmBlockId";
      rteFncArgBlueprint.ArgumentType = rteBaseTypesResource.Uint16_Type;
      rteFncBlueprint.ArgList.Add(rteFncArgBlueprint);

      rteFncArgBlueprint = new RteLib.RteInterface.RteFunctionArgumentBlueprint();
      rteFncArgBlueprint.ArgumentName = "pNvmReadData";
      rteFncArgBlueprint.ArgumentType = rteBaseTypesResource.Uint8_Type;
      rteFncArgBlueprint.ArgumentType.DataAccessType = RteLib.RteTypes.RteDataAccessType.POINTER;
      rteFncBlueprint.ArgList.Add(rteFncArgBlueprint);

      cs_ipb_PpGeneralNvm.Functions.Add(rteFncBlueprint);
      #endregion

      #region Function Nvm_EraseNvBlock
      rteFncBlueprint = new RteLib.RteInterface.RteFunctionBlueprint();
      rteFncBlueprint.ReturnValue = rteBaseTypesResource.Rte_ErrorType_Type.DataType;
      rteFncBlueprint.AvailableErrorTypes = new List<string>();
      rteFncBlueprint.AvailableErrorTypes.Add("NotOk");
      rteFncBlueprint.AvailableErrorTypes.Add("Skipped");
      rteFncBlueprint.FunctionName = "Nvm_EraseNvBlock";
      rteFncBlueprint.UUID = "72b85b0342e648b4967863177f7640ef";

      rteFncBlueprint.ArgList = new List<RteLib.RteInterface.RteFunctionArgumentBlueprint>();
      rteFncArgBlueprint = new RteLib.RteInterface.RteFunctionArgumentBlueprint();
      rteFncArgBlueprint.ArgumentName = "nvmBlockId";
      rteFncArgBlueprint.ArgumentType = rteBaseTypesResource.Uint16_Type;
      rteFncBlueprint.ArgList.Add(rteFncArgBlueprint);

      cs_ipb_PpGeneralNvm.Functions.Add(rteFncBlueprint);
      #endregion

      #endregion

      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* ++++++++++++++++++++++++++++++++++++++++ Create Blueprint Interfaces SR +++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* ++++++++++++++++++++++++++++++++++++++++++ Create CS Port Interfaces ++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* ########## Adding Server Port "PpGeneralNvm" ########## */
      #region Adding Server Port "PpGeneralNvm"
      // Create general prototype information
      generalInterfaceInfo = new RteLib.RteInterface.RteGeneralInterfaceInfo();
      generalInterfaceInfo.InterfaceName = "PpGeneralNvm";
      generalInterfaceInfo.UUID = "523801fe9fc6436790792789538a0a20";

      RteLib.RteInterface.RteCSInterfacePrototype csIpt_PpGeneralNvm = new RteLib.RteInterface.RteCSInterfacePrototype();
      csIpt_PpGeneralNvm.InterfacePrototype = generalInterfaceInfo;
      csIpt_PpGeneralNvm.InterfaceBlueprint = gii_ibp_PpGeneralNvm;
      csIpt_PpGeneralNvm.InterfaceType = RteLib.RteInterface.RteCSInterfaceImplementationType.SERVER;
      csI_PpGeneralNvm.ServerPort = csIpt_PpGeneralNvm; //Adding information to PpGeneralNvm Port

      // Create Runnable Information
      csI_PpGeneralNvm.ServerPortRunnables = new List<RteLib.RteModuleInternalBehavior.RteRunnable>();
      rteRunnable = new RteLib.RteModuleInternalBehavior.RteRunnable();

      runabbleInfo = new RteLib.RteInterface.RteGeneralInterfaceInfo();
      runabbleInfo.InterfaceName = "Nvm_GetErrorStatus";
      runabbleInfo.UUID = "9248831213ba4e4d974b7459402d0723";
      rteRunnable.RunnableInfo = runabbleInfo;
      csI_PpGeneralNvm.ServerPortRunnables.Add(rteRunnable); //Adding information to PpGeneralNvm Port

      rteRunnable = new RteLib.RteModuleInternalBehavior.RteRunnable();
      runabbleInfo = new RteLib.RteInterface.RteGeneralInterfaceInfo();
      runabbleInfo.InterfaceName = "Nvm_WriteBlock";
      runabbleInfo.UUID = "269c82f573aa41da83e6acf7f4df2e4e";
      rteRunnable.RunnableInfo = runabbleInfo;
      csI_PpGeneralNvm.ServerPortRunnables.Add(rteRunnable); //Adding information to PpGeneralNvm Port

      rteRunnable = new RteLib.RteModuleInternalBehavior.RteRunnable();
      runabbleInfo = new RteLib.RteInterface.RteGeneralInterfaceInfo();
      runabbleInfo.InterfaceName = "Nvm_ReadBlock";
      runabbleInfo.UUID = "352cb060069648b9b74209b6d6a55838";
      rteRunnable.RunnableInfo = runabbleInfo;
      csI_PpGeneralNvm.ServerPortRunnables.Add(rteRunnable); //Adding information to PpGeneralNvm Port

      rteRunnable = new RteLib.RteModuleInternalBehavior.RteRunnable();
      runabbleInfo = new RteLib.RteInterface.RteGeneralInterfaceInfo();
      runabbleInfo.InterfaceName = "Nvm_EraseNvBlock";
      runabbleInfo.UUID = "72b85b0342e648b4967863177f7640ef";
      rteRunnable.RunnableInfo = runabbleInfo;
      csI_PpGeneralNvm.ServerPortRunnables.Add(rteRunnable); //Adding information to PpGeneralNvm Port
      #endregion


      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* ++++++++++++++++++++++++++++++++++++++++++ Create SR Port Interfaces ++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++ Create Init Runnable ++++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      runabbleInfo = new RteLib.RteInterface.RteGeneralInterfaceInfo();
      runabbleInfo.InterfaceName = GenResources.RteMib_InitRunnableName;
      runabbleInfo.UUID = GenResources.RteMib_InitRunnableName_UUID;

      initRunnable.RunnableInfo = runabbleInfo;

      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* ++++++++++++++++++++++++++++++++++++++++++ Create Cyclic Runnable +++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      runabbleInfo = new RteLib.RteInterface.RteGeneralInterfaceInfo();
      runabbleInfo.InterfaceName = GenResources.RteMib_MainfunctionRunnableName;
      runabbleInfo.UUID = GenResources.RteMib_MainfunctionRunnableName_UUID;

      rteRunnable = new RteLib.RteModuleInternalBehavior.RteRunnable();
      rteRunnable.RunnableInfo = runabbleInfo;
      rteRunnable.AccessToClientPortFunctions = new List<RteLib.RteModuleInternalBehavior.RteGenericRunnableObject>();
      rteRunnable.AccessToSenderReceiverPortElements = new List<RteLib.RteModuleInternalBehavior.RteGenericRunnableObject>();


      mainfunctionRunnable = new RteLib.RteModuleInternalBehavior.RteCyclicRunnable();
      mainfunctionRunnable.Runnable = rteRunnable;
      mainfunctionRunnable.CycleTime = xmlCfg.miBMainfunctionCycleTimeMs;

      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* ++++++++++++++++++++++++++++++++++++++++ Build up and generate file +++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      internalBehavior.ModuleName = DefResources.ModuleName;
      internalBehavior.ModuleType = RteLib.RteConfig.RteConfigModuleTypes.BSW;
      internalBehavior.UUID = GenResources.RteMib_Module_UUID;
      internalBehavior.InitRunnables.Add(initRunnable);
      internalBehavior.CyclicRunnables.Add(mainfunctionRunnable);
      internalBehavior.AvailableServerPorts.Add(csI_PpGeneralNvm);
      internalBehavior.BackupCSBlueprintList.Add(cs_ipb_PpGeneralNvm);
      internalBehavior.BackupAdvancedDataTypeList.Add(adt_BlockStatusType);

      internalBehavior.SaveActiveRteModuleInternalBehaviorToXml(pathToModuleBaseFolder + GenResources.GenGeneratorFilePath + "\\" + GenResources.RteCfgFileName);

      info.AddLogMsg(GenResources.LogMsg_RteMibCfgFileGenerated);
    }
  }
}
/**
 * @}
 */
