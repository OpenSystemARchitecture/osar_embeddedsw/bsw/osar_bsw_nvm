﻿/*****************************************************************************************************************************
 * @file        Module_ViewModel.cs                                                                                          *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        26.03.2019                                                                                                   *
 * @brief       Implementation of the View Model from the Module Xml Cfg Class                                               *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
 * @addtogroup ModuleLibrary.Versions.v_1_0_0.ViewModels
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OsarResources.XML;
using OsarResources.Generic;
using System.Xml.Serialization;
using System.IO;
using OsarResources.Generator;
using System.Collections.ObjectModel;
using System.ComponentModel;
using ModuleLibrary.Versions.v_1_0_0.Models;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace ModuleLibrary.Versions.v_1_0_0.ViewModels
{
  /// <summary>
  /// Delegate to store the Configuration
  /// </summary>
  public delegate void StoreConfiguration();

  public class Module_ViewModel : BaseViewModel, OsarModuleGeneratorInterface
  {
    private static Models.NvmXml NvmXmlCfg = new Models.NvmXml();
    private static string pathToConfiguratioFile;
    private static string pathToModuleBaseFolder;
    private static StoreConfiguration storeCfg = new StoreConfiguration(SaveConfigToXml);
    private static ObservableCollection<NvmBaseModuleVM> baseModuleObservableCollection;
    private static ObservableCollection<NvmDataBlockVM> blockCfgObservableCollection;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="pathToCfgFile"> Should be a absolute Path</param>
    /// <param name="absPathToBaseModuleFolder"> Should be a absolute Path</param>
    public Module_ViewModel(string pathToCfgFile, string absPathToBaseModuleFolder) : base(storeCfg)
    {
      pathToConfiguratioFile = pathToCfgFile;
      pathToModuleBaseFolder = absPathToBaseModuleFolder;

      /* Read Configuration File */
      if (!File.Exists(pathToConfiguratioFile))
      {
        // If config file does not exist, create a default file
        SetDefaultConfiguration();
      }

      /* Read Configuration File */
      ReadConfigFromXml();

      // Initialize Base module list
      baseModuleObservableCollection = new ObservableCollection<NvmBaseModuleVM>();
      for (int idx = 0; idx < NvmXmlCfg.nvmSubModuleList.Count; idx++)
      {
        baseModuleObservableCollection.Add(new NvmBaseModuleVM(NvmXmlCfg.nvmSubModuleList[idx], storeCfg));
      }

      // Initialize module data block cfg list
      blockCfgObservableCollection = new ObservableCollection<NvmDataBlockVM>();
      for (int idx = 0; idx < NvmXmlCfg.nvmBlkCfgList.Count; idx++)
      {
        blockCfgObservableCollection.Add(new NvmDataBlockVM(NvmXmlCfg.nvmBlkCfgList[idx], storeCfg));
      }
    }


    #region public attributes
    /// <summary>
    /// Interface for the XML File Version
    /// </summary>
    public XmlFileVersion XmlFileVersion
    {
      get { return NvmXmlCfg.xmlFileVersion; }
      set { NvmXmlCfg.xmlFileVersion = value; }
    }

    /// <summary>
    /// Interface for the XML File Version string
    /// </summary>
    public string XmlFileVersion_String
    {
      get
      {
        return ( "v." + NvmXmlCfg.xmlFileVersion.MajorVersion.ToString() + "." +
          NvmXmlCfg.xmlFileVersion.MinorVersion.ToString() + "." +
          NvmXmlCfg.xmlFileVersion.PatchVersion.ToString() );
      }
    }

    /// <summary>
    /// Interface for the Det Module Id
    /// </summary>
    public UInt16 DetModuleID
    {
      get { return NvmXmlCfg.detModuleID; }
      set { NvmXmlCfg.detModuleID = value; }
    }

    /// <summary>
    /// Interface for the Det Module Usage
    /// </summary>
    public SystemState DetModuleUsage
    {
      get { return NvmXmlCfg.detModuleUsage; }
      set { NvmXmlCfg.detModuleUsage = value; }
    }

    /// <summary>
    /// Interface for the Module Version
    /// </summary>
    public string ModuleVersion
    {
      get { return Generator.DefResources.ModuleVersion; }
    }

    /// <summary>
    /// Interface for the Module Mainfunction cycle time
    /// </summary>
    public UInt16 MainfunctionCycleTime
    {
      get => NvmXmlCfg.miBMainfunctionCycleTimeMs;
      set => NvmXmlCfg.miBMainfunctionCycleTimeMs = value;
    }

    /// <summary>
    /// Interface for the Module Request Buffer Size
    /// </summary>
    public Byte RequestBufferSize
    {
      get => NvmXmlCfg.nvmRequestBufferSize;
      set => NvmXmlCfg.nvmRequestBufferSize = value;
    }

    /// <summary>
    /// Interface for the Base Module List
    /// </summary>
    public ObservableCollection<NvmBaseModuleVM> NvmBaseModules
    {
      get => baseModuleObservableCollection;
      set => baseModuleObservableCollection = value;
    }

    /// <summary>
    /// Interface for the Data block cfg list
    /// </summary>
    public ObservableCollection<NvmDataBlockVM> NvmDataBlocks
    {
      get => blockCfgObservableCollection;
      set => blockCfgObservableCollection = value;
    }
    #endregion

    #region UI Actions
    /// <summary>
    /// Interface to add a new base module
    /// </summary>
    public void AddNewBaseModule()
    {
      baseModuleObservableCollection.Add(new NvmBaseModuleVM("", storeCfg));
      OnPropertyChanged(new PropertyChangedEventArgs("NvmBaseModules"));
    }

    /// <summary>
    /// Interface to remove a specific base module
    /// </summary>
    /// <param name="baseModule"></param>
    public void RemoveBaseModule(NvmBaseModuleVM baseModule)
    {
      baseModuleObservableCollection.Remove(baseModule);
      OnPropertyChanged(new PropertyChangedEventArgs("NvmBaseModules"));
    }

    /// <summary>
    /// Interface to add a new base module
    /// </summary>
    public void AddNewDataBlock()
    {
      blockCfgObservableCollection.Add(new NvmDataBlockVM(new NvmBlkCfg(), storeCfg));
      OnPropertyChanged(new PropertyChangedEventArgs("NvmDataBlocks"));
    }

    /// <summary>
    /// Interface to remove a specific data block
    /// </summary>
    /// <param name="dataBlock"></param>
    public void RemoveDataBlock(NvmDataBlockVM dataBlock)
    {
      blockCfgObservableCollection.Remove(dataBlock);
      OnPropertyChanged(new PropertyChangedEventArgs("NvmDataBlocks"));
    }

    #endregion

    #region Configuration Load and Storage
    /*
     * @brief       Function to store the current configuration structure to an xml file
     * @param       none
     * @retval      none
     */
    private static void SaveConfigToXml()
    {
      // Write back UI page configuration data to config structure
      if (null != baseModuleObservableCollection)
      {
        NvmXmlCfg.nvmSubModuleList.Clear();
        foreach (var element in baseModuleObservableCollection)
        {
          NvmXmlCfg.nvmSubModuleList.Add(element.BaseModuleName);
        }
      }

      // Write back UI page configuration data to config structure
      if (null != blockCfgObservableCollection)
      {
        NvmXmlCfg.nvmBlkCfgList.Clear();
        foreach (var element in blockCfgObservableCollection)
        {
          NvmXmlCfg.nvmBlkCfgList.Add(element.BlockCfgData);
        }
      }

      XmlSerializer writer = new XmlSerializer(NvmXmlCfg.GetType());
      StreamWriter file = new StreamWriter(pathToConfiguratioFile);
      writer.Serialize(file, NvmXmlCfg);
      file.Close();
    }

    /*
     * @brief       Function to read the current configuration structure from an xml file
     * @param       none
     * @retval      none
     */
    private static void ReadConfigFromXml()
    {
      XmlSerializer reader = new XmlSerializer(NvmXmlCfg.GetType());
      StreamReader file = new StreamReader(pathToConfiguratioFile);
      NvmXmlCfg = ( Models.NvmXml)reader.Deserialize(file);
      file.Close();
    }
    #endregion

    #region Generic Osar Generator
    /// <summary>
    /// Used to validate the active configuration of its correctness.
    /// </summary>
    /// <returns>
    /// >> Return of standardized information <<
    /// List<string> info >> General validation information
    /// List<string> warnings >> General validation warnings
    /// List<string> errors >> General validation errors
    /// List<string> errors >> Validation logs
    /// </returns>
    public GenInfoType ValidateConfiguration()
    {
      GenInfoType genInfo1 = new GenInfoType(), genInfo2 = new GenInfoType(), genInfoMerge = new GenInfoType();
      ReadConfigFromXml();

      /* Ensure a correct version */
      if (null == NvmXmlCfg)
      {
        genInfo1 = SetDefaultConfiguration();
      }

      /* Call Validator */
      Generator.ModuleValidator modVal = new Generator.ModuleValidator(NvmXmlCfg, pathToConfiguratioFile);
      genInfo2 = modVal.ValidateConfiguration();

      /* Merge outputs */
      genInfoMerge = OsarGenHelper.MergeGenInfoType(genInfo1, genInfo2);
      return genInfoMerge;
    }

    /// <summary>
    /// Used to generate the active configuration.
    /// </summary>
    /// <returns>
    /// >> Return of standardized information <<
    /// List<string> info >> General validation information
    /// List<string> warnings >> General validation warnings
    /// List<string> errors >> General validation errors
    /// List<string> errors >> Validation logs
    /// </returns>
    public GenInfoType GenerateConfiguration()
    {
      GenInfoType genInfo1 = new GenInfoType(), genInfo2 = new GenInfoType(), genInfoMerge = new GenInfoType();
      ReadConfigFromXml();

      /* Validate configuration */
      genInfo1 = ValidateConfiguration();

      /* If no validation error has been detected >> Start generation */
      if (0U == genInfo1.error.Count)
      {
        Generator.ModuleGenerator modGen = new Generator.ModuleGenerator(NvmXmlCfg, pathToConfiguratioFile, pathToModuleBaseFolder);
        genInfo2 = modGen.GenerateConfiguration();
      }

      /* Merge outputs */
      genInfoMerge = OsarGenHelper.MergeGenInfoType(genInfo1, genInfo2);

      return genInfoMerge;
    }

    /// <summary>
    /// Used to set the default configuration.
    /// </summary>
    /// <returns>
    /// >> Return of standardized information <<
    /// List<string> info >> General validation information
    /// List<string> warnings >> General validation warnings
    /// List<string> errors >> General validation errors
    /// List<string> errors >> Validation logs
    /// </returns>
    public GenInfoType SetDefaultConfiguration()
    {
      GenInfoType genInfo;
      Generator.DefaultCfgGenerator defGen = new Generator.DefaultCfgGenerator(NvmXmlCfg, pathToConfiguratioFile);
      genInfo = defGen.CreateDefaultConfiguration();
      SaveConfigToXml();
      return genInfo;
    }

    /// <summary>
    /// Interface to update an outdated configuration to an up to date configuration version
    /// </summary>
    /// <returns>
    /// >> Return of standardized information <<
    /// List<string> info >> General validation information
    /// List<string> warnings >> General validation warnings
    /// List<string> errors >> General validation errors
    /// List<string> errors >> Validation logs
    /// </returns>
    public GenInfoType UpdateConfigurationFileVersion()
    {
      GenInfoType genInfo;
      ReadConfigFromXml();
      Generator.CfgVersionUpdater versUpdater = new Generator.CfgVersionUpdater(NvmXmlCfg, pathToConfiguratioFile);
      genInfo = versUpdater.UpdateCfgVersion();
      SaveConfigToXml();
      return genInfo;
    }
    /// <summary>
    /// Interface to update an outdated configuration to an specific configuration version
    /// </summary>
    /// <param name="updateToVersion"> Xml file version to be updated </param>
    /// <returns>
    /// >> Return of standardized information <<
    /// List<string> info >> General validation information
    /// List<string> warnings >> General validation warnings
    /// List<string> errors >> General validation errors
    /// List<string> errors >> Validation logs</returns>
    public GenInfoType UpdateConfigurationFileVersion(XmlFileVersion updateToVersion)
    {
      GenInfoType genInfo;
      ReadConfigFromXml();
      Generator.CfgVersionUpdater versUpdater = new Generator.CfgVersionUpdater(NvmXmlCfg, pathToConfiguratioFile);
      genInfo = versUpdater.UpdateCfgVersion(updateToVersion);
      SaveConfigToXml();
      return genInfo;
    }
    #endregion
  }
}
/**
 * @}
 */
