﻿/*****************************************************************************************************************************
 * @file        NvmDataBlockVM.cs                                                                                            *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        27.09.2019                                                                                                   *
 * @brief       Implementation of the View Model from the Nvm Data Block Config List                                         *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
 * @addtogroup ModuleLibrary.Versions.v_1_0_0.ViewModels
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using ModuleLibrary.Versions.v_1_0_0.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace ModuleLibrary.Versions.v_1_0_0.ViewModels
{
  /// <summary>
  /// View Model class for the block cfg Data
  /// </summary>
  public class NvmDataBlockVM : BaseViewModel
  {
    private NvmBlkCfg blockCfgData;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="blockCfg"></param>
    /// <param name="storeCfgDel"></param>
    public NvmDataBlockVM(NvmBlkCfg blockCfg, StoreConfiguration storeCfgDel) : base(storeCfgDel)
    {
      blockCfgData = blockCfg;
    }

    /// <summary>
    /// Interface for Attribute object blockCfgData
    /// </summary>
    public NvmBlkCfg BlockCfgData { get => blockCfgData; set => blockCfgData = value; }

    /// <summary>
    /// Interface for Attribute object blockCfgData.nvmBlockName
    /// </summary>
    public string BlockName { get => blockCfgData.nvmBlockName; set => blockCfgData.nvmBlockName = value; }

    /// <summary>
    /// Interface for Attribute object blockCfgData.nvmBlockDataLength
    /// </summary>
    public UInt16 BlockLength { get => blockCfgData.nvmBlockDataLength; set => blockCfgData.nvmBlockDataLength = value; }

    /// <summary>
    /// Interface for Attribute object blockCfgData.nvmBlockRamVariable
    /// </summary>
    public string BlockRamVariable { get => blockCfgData.nvmBlockRamVariable; set => blockCfgData.nvmBlockRamVariable = value; }

    /// <summary>
    /// Interface for Attribute object blockCfgData.nvmBlockUseCrc
    /// </summary>
    public NvmCrcTypes CrcType { get => blockCfgData.nvmBlockUseCrc; set => blockCfgData.nvmBlockUseCrc = value; }

    /// <summary>
    /// Interface for Attribute object blockCfgData.nvmBlockUseSubModule
    /// </summary>
    public string BlockBaseModule { get => blockCfgData.nvmBlockUseSubModule; set => blockCfgData.nvmBlockUseSubModule = value; }

    /// <summary>
    /// Interface for Attribute object blockCfgData.nvmBlockUseForReadAll
    /// </summary>
    public Boolean UseReadAll { get => blockCfgData.nvmBlockUseForReadAll; set => blockCfgData.nvmBlockUseForReadAll = value; }

    /// <summary>
    /// Interface for Attribute object blockCfgData.nvmBlockUseForWriteAll
    /// </summary>
    public Boolean UseWriteAll { get => blockCfgData.nvmBlockUseForWriteAll; set => blockCfgData.nvmBlockUseForWriteAll = value; }

  }
}
/**
 * @}
 */
