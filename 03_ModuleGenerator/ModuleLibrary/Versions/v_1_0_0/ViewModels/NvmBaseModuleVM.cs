﻿/*****************************************************************************************************************************
 * @file        NvmBaseModuleVM.cs                                                                                           *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        27.09.2019                                                                                                   *
 * @brief       Implementation of the View Model from the Base Module List                                                   *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
 * @addtogroup ModuleLibrary.Versions.v_1_0_0.ViewModels
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModuleLibrary.Versions.v_1_0_0.ViewModels
{
  /****************************************************************************************************************************/
  /* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
  /****************************************************************************************************************************/
  public class NvmBaseModuleVM : BaseViewModel
  {
    private string usedBaseModule;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="baseModule"></param>
    /// <param name="storeCfgDel"></param>
    public NvmBaseModuleVM(string baseModule, StoreConfiguration storeCfgDel) : base(storeCfgDel)
    {
      usedBaseModule = baseModule;
    }

    /// <summary>
    /// Interface for Attribute object usedBaseModule
    /// </summary>
    public string BaseModuleName { get => usedBaseModule; set => usedBaseModule = value; }
  }
}
/**
 * @}
 */
