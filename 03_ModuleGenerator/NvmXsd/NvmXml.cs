﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OsarResources.XML;
using OsarResources.Generic;

namespace NvmXsd
{
  public enum NvmCrcTypes
  {
    NVM_CRC_NONE,
    NVM_CRC_CRC32
  }

  public struct NvmBlkCfg
  {
    public String nvmBlockName;
    public UInt16 nvmBlockDataLength;
    public String nvmBlockRamVariable;
    public NvmCrcTypes nvmBlockUseCrc;
    public String nvmBlockUseSubModule;
    public Boolean nvmBlockUseForReadAll;
    public Boolean nvmBlockUseForWriteAll;
  }

  public class NvmXml
  {
    public XmlFileVersion xmlFileVersion;
    public UInt16 detModuleID;
    public SystemState detModuleUsage;

    public UInt16 miBMainfunctionCycleTimeMs;

    public Byte nvmRequestBufferSize;
    public List<String> nvmSubModuleList;
    public List<NvmBlkCfg> nvmBlkCfgList;
  }
}
