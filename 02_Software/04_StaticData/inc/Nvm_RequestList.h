/*****************************************************************************************************************************
 * @file        Nvm_RequestList.h                                                                                            *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        25.08.2018 15:58:25                                                                                          *
 * @brief       Implementation of definitions / interface function prototypes / datatypes and generic module interface       *
 *              informations of the "Nvm" module.                                                                            *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
 * @libNote     OSAR Source File Library v.1.0.0.0                                                                           *
*****************************************************************************************************************************/
#ifndef __NVM_REQUESTLIST_H
#define __NVM_REQUESTLIST_H

/**
* @addtogroup OSAR_BSW
* @{
*/
/**
* @addtogroup Nvm
* @{
*/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 Start of include area                        << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#include "Nvm_Types.h"
#include "Nvm_PBCfg.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                  End of include area                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of public data types                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of public data types                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            Start of public definition area                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of public definition area                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of global variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of global variables                      << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>           Start of public function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/**
 * @brief           Module global initialization function.
 * @param[in]       None
 * @retval          None
 * @details         This function shall be called before using the module and after the memory initialization.
 */
void Nvm_RequestList_Init( void );

/**
* @brief           Function to add a new write request to the request buffer 
* @param[in]       uint16 corresponding block id
* @param[in]       uint8* pointer to user request data
* @retval          Nvm_ReturnType
*                  > NVM_E_OK
*                  > NVM_E_NOT_OK
*                  > NVM_E_SKIPPED
*/
Nvm_ReturnType Nvm_RequestList_InsertANewWriteRequest(uint16 reqBlockId, uint8* reqDataPointer);

/**
* @brief           Function to add a new read request to the request buffer
* @param[in]       uint16 corresponding block id
* @param[in]       uint8* pointer to user request data
* @retval          Nvm_ReturnType
*                  > NVM_E_OK
*                  > NVM_E_NOT_OK
*                  > NVM_E_SKIPPED
*/
Nvm_ReturnType Nvm_RequestList_InsertANewReadRequest(uint16 reqBlockId, uint8* reqDataPointer);

/**
* @brief           Function to add a new erase request to the request buffer
* @param[in]       uint16 corresponding block id
* @retval          Nvm_ReturnType
*                  > NVM_E_OK
*                  > NVM_E_NOT_OK
*                  > NVM_E_SKIPPED
*/
Nvm_ReturnType Nvm_RequestList_InsertANewEraseRequest(uint16 reqBlockId);

/**
* @brief           Helper function to invalidate a specific list element
* @param[in]       None
* @retval          None
* @details         This function shall be called before using the module and after the memory initialization.
*/
void Nvm_RequestList_HlpInvalidateListElement(uint8 idx);
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of public function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/**
 * @}
 */
/**
 * @}
 */

#endif /* __NVM_REQUESTLIST_H */
