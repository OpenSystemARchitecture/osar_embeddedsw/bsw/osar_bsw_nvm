/*****************************************************************************************************************************
 * @file        NvmHdl.h                                                                                                     *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        25.08.2018 15:58:25                                                                                          *
 * @brief       Implementation of a automatic nvm block handler statemachine definitions / interface function prototypes /   * 
 *              datatypes and generic module interface informations of the "NvmHdl" module.                                  *
 *                                                                                                                           *
 * @details     Implementation of an non volatile memory handling. The module provides an simple interface to create and     *
 *              access non volatile memory blocks without handling the non volatile memory sequence. Additional it           *
 *              implements some safety features like an block CRC to detect invalid data.                                    *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
 * @libNote     OSAR Source File Library v.1.0.0.0                                                                           *
*****************************************************************************************************************************/
#ifndef __NVMHDL_H
#define __NVMHDL_H

/**
* @addtogroup OSAR_BSW
* @{
*/
/**
 * @addtogroup Nvm
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 Start of include area                        << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#include "Nvm_Types.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                  End of include area                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of public data types                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/**
 * @brief           Nvm Handler states
 */
typedef enum {
  NVMHDL_SM_REQUESTBLOCKSTATUS = 0,
  NVMHDL_SM_SET_NVM_REQUEST = 1,
  NVMHDL_SM_WAIT_FOR_NVM_REQUEST = 2
}NvmHdl_StateMachineStateType;

/**
* @brief           NvmHandle Type
*/
typedef struct{
  uint16 nvmBlockHdlId;                                             /*!< Nvm module block id */
  NvmHdl_StateMachineStateType nvmReadStatemachineState;            /*!< Nvm Handler Read Statemachien State */
  NvmHdl_StateMachineStateType nvmWriteStatemachineState;           /*!< Nvm Handler Write Statemachien State */
}NvmHdl_Type;




/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of public data types                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            Start of public definition area                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of public definition area                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of global variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of global variables                      << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>           Start of public function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/**
 * @brief           API struct object initialization function.
 * @param[in/out]   NvmHdl_Type pointer to corresponding NvmHdl
 * @param[in]       uint16 correspondending Nvm Block Id
 * @retval          Std_ReturnType
 *                  > E_OK
 *                  > E_NOT_OK
 * @details         This function shall be called before using the object.
 */
Std_ReturnType NvmHdl_Init( NvmHdl_Type* nvmHdl, uint16 nvmBlockId );

/**
 * @brief           API Nvm Handler Read Nvm Block Item 
 * @param[in/out]   NvmHdl_Type pointer to corresponding NvmHdl
 * @param[out]      uint8* pointer to the read data
 * @retval          Std_ReturnType
 *                  > E_OK          >> Request processing finished
 *                  > E_PENDING     >> Request still in progress
 *                  > E_SKIPPED     >> Request skipped >> Data invalid
 *                  > E_NOT_OK      >> Request failed
 * @details         This function is an handler function which abstracts the general Nvm APIs
 *                  It is used to read an Nvm Block Item
 */
Std_ReturnType NvmHdl_ReadBlockItem( NvmHdl_Type* nvmHdl, uint8* pData );

/**
 * @brief           API Nvm Handler Write Nvm Block Item 
 * @param[in/out]   NvmHdl_Type pointer to corresponding NvmHdl
 * @param[in]       uint8* pointer to the write data
 * @retval          Std_ReturnType
 *                  > E_OK          >> Request processing finished
 *                  > E_PENDING     >> Request still in progress
 *                  > E_SKIPPED     >> Request skipped >> Data invalid
 *                  > E_NOT_OK      >> Request failed
 * @details         This function is an handler function which abstracts the general Nvm APIs
 *                  It is used to write an Nvm Block Item
 */
Std_ReturnType NvmHdl_WriteBlockItem( NvmHdl_Type* nvmHdl, uint8* pData );

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of public function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/**
 * @}
 */
/**
 * @}
 */

#endif /* __NVMHDL_H*/
