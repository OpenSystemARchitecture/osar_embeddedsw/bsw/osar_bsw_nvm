/*****************************************************************************************************************************
 * @file        Nvm_RequestList.c                                                                                            *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        25.08.2018 15:58:25                                                                                          *
 * @brief       Implementation of functionalities from the "Nvm" module.                                                     *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
 * @libNote     OSAR Source File Library v.1.0.0.0                                                                           *
*****************************************************************************************************************************/

/**
* @addtogroup OSAR_BSW
* @{
*/
/**
* @addtogroup Nvm
* @{
*/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 Start of include area                        << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#include "Det.h"
#include "Nvm_RequestList.h"
#include "Nvm_PBCfg.h"
#include "Crc.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                  End of include area                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of private data types                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of private data types                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            Start of private definitions area                 << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of private definitions area                  << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of global variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/*------------------------------------------- Map variables into constand memory -------------------------------------------*/
#define Nvm_START_SEC_CONST
#include "Nvm_MemMap.h"
#define Nvm_STOP_SEC_CONST
#include "Nvm_MemMap.h"
/*---------------------------------------- Map variables into uninitialized memory ----------------------------------------*/
#define Nvm_START_SEC_NOINIT_VAR
#include "Nvm_MemMap.h"
Nvm_RequestListEntryType nvmRequestList[NVM_REQUEST_LIST_SIZE];
#define Nvm_STOP_SEC_NOINIT_VAR
#include "Nvm_MemMap.h"
/*----------------------------------------- Map variables into initialized memory -----------------------------------------*/
#define Nvm_START_SEC_INIT_VAR
#include "Nvm_MemMap.h"
#define Nvm_STOP_SEC_INIT_VAR
#include "Nvm_MemMap.h"
/*--------------------------------------- Map variables into zero initialized memory ---------------------------------------*/
#define Nvm_START_SEC_ZERO_INIT_VAR
#include "Nvm_MemMap.h"
#define Nvm_STOP_SEC_ZERO_INIT_VAR
#include "Nvm_MemMap.h"
/*-------------------------------------------------- External defined variables --------------------------------------------*/
extern const Nvm_BlockCfgType nvmBlkCfgList[];

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of global variables                      << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of private variables                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/*------------------------------------------- Map variables into constand memory -------------------------------------------*/
#define Nvm_START_SEC_CONST
#include "Nvm_MemMap.h"
#define Nvm_STOP_SEC_CONST
#include "Nvm_MemMap.h"
/*---------------------------------------- Map variables into uninitialized memory ----------------------------------------*/
#define Nvm_START_SEC_NOINIT_VAR
#include "Nvm_MemMap.h"
#define Nvm_STOP_SEC_NOINIT_VAR
#include "Nvm_MemMap.h"
/*----------------------------------------- Map variables into initialized memory -----------------------------------------*/
#define Nvm_START_SEC_INIT_VAR
#include "Nvm_MemMap.h"
#define Nvm_STOP_SEC_INIT_VAR
#include "Nvm_MemMap.h"
/*--------------------------------------- Map variables into zero initialized memory ---------------------------------------*/
#define Nvm_START_SEC_ZERO_INIT_VAR
#include "Nvm_MemMap.h"
#define Nvm_STOP_SEC_ZERO_INIT_VAR
#include "Nvm_MemMap.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of private variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>           Start of private function prototypes               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/


/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            End of private function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>        Start of private function implementation              << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/*--------------------------------------------- Map function into code memory ---------------------------------------------*/
#define Nvm_START_SEC_CODE
#include "Nvm_MemMap.h"

#define Nvm_STOP_SEC_CODE
#include "Nvm_MemMap.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>         End of private function implementation               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>          Start of public function implementation             << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/*------------------------------------------------------------  ------------------------------------------------------------*/
#define Nvm_START_SEC_CODE
#include "Nvm_MemMap.h"

/**
 * @brief           Module global initialization function.
 * @param[in]       None
 * @retval          None
 * @details         This function shall be called before using the module and after the memory initialization.
 */
void Nvm_RequestList_Init( void )
{
  uint8 idx;

  /* Initialize Request List */
  for (idx = 0; idx < NVM_REQUEST_LIST_SIZE; idx++)
  {
    Nvm_RequestList_HlpInvalidateListElement(idx);
  }
}

/**
* @brief           Function to add a new write request to the request buffer
* @param[in]       uint16 corresponding block id
* @param[in]       uint8* pointer to user request data
* @retval          Nvm_ReturnType
*                  > NVM_E_OK
*                  > NVM_E_NOT_OK
*                  > NVM_E_SKIPPED
*/
Nvm_ReturnType Nvm_RequestList_InsertANewWriteRequest(uint16 reqBlockId, uint8* reqDataPointer)
{
  Nvm_ReturnType retVal = NVM_E_NOT_OK;
  uint8 listIdx;
  uint16 dataIdx;
  boolean freeListIdxFound = FALSE;
  Crc_ReturnType crcRetVal;

  /* Check innput parameter */
  if (reqBlockId >= NVM_CNT_CONFIGURED_BLOCKS)
  {
    #if(STD_ON == NVM_MODULE_USE_DET)
    Det_ReportError(NVM_DET_MODULE_ID, NVM_E_INVALID_ARGUMENT);
    #endif
    retVal = NVM_E_NOT_OK;
  }
  else if (NULL_PTR == reqDataPointer)
  {
    #if(STD_ON == NVM_MODULE_USE_DET)
    Det_ReportError(NVM_DET_MODULE_ID, NVM_E_INVALID_ARGUMENT);
    #endif
    retVal = NVM_E_NOT_OK;
  }
  else
  {
    /* Perform Action */
    for (listIdx = 0; listIdx < NVM_REQUEST_LIST_SIZE; listIdx++)
    {
      if (NVM_BLK_EMPTY == nvmRequestList[listIdx].nvmRequestStatus)
      {
        freeListIdxFound = TRUE;
        break;
      }
    }

    if (TRUE == freeListIdxFound)
    {
      /* Calculate CRC if requested  */
      switch (nvmBlkCfgList[reqBlockId].nvmBlockUseCrc)
      {
      case NVM_CRC_NONE:
        crcRetVal = CRC_E_OK;
        break;

      case NVM_CRC_CRC32:
        crcRetVal = Crc32_0x04C11DB7_Compute(reqDataPointer, nvmBlkCfgList[reqBlockId].nvmBlockDataLength, (uint32*)&nvmRequestList[listIdx].nvmData[nvmBlkCfgList[reqBlockId].nvmBlockDataLength]);
        break;
        //TODO: Add more Crc algorithms i future >> CRC 16

      default:
        #if(STD_ON == NVM_MODULE_USE_DET)
        Det_ReportError(NVM_DET_MODULE_ID, NVM_E_INVALID_CONFIGURATION);
        #endif
        crcRetVal = CRC_E_OK;
        break;
      }

      /* Create list entry */
      if (CRC_E_OK == crcRetVal)
      {
        nvmRequestList[listIdx].nvmRequestStatus = NVM_BLK_WAITING;
        nvmRequestList[listIdx].requestDataPointer = reqDataPointer;
        nvmRequestList[listIdx].nvmBlockId = reqBlockId;
        nvmRequestList[listIdx].nvmRequestType = NVM_REQ_WRITE;
        for (dataIdx = 0; dataIdx < nvmBlkCfgList[reqBlockId].nvmBlockDataLength; dataIdx++)
        {
          nvmRequestList[listIdx].nvmData[dataIdx] = reqDataPointer[dataIdx];
        }

        retVal = NVM_E_OK;
      }
      else
      {
        /* Invalidate complete list element */
        Nvm_RequestList_HlpInvalidateListElement(listIdx);
        retVal = NVM_E_NOT_OK;
      }
    }
    else
    {
      retVal = NVM_E_SKIPPED;
    }
  }

  return retVal;
}
/**
* @brief           Function to add a new read request to the request buffer
* @param[in]       uint16 corresponding block id
* @param[in]       uint8* pointer to user request data
* @retval          Nvm_ReturnType
*                  > NVM_E_OK
*                  > NVM_E_NOT_OK
*                  > NVM_E_SKIPPED
*/
Nvm_ReturnType Nvm_RequestList_InsertANewReadRequest(uint16 reqBlockId, uint8* reqDataPointer)
{
  Nvm_ReturnType retVal = NVM_E_NOT_OK;
  uint8 listIdx;
  boolean freeListInxFound = FALSE;

  /* Check innput parameter */
  if (reqBlockId >= NVM_CNT_CONFIGURED_BLOCKS)
  {
#if(STD_ON == NVM_MODULE_USE_DET)
    Det_ReportError(NVM_DET_MODULE_ID, NVM_E_INVALID_ARGUMENT);
#endif
    retVal = NVM_E_NOT_OK;
  }
  else if (NULL_PTR == reqDataPointer)
  {
#if(STD_ON == NVM_MODULE_USE_DET)
    Det_ReportError(NVM_DET_MODULE_ID, NVM_E_INVALID_ARGUMENT);
#endif
    retVal = NVM_E_NOT_OK;
  }
  else
  {
    /* Perform Action */
    for (listIdx = 0; listIdx < NVM_REQUEST_LIST_SIZE; listIdx++)
    {
      if (NVM_BLK_EMPTY == nvmRequestList[listIdx].nvmRequestStatus)
      {
        freeListInxFound = TRUE;
        break;
      }
    }

    if (TRUE == freeListInxFound)
    {
      nvmRequestList[listIdx].nvmRequestStatus = NVM_BLK_WAITING;
      nvmRequestList[listIdx].requestDataPointer = reqDataPointer;
      nvmRequestList[listIdx].nvmBlockId = reqBlockId;
      nvmRequestList[listIdx].nvmRequestType = NVM_REQ_READ;
      retVal = NVM_E_OK;
    }
    else
    {
      retVal = NVM_E_SKIPPED;
    }
  }

  return retVal;
}

/**
* @brief           Function to add a new erase request to the request buffer
* @param[in]       uint16 corresponding block id
* @retval          Nvm_ReturnType
*                  > NVM_E_OK
*                  > NVM_E_NOT_OK
*                  > NVM_E_SKIPPED
*/
Nvm_ReturnType Nvm_RequestList_InsertANewEraseRequest(uint16 reqBlockId)
{
  Nvm_ReturnType retVal = NVM_E_NOT_OK;
  uint8 listIdx;
  boolean freeListInxFound = FALSE;

  /* Check innput parameter */
  if (reqBlockId >= NVM_CNT_CONFIGURED_BLOCKS)
  {
#if(STD_ON == NVM_MODULE_USE_DET)
    Det_ReportError(NVM_DET_MODULE_ID, NVM_E_INVALID_ARGUMENT);
#endif
    retVal = NVM_E_NOT_OK;
  }
  else
  {
    /* Perform Action */
    for (listIdx = 0; listIdx < NVM_REQUEST_LIST_SIZE; listIdx++)
    {
      if (NVM_BLK_EMPTY == nvmRequestList[listIdx].nvmRequestStatus)
      {
        freeListInxFound = TRUE;
        break;
      }
    }

    if (TRUE == freeListInxFound)
    {
      nvmRequestList[listIdx].nvmRequestStatus = NVM_BLK_WAITING;
      nvmRequestList[listIdx].requestDataPointer = NULL_PTR;
      nvmRequestList[listIdx].nvmBlockId = reqBlockId;
      nvmRequestList[listIdx].nvmRequestType = NVM_REQ_ERASE;
      retVal = NVM_E_OK;
    }
    else
    {
      retVal = NVM_E_SKIPPED;
    }
  }

  return retVal;
}

/**
* @brief           Helper function to invalidate a specific list element
* @param[in]       uint8 List index to be invalidated
* @retval          None
* @details         This function shall be called before using the module and after the memory initialization.
*/
void Nvm_RequestList_HlpInvalidateListElement(uint8 listIdx)
{
  uint16 idx;
  nvmRequestList[listIdx].nvmBlockId = 0xFFFF;
  nvmRequestList[listIdx].requestDataPointer = NULL_PTR;
  nvmRequestList[listIdx].nvmRequestStatus = NVM_BLK_EMPTY;
  nvmRequestList[listIdx].nvmRequestType = NVM_REQ_ERASE;
  nvmRequestList[listIdx].nvmSafe = 0x00;

  for (idx = 0; idx < NVM_MAX_REQUEST_DATA_SIZE; idx++)
  {
    nvmRequestList[listIdx].nvmData[idx] = 0x00;
  }
}
#define Nvm_STOP_SEC_CODE
#include "Nvm_MemMap.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>          End of public function implementation               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/**
 * @}
 */
/**
 * @}
 */
